var gulp = require('gulp');
var gutil = require('gulp-util');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify');
var pkg = require('./package.json');
var concat = require('gulp-concat');
var backend = require('./backend.js');

var reload = browserSync.reload;

// Compile SCSS files from /less into /css
gulp.task('sass', function () {
    return gulp.src('app/scss/style.scss')
            .pipe(sass())
            .pipe(gulp.dest('app/css'))
            .pipe(browserSync.stream())
});

//backend
gulp.task('backend', function () {
    return gulp.src('vendor/Codando/Controller/*.php')
               .pipe(backend());
});

//Juntar todos js
gulp.task('concat:js', function () {
    return gulp.src('app/js/*.js')
            .pipe(concat('all.js'))
            .pipe(gulp.dest('app/js'));
});


gulp.task('minify-css', function () {
    return gulp.src(['app/css/*.css', '!app/css/*.min.css'])
            .pipe(cleanCSS({compatibility: 'ie8'}))
            .pipe(rename({suffix: '.min'}))
            .pipe(gulp.dest('app/css'))
            .pipe(browserSync.stream())
});

// Minify JS
gulp.task('minify-js', function () {
    return gulp.src(['app/_js/*.js'])
            .pipe(uglify())
//        .pipe(rename({ suffix: '.min' }))
            .pipe(gulp.dest('app/js'))
            .pipe(browserSync.stream())
});

// Run everything - gulp
gulp.task('default', ['sass', 'minify-css', 'minify-js']);

// Configure the browserSync task
gulp.task('browserSync', function () {
    browserSync.init({
        proxy: {
            target: "http://sitedefault.dev", //Alterar para o link do projeto
            proxyRes: [
                function (proxyRes, req, res) {
                    //console.log(proxyRes.headers);
                }
            ]
        },
        watchOptions: {
            ignoreInitial: true,
            ignored: '*.txt'
        }
    })
})

// Dev task with browserSync - gulp dev
gulp.task('dev', ['browserSync', 'sass'], function () {

    gulp.watch('app/scss/*.scss', ['sass']);
    //gulp.watch('app/css/*.css', ['minify-css']);
    //gulp.watch('app/js/*.js', ['minify-js']);

    gulp.watch("vendor/Codando/Controller/*.php").on('change', reload);
    gulp.watch("vendor/Codando/Route/*.php").on('change', reload);
    gulp.watch("app/routes.php").on('change', reload);
    gulp.watch("app/_views/*.phtml").on('change', reload);
    gulp.watch('app/js/**/*.js').on('change', reload);
    gulp.watch('app/scss/*.scss').on('change', reload);
});
