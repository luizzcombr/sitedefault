
SET FOREIGN_KEY_CHECKS=0;

DROP VIEW IF EXISTS `vw_usuario`;
DROP VIEW IF EXISTS `vw_modulo_perfil_nivel`;
DROP VIEW IF EXISTS `vw_menu_modulo`;
DROP TABLE IF EXISTS `usuario`;
DROP TABLE IF EXISTS `pagina`;
DROP TABLE IF EXISTS `modulo_perfil_nivel`;
DROP TABLE IF EXISTS `perfil`;
DROP TABLE IF EXISTS `nivel`;
DROP TABLE IF EXISTS `log`;
DROP TABLE IF EXISTS `localizacao`;
DROP TABLE IF EXISTS `departamento`;
DROP TABLE IF EXISTS `contato`;
DROP TABLE IF EXISTS `arquivo`;
DROP TABLE IF EXISTS `modulo`;
DROP TABLE IF EXISTS `menu`;
DROP TABLE IF EXISTS `newsletter`;
DROP TABLE IF EXISTS `configuracao`;
DROP TABLE IF EXISTS `noticia`;
DROP TABLE IF EXISTS `zona`;
DROP TABLE IF EXISTS `redesocial`;
DROP TABLE IF EXISTS `banner`;

CREATE TABLE `banner` (
	`id_banner` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
	`titulo` VARCHAR(180) NOT NULL COMMENT 'Titulo',
	`link` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Link',
	`id_zona` INT(11) NOT NULL COMMENT 'Zona',
	`status` INT(11) NOT NULL COMMENT 'Status {"Ativo":1,"Inativo":2}',
	`datainicio` DATETIME NOT NULL COMMENT 'Data inicio',
	`datafim` DATETIME NOT NULL COMMENT 'Data fim',
	`resumo` VARCHAR(400) NULL DEFAULT NULL COMMENT 'Google AdSense',
	`acesso` INT(11) NULL DEFAULT '0' COMMENT 'Acesso',
	`exibido` INT(11) NULL DEFAULT '0' COMMENT 'Exibido',
	PRIMARY KEY (`id_banner`),
	UNIQUE INDEX `id_banner` (`id_banner`),
	INDEX `id_zona` (`id_zona`),
	CONSTRAINT `banner_zona` FOREIGN KEY (`id_zona`) REFERENCES `zona` (`id_zona`)
)
COMMENT='Identificador' COLLATE='utf8_general_ci' ENGINE=InnoDB AUTO_INCREMENT=1;

CREATE TABLE `zona` (
	`id_zona` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
	`titulo` VARCHAR(180) NOT NULL COMMENT 'Titulo',
	PRIMARY KEY (`id_zona`),
	UNIQUE INDEX `id_zona` (`id_zona`),
	UNIQUE INDEX `titulo` (`titulo`)
)
COMMENT='zona' COLLATE='utf8_general_ci' ENGINE=InnoDB AUTO_INCREMENT=1;

CREATE TABLE `configuracao` (
  `id_configuracao` INTEGER(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
  `titulo` VARCHAR(20) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Titulo',
  `configuracao` VARCHAR(400) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Configuracao',
  PRIMARY KEY USING BTREE (`id_configuracao`) COMMENT '',
  UNIQUE INDEX `id_configuracao` USING BTREE (`id_configuracao`) COMMENT ''
)ENGINE=InnoDB CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';

CREATE TABLE `newsletter` (
  `id_newsletter` INTEGER(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
  `nome` VARCHAR(20) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Nome',
  `email` VARCHAR(200) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Email',
  PRIMARY KEY USING BTREE (`id_newsletter`) COMMENT '',
  UNIQUE INDEX `id_newsletter` USING BTREE (`id_newsletter`) COMMENT '',
  UNIQUE INDEX `email` USING BTREE (`email`) COMMENT ''
)ENGINE=InnoDB CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';

CREATE TABLE `menu` (
  `id_menu` INTEGER(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
  `nome` VARCHAR(20) COLLATE utf8_general_ci NOT NULL COMMENT 'Nome do Menu',
  `icone` VARCHAR(20) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Icone',
  PRIMARY KEY USING BTREE (`id_menu`) COMMENT '',
  UNIQUE INDEX `id_menu` USING BTREE (`id_menu`) COMMENT '',
  UNIQUE INDEX `nome` USING BTREE (`nome`) COMMENT ''

  )ENGINE=InnoDB CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';

CREATE TABLE `noticia` (
  `id_noticia` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
  `titulo` varchar(250) NOT NULL COMMENT 'Titulo',
  `url_titulo` varchar(250) NOT NULL COMMENT 'URL Titulo',
  `data` datetime NOT NULL COMMENT 'Data',
  `status` int(11) DEFAULT '1' COMMENT 'Status {"Ativa":1, "Rascunho":2}',
  `autor` varchar(80) DEFAULT NULL COMMENT 'Autor',
  `fonte` varchar(200) DEFAULT NULL COMMENT 'Fonte',
  `resumo` varchar(250) DEFAULT NULL COMMENT 'Resumo',
  `texto` text NOT NULL COMMENT 'Texto',
  PRIMARY KEY (`id_noticia`),
  UNIQUE KEY `id_noticia` (`id_noticia`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='Noticia';

CREATE TABLE `modulo` (
  `id_modulo` INTEGER(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
  `nome` VARCHAR(50) COLLATE utf8_general_ci NOT NULL COMMENT 'Nome',
  `tabela` VARCHAR(50) COLLATE utf8_general_ci NOT NULL COMMENT 'Tabela',
  `url_nome` VARCHAR(50) COLLATE utf8_general_ci NOT NULL COMMENT 'URL',
  `type` VARCHAR(2) COLLATE utf8_general_ci NOT NULL COMMENT 'Tipo',
  `view` VARCHAR(50) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'View',
  `ordem` INTEGER(11) DEFAULT NULL COMMENT 'Ordem',
  `arquivo` INTEGER(11) DEFAULT NULL COMMENT 'Arquivo {\"SIM\":1,\"NAO\":0}',
  `arquivo_quantidade` INTEGER(11) DEFAULT NULL COMMENT 'Quantidade de Arq.',
  `arquivo_ext` VARCHAR(200) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Extensão do Arq.',
  `dependencia` INTEGER(11) DEFAULT 1 COMMENT 'Dependencia {\"UNI\":1,\"NOT\":0,\"MUL\":2}',
  `visibilidade` INTEGER(11) DEFAULT 1 COMMENT 'Visivel {\"SIM\":1,\"NAO\":0,\"WIDGET\":2,\"Relacionado\":3}',
  `id_menu` INTEGER(11) NOT NULL DEFAULT 1 COMMENT 'Menu',
  PRIMARY KEY USING BTREE (`id_modulo`) COMMENT '',
  UNIQUE INDEX `nome` USING BTREE (`nome`) COMMENT '',
  UNIQUE INDEX `url` USING BTREE (`url_nome`) COMMENT '',
  UNIQUE INDEX `tabela` USING BTREE (`tabela`) COMMENT '',
  UNIQUE INDEX `url_nome` USING BTREE (`url_nome`) COMMENT '',
   INDEX `id_menu` USING BTREE (`id_menu`) COMMENT '',
  CONSTRAINT `modulo_menu` FOREIGN KEY (`id_menu`) REFERENCES `menu` (`id_menu`) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=InnoDB CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' COMMENT='';

CREATE TABLE `arquivo` (
  `id_arquivo` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `url` VARCHAR(255) COLLATE utf8_general_ci NOT NULL DEFAULT 'semfoto.jpg',
  `id_est` INTEGER(11) NOT NULL COMMENT 'Identificador estrageiro',
  `id_modulo` INTEGER(11) NOT NULL COMMENT 'Modulo relacionado com o idestr',
  `ordem` INTEGER(11) DEFAULT 0,
  `legenda` VARCHAR(180) COLLATE utf8_general_ci DEFAULT NULL,
  `fonte` VARCHAR(120) COLLATE utf8_general_ci DEFAULT NULL,
  `link` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Link',
  PRIMARY KEY USING BTREE (`id_arquivo`) COMMENT '',
   INDEX `id_modulo` USING BTREE (`id_modulo`) COMMENT '',
  CONSTRAINT `arquivo_modulo` FOREIGN KEY (`id_modulo`) REFERENCES `modulo` (`id_modulo`) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=InnoDB CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' COMMENT='';

CREATE TABLE `contato` (
  `id_contato` INTEGER(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
  `nome` VARCHAR(180) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Nome',
  `assunto` VARCHAR(180) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Assunto',
  `email` VARCHAR(180) COLLATE utf8_general_ci NOT NULL COMMENT 'Email',
  `telefone` VARCHAR(30) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Telefone',
  `mensagem` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Mensagem',
  `data` DATE DEFAULT NULL COMMENT 'Data',
  PRIMARY KEY USING BTREE (`id_contato`) COMMENT '',
  UNIQUE INDEX `id_contato` USING BTREE (`id_contato`) COMMENT ''
)ENGINE=InnoDB CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' COMMENT='Contato';

CREATE TABLE `departamento` (
  `id_departamento` INTEGER(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
  `nome` VARCHAR(22) COLLATE utf8_general_ci NOT NULL COMMENT 'Nome',
  `email` VARCHAR(50) COLLATE utf8_general_ci NOT NULL COMMENT 'Email',
  PRIMARY KEY USING BTREE (`id_departamento`) COMMENT ''
)ENGINE=InnoDB CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' COMMENT='';

CREATE TABLE `localizacao` (
  `id_localizacao` INTEGER(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
  `titulo` VARCHAR(100) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Titulo',
  `endereco` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Endereço',
  `telefone` VARCHAR(40) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Telefone',
  `email` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Email',
  `cep` VARCHAR(20) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'CEP',
  `cidadeuf` VARCHAR(200) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Cidade / UF',
  `resumo` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Texto',
  `latlong` VARCHAR(80) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Google Maps',
  PRIMARY KEY USING BTREE (`id_localizacao`) COMMENT '',
  UNIQUE INDEX `id_localizacao` USING BTREE (`id_localizacao`) COMMENT ''
)ENGINE=InnoDB CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' COMMENT='Localizacao';

CREATE TABLE `log` (
  `id_log` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) COLLATE utf8_general_ci NOT NULL COMMENT 'Acao do log',
  `id_modulo` INTEGER(11) NOT NULL COMMENT 'Modulo identificador',
  `id_usuario` INTEGER(11) NOT NULL COMMENT 'Identificador do usuario',
  `identificador` INTEGER(11) NOT NULL COMMENT 'Identificador',
  `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date da acao',
  `msg` VARCHAR(255) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Ultimas mensagens',
  PRIMARY KEY USING BTREE (`id_log`) COMMENT '',
  UNIQUE INDEX `id_log` USING BTREE (`id_log`) COMMENT ''
)ENGINE=InnoDB CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';

CREATE TABLE `nivel` (
  `id_nivel` INTEGER(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
  `nome` VARCHAR(20) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Nome',
  PRIMARY KEY USING BTREE (`id_nivel`) COMMENT '',
  UNIQUE INDEX `id_nivel` USING BTREE (`id_nivel`) COMMENT ''
)ENGINE=InnoDB CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='';

CREATE TABLE `perfil` (
  `id_perfil` INTEGER(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
  `nome` VARCHAR(40) COLLATE utf8_general_ci NOT NULL COMMENT 'Nome do perfil',
  `ativo` INTEGER(11) DEFAULT 1 COMMENT 'Status',
  PRIMARY KEY USING BTREE (`id_perfil`) COMMENT '',
  UNIQUE INDEX `id_perfil` USING BTREE (`id_perfil`) COMMENT ''
)ENGINE=InnoDB CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Perfis de usuario';

CREATE TABLE `modulo_perfil_nivel` (
  `id_modulo_perfil_nivel` INTEGER(11) NOT NULL AUTO_INCREMENT COMMENT 'Ientificador',
  `id_perfil` INTEGER(11) NOT NULL COMMENT 'Perfil',
  `id_modulo` INTEGER(11) NOT NULL COMMENT 'Modulo',
  `id_nivel` INTEGER(11) NOT NULL COMMENT 'Permissão',
  PRIMARY KEY USING BTREE (`id_modulo_perfil_nivel`) COMMENT '',
  UNIQUE INDEX `modulo_perfil_nivel` USING BTREE (`id_modulo_perfil_nivel`) COMMENT '',
  UNIQUE INDEX `unico_id` USING BTREE (`id_perfil`, `id_modulo`, `id_nivel`) COMMENT '',
   INDEX `id_modulo` USING BTREE (`id_modulo`) COMMENT '',
   INDEX `id_nivel` USING BTREE (`id_nivel`) COMMENT '',
   INDEX `id_perfil` USING BTREE (`id_perfil`) COMMENT '',
  CONSTRAINT `modulo_perfil_nivel_modulo` FOREIGN KEY (`id_modulo`) REFERENCES `modulo` (`id_modulo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `modulo_perfil_nivel_nivel` FOREIGN KEY (`id_nivel`) REFERENCES `nivel` (`id_nivel`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `modulo_perfil_nivel_pf` FOREIGN KEY (`id_perfil`) REFERENCES `perfil` (`id_perfil`) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=InnoDB CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' COMMENT='';

CREATE TABLE `pagina` (
  `id_pagina` INTEGER(11) NOT NULL AUTO_INCREMENT COMMENT 'identificador',
  `titulo` VARCHAR(180) COLLATE utf8_general_ci NOT NULL COMMENT 'Titulo',
  `texto` TEXT COLLATE utf8_general_ci COMMENT 'Texto',
  PRIMARY KEY USING BTREE (`id_pagina`) COMMENT '',
  UNIQUE INDEX `id_pagina` USING BTREE (`id_pagina`) COMMENT ''
)ENGINE=InnoDB CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Paginas';

CREATE TABLE IF NOT EXISTS `redesocial` (
  `id_redesocial` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(160) NOT NULL COMMENT 'Titulo',
  `link` varchar(200) DEFAULT NULL COMMENT 'Link',
  PRIMARY KEY (`id_redesocial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `usuario` (
  `id_usuario` INTEGER(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
  `nome` VARCHAR(120) COLLATE utf8_general_ci NOT NULL COMMENT 'Nome',
  `url_nome` VARCHAR(120) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'URL',
  `email` VARCHAR(255) COLLATE utf8_general_ci NOT NULL COMMENT 'Email',
  `senha` VARCHAR(255) COLLATE utf8_general_ci NOT NULL COMMENT 'Senha',
  `ativo` INTEGER(11) NOT NULL DEFAULT 1 COMMENT 'Status {\"ATIVO\":1, \"INATIVO\":2}',
  `data` DATE DEFAULT NULL COMMENT 'Data',
  `id_perfil` INTEGER(11) NOT NULL COMMENT 'Perfil do usuario',
  `descricao` VARCHAR(600) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Descrição',
  `colunista` INTEGER(11) DEFAULT 2 COMMENT 'Colunista {\"SIM\":1, \"NAO\":2}',
  PRIMARY KEY USING BTREE (`id_usuario`) COMMENT '',
  UNIQUE INDEX `email` USING BTREE (`email`) COMMENT '',
   INDEX `id_perfil` USING BTREE (`id_perfil`) COMMENT '',
  CONSTRAINT `usuario_perfil` FOREIGN KEY (`id_perfil`) REFERENCES `perfil` (`id_perfil`)
)ENGINE=InnoDB CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Usuario';

CREATE VIEW `vw_menu_modulo`
AS
select 
    `mo`.`id_modulo` AS `id_modulo`,
    `mo`.`nome` AS `nome`,
    `mo`.`tabela` AS `tabela`,
    `mo`.`url_nome` AS `url_nome`,
    `mo`.`type` AS `type`,
    `mo`.`view` AS `view`,
    `mo`.`ordem` AS `ordem`,
    `mo`.`arquivo` AS `arquivo`,
    `mo`.`arquivo_quantidade` AS `arquivo_quantidade`,
    `mo`.`arquivo_ext` AS `arquivo_ext`,
    `mo`.`dependencia` AS `dependencia`,
    `mo`.`visibilidade` AS `visibilidade`,
    `mo`.`id_menu` AS `id_menu`,
    `p`.`id_perfil` AS `id_perfil` 
  from 
    (((`modulo_perfil_nivel` `mpn` join `modulo` `mo` on((`mpn`.`id_modulo` = `mo`.`id_modulo`))) join `perfil` `p` on((`mpn`.`id_perfil` = `p`.`id_perfil`))) join `menu` `me` on((`mo`.`id_menu` = `me`.`id_menu`)));

#
# Definition for the `vw_modulo_perfil_nivel` view : 
#

CREATE VIEW `vw_modulo_perfil_nivel`
AS
select 
    `mod_per_n`.`id_modulo_perfil_nivel` AS `id_modulo_perfil_nivel`,
    `mod_per_n`.`id_perfil` AS `id_perfil`,
    `mod_per_n`.`id_modulo` AS `id_modulo`,
    `mod_per_n`.`id_nivel` AS `id_nivel`,
    `n`.`nome` AS `nivel`,
    `m`.`nome` AS `modulo`,
    `per_us`.`nome` AS `perfil` 
  from 
    (((`modulo_perfil_nivel` `mod_per_n` join `nivel` `n` on((`mod_per_n`.`id_nivel` = `n`.`id_nivel`))) join `perfil` `per_us` on((`mod_per_n`.`id_perfil` = `per_us`.`id_perfil`))) join `modulo` `m` on((`mod_per_n`.`id_modulo` = `m`.`id_modulo`)));

#
# Definition for the `vw_usuario` view : 
#

CREATE VIEW `vw_usuario`
AS
select 
    `usu`.`id_usuario` AS `id_usuario`,
    `usu`.`nome` AS `nome`,
    `usu`.`email` AS `email`,
    `usu`.`senha` AS `senha`,
    `usu`.`ativo` AS `ativo`,
    `usu`.`data` AS `data`,
    `usu`.`id_perfil` AS `id_perfil`,
    `per`.`nome` AS `perfil_nome`,
    `per`.`ativo` AS `perfil_ativo` 
  from 
    (`usuario` `usu` join `perfil` `per` on((`usu`.`id_perfil` = `per`.`id_perfil`)));

	
INSERT INTO `menu` (`id_menu`, `nome`, `icone`) VALUES
  (1,'Painel',NULL),
  (2,'ConfiguraÃ§Ã£o',NULL),
  (3,'Modulo',NULL),
  (4,'Log Sistema',NULL);
COMMIT;

INSERT INTO `configuracao` (`id_configuracao`, `titulo`, `configuracao`) VALUES
  (1,'Titulo','Descricao');
COMMIT;


INSERT INTO `modulo` (`id_modulo`, `nome`, `tabela`, `url_nome`, `type`, `view`, `ordem`, `arquivo`, `arquivo_quantidade`, `arquivo_ext`, `dependencia`, `visibilidade`, `id_menu`) VALUES

  (1,'Nivel','nivel','nivel','2',NULL,99,0,1,'',0,0,3),
  (2,'Modulo','modulo','modulo','2',NULL,99,0,0,'',1,1,3),
  (3,'Modulo perfil','vw_modulo_perfil_nivel','modulo-perfil','2','vw_modulo_perfil_nivel',99,0,0,'',1,0,3),
  (4,'Menu','menu','menu','2',NULL,99,0,0,'',0,1,3),
  (5,'Usuario','usuario','usuario','2',NULL,99,0,0,'',1,1,2),
  (6,'Logs','log','log','2',NULL,99,0,0,'',1,1,2),
  (10,'Newsletter','newsletter','newsletter','2',NULL,99,0,0,'0',0,1,2),
  (11,'Departamento','departamento','departamento','2',NULL,99,0,0,'0',0,1,2),
  (13,'Perfil','perfil','perfil','2',NULL,99,0,0,'',0,1,3),
  (14,'Contato','contato','contato','1',NULL,99,0,0,'',0,0,1),
  (15,'LocalizaÃ§Ã£o','localizacao','localizacao','1',NULL,99,0,0,'jpg,png,jpeg',0,1,2),
  (19,'Modulo permissÃ£o','modulo-permissao','modulo-permissao','2','',99,0,0,'',0,2,3),
  (20,'ConfiguraÃ§Ã£o','configuracao','configuracao','1',NULL,99,0,0,'jpg,png,jpeg',0,1,2),
  (21,'Pagina','pagina','pagina','2','',5,1,50,'jpg,png,jpg',0,1,1),
  (22,'Player','player','player','2',NULL,3,1,1,'jpg,png,jpg',0,1,3);
COMMIT;

#
# Data for the `nivel`
#

INSERT INTO `nivel` (`id_nivel`, `nome`) VALUES

  (1,'VER'),
  (2,'ADICIONAR'),
  (3,'ATUALIZAR'),
  (4,'REMOVER');
COMMIT;

#
# Data for the `perfil`
#

INSERT INTO `perfil` (`id_perfil`, `nome`, `ativo`) VALUES
  (1,'Desenvolvedor',1);
COMMIT;

#
# Data for the `modulo_perfil_nivel`
#

INSERT INTO `modulo_perfil_nivel` (`id_modulo_perfil_nivel`, `id_perfil`, `id_modulo`, `id_nivel`) VALUES
  (NULL,1,1,1),
  (NULL,1,1,2),
  (NULL,1,1,3),
  (NULL,1,1,4),
  (NULL,1,2,1),
  (NULL,1,2,2),
  (NULL,1,2,3),
  (NULL,1,2,4),
  (NULL,1,3,1),
  (NULL,1,3,2),
  (NULL,1,3,3),
  (NULL,1,3,4),
  (NULL,1,4,1),
  (NULL,1,4,2),
  (NULL,1,4,3),
  (NULL,1,4,4),
  (NULL,1,5,1),
  (NULL,1,5,2),
  (NULL,1,5,3),
  (NULL,1,5,4),
  (NULL,1,6,1),
  (NULL,1,6,2),
  (NULL,1,6,3),
  (NULL,1,6,4),
  (NULL,1,10,1),
  (NULL,1,10,2),
  (NULL,1,10,3),
  (NULL,1,10,4),
  (NULL,1,11,1),
  (NULL,1,11,2),
  (NULL,1,11,3),
  (NULL,1,11,4),
  (NULL,1,20,1),
  (NULL,1,20,2),
  (NULL,1,20,3),
  (NULL,1,20,4),
  (NULL,1,21,1),
  (NULL,1,21,3),
  (NULL,1,14,1),
  (NULL,1,14,2),
  (NULL,1,14,3),
  (NULL,1,14,4),
  (NULL,1,13,1),
  (NULL,1,13,2),
  (NULL,1,13,3),
  (NULL,1,13,4),
  (NULL,1,19,1),
  (NULL,1,19,2),
  (NULL,1,19,3),
  (NULL,1,19,4),
  (NULL,1,15,1),
  (NULL,1,15,2),
  (NULL,1,15,3),
  (NULL,1,15,4);
COMMIT;


#
# Data for the `usuario`
#
# senha 1234
INSERT INTO `usuario` (`id_usuario`, `nome`, `url_nome`, `email`, `senha`, `ativo`, `data`, `id_perfil`, `descricao`) VALUES
  (1,'Administrador','administrador','trindade@trinitybrasil.com.br','fe76dea8bb0d51f7872bd22a2ea0babb',1,NULL,1,NULL); 
COMMIT;