Exemplo de configuracao com o dominio fictício : sitedefault.com.br
PHP >= 5.3 & psr-0 & composer
MYSQL > 4.5

ALTERAR CONFIGURAÇÃO:

	\composer.json - Nome do projeto e dependencias

	\.htaccess - HTTP_REFERER

	\app\configuracao.php - Configurações geral[DB, TITLE]
	
REGRAS URL

	/css/styles.css[app/styles.php] carrega todos os CSS configurados em configuracao.php
	
	/imagens[app/_arquivos/timthumb.php] criar thumb de imagens

JS
	requirejs.org esta sendo usado para o carregamento, javascritp em formato AMD
	
INFO:

	\app - Pasta publica, acessivel a http://www.com.br
	
	\app\routes.php - configuracao da rotas via Slim
	
	\app\_arquivos - Pasta padrao Upload, separando em modulo por pasta;
	
	\app\_arquivos\temp - Pasta Temporario de Upload;
	
	\vendor\Codando - Principais codigos
	
	\vendor\Codando\Route - Classes de rotas
	
	\app\_views - padrao de templates

	\app\_config - padrao de configuração

Obs:

	User 'composer install' pra instalar as dependencias !