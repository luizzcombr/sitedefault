<?php

$root = str_replace('\\', '/', dirname(dirname(dirname(__FILE__))));

/*
 * CONFIGURACAO
 */

return array(
    'php' => array(//Configuracao Padrão PHP
        'display_errors' => 1,
        'error_reporting' => (E_ALL | E_STRICT),
        'default_charset' => 'UTF-8',
        'date_default_timezone' => 'America/Campo_Grande'
    ),
    'dev' => array(
        'nome' => 'Andrey',
        'email' => 'andrey@neexbrasil.com'
    ),
    'header' => array(//Configuracao de Cabeçario Padrão
        'X-XSS-Protection' => '1; mode=block',
        'X-Frame-Options' => 'sameorigin',
        'X-Content-Type-Options' => 'nosniff',
        'X-Powered-By' => 'www.luizz.com.br',
        'Author' => 'www.luizz.com.br',
        'Server' => '',
        'Connection' => 'Keep-Alive'
    ),
   'recaptcha' => array(
        'key' => '6LctkRcTAAAAACsTaHwwlN5E5qvW2NEGoUGYUw8H',
        'secret' => '6LctkRcTAAAAAISqxImuKqV0r9vj_RotAmfC_OvK'
    ),
    'translate' => array(
        'dir' => '/_translates',
        'default' => 'en',
        'lang' => array('pt_BR', 'en'),
        'routes' => array('en' => '1', 'pt' => '2', 'es' => '3')
    ),
    'dominio' => array(//Configuracao Geral do Site
        'root' => 'http://sitedefault.trinitybrasil.net.br',
        'cdn' => 'http://sitedefault.trinitybrasil.net.br',
        'notfound_image' => 'semimagem.jpg',
        'language' => 'pt-br',
        'title' => 'Site Titulo',
        'description' => 'Site Descrição',
        'web_author' => '',
        'copyright' => 'Copyright (c) 2017',
        'path' => '/',
        'session_name' => 'sitedefault',
        'google-translate' => 'notranslate',
        'google-robots' => 'index,follow',
        'google-analytics' => 'UA-64673024-1',
        'og_logo' => 'http://sitedefault.trinitybrasil.net.br/img/logo.png',
        'mobile' => NULL,
        'version' => '1'
    ),
    'database' => array(//Configuracao do Banco de Dados
        'driver' => 'mysql',
        'host' => 'localhost',
        'port' => '3306',
        'name' => 'sitedefault',
        'user' => 'root',
        'password' => '',
        'cache' => false,
        'timecache' => '60',
        'nocache' => array('arquivo', 'configuracao') //Nao criar cache 
    ),
    'emailsend' => array(//Configuracao para envio de E-mail
        'smtp' => 'smtp.gmail.com',
        'port' => '587',
        'email' => 'naoresponda@sitedefault.com',
        'senha' => '',
        'return' => 'naoresponda@sitedefault.com.br',
		'type' => 'tls',
        'departament_contato' => '1'
    ),
    'template' => array(//Configuração do Template(View) 
        'dir' => '/_views',
        'dir_cache' => '/_views/phcache',
        'time' => '60',
        'cache' => false,
        'ext' => 'phtml',
        'minify' => true,
        'lazyload' => false
    ),
    'sgc' => array(//Configurações SGC - Sistema de Gerenciamento de Conteudo
        'dominio' => 'sitedefault.com.br',
        'system' => 'sgcsite.trinitybrasil.net.br'
    ),
    'sitemap' => array('contato', 'sobre'), //GERAR SITE MAP pelo /app/sitemap.php
    'styles' => array(//Lista de CSS , que sera juntados em /css/styles.css pelo /app/styles.php
        $root . '/app/css/normalize.css',
        $root . '/app/css/style.css'
    ),
	'pagseguro' => array(
      'email' => 'pagseguro@sitedefault.com',
      'token' => '0123456789ABCDEFGHIJLMNOP'
    ),
);
