<?php

$translate = \Codando\App::getConfig('translate');
$LANGS = array_keys($translate['routes']);
$DEFAULT_LANG = $translate['default'];

\Slim\Route::setDefaultConditions(array(
    'url' => '[a-z0-9-]+',
    'codigo' => '[a-zA-Z0-9-]+',
    'id' => '[0-9]+',
    'lang' => implode('|', $LANGS),
    'modulo' => '[a-z0-9-]+',
    'action' => '[a-z0-9-]+'
));

//SGC API
$app->get('/sgcapi/:modulo/:action/:id', '\Codando\System\SGC:api');

$langCheck = '\Codando\Route\Lang::check';

$app->get('/', '\Codando\Route\Home:langDefault');

//$app->add(new \Codando\Route\Visitante());

$app->group('/:lang', $langCheck, function() use($app) {
	
	$authLogado = '\Codando\Route\Auth::logado';
	$notauthLogado = '\Codando\Route\Auth::notlogado';
	
    /* Home */
    $app->map('/', '\Codando\Route\Home:index')->via('GET', 'POST');

    /* Sobre */
    $app->get('/about', '\Codando\Route\Pagina:sobre');

    /* Contato */
    $app->map('/contact', '\Codando\Route\Contato:insert')->via('GET', 'POST');

    /* APOSTAS */

    /* USUARIO */
    $app->get('/logout', '\Codando\Route\Auth:logout');
    $app->map('/register', $notauthLogado, '\Codando\Route\Cadastro:insert')->via('GET', 'POST');
    $app->map('/login', $notauthLogado, '\Codando\Route\Cadastro:login')->via('GET', 'POST');
    $app->map('/forgot-password', $notauthLogado, '\Codando\Route\Cadastro:esqueceSenha')->via('GET', 'POST');
    $app->map('/profile', $authLogado, '\Codando\Route\Cadastro:update')->via('GET', 'POST');
});

/* MORE ... */
$app->get('/islogado', '\Codando\Route\Cadastro:is_logado');
$app->post('/conf-notificacao', '\Codando\Route\Cadastro:updateGcm');

$app->notFound(function () use ($DEFAULT_LANG) {
    redirecionar('/' . $DEFAULT_LANG);
});
