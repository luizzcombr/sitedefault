<?php

\Slim\Route::setDefaultConditions(array(
    'url' => '[a-z0-9-]+',
    'modulo' => '[a-z0-9-]+',
    'action' => '[a-z0-9-]+',
    'id' => '[0-9]+',
));

//SGC API
$app->get('/sgcapi/:modulo/:action/:id', '\Codando\System\SGC:api');

/* HOME */
$app->get('/', '\Codando\Route\Home:index');
$app->get('/erro404', '\Codando\Route\Home:erro404');
$app->get('/jserro', '\Codando\Route\Home:jserro');
$app->notFound('\Codando\Route\Home::erro404');


$app->get('/api/build/:url', '\Codando\Route\Build:classe');

/*   DEMO
 * 	
 * 	$callogado = '\Codando\Route\Auth::logado';
 *  $calnotlogado = '\Codando\Route\Auth::notlogado';
 *
 * 	//LOGIN
 * 	 $app->get('/sair', '\Codando\Route\Auth:logout');
 * 	 $app->map('/login', $calnotlogado, '\Codando\Route\Cadastro:login')->via('GET','POST');
 * 	 $app->map('/esquece-senha', $calnotlogado, '\Codando\Route\Cadastro:esqueceSenha')->via('GET','POST');
 * 	 $app->map('/codigo-senha', $calnotlogado, '\Codando\Route\Cadastro:codigoSenha')->via('GET','POST');
 * 	 $app->map('/alterar-cadastro', $callogado, '\Codando\Route\Cadastro:update')->via('GET','POST');
 *   $app->get('/arquivos', '\Codando\Route\Download:listAll');
 *   $app->map('/cadastro-arquivo', '\Codando\Route\Download:insert')->via('GET','POST');
 *   $app->map('/editar-arquivo/:id', '\Codando\Route\Download:update')->via('GET','POST');
 *   $app->get('/deletar-arquivo/:id', '\Codando\Route\Download:deletar');
 */

$app->get('/noticias', '\Codando\Route\Noticia:listAll');
$app->get('/noticias/:url-:id', '\Codando\Route\Noticia:load');

$app->get('/fotos', '\Codando\Route\Foto:listAll');
$app->get('/fotos/:url-:id', '\Codando\Route\Foto:load');

$app->get('/videos', '\Codando\Route\Video:listAll');
$app->get('/videos/:url-:id', '\Codando\Route\Video:load');

$app->map('/contato', '\Codando\Route\Contato:index')->via('GET', 'POST');
$app->map('/trabalhe-conosco', '\Codando\Route\Trabalhe:index')->via('GET', 'POST');

$app->get('/ads/:id', '\Codando\Route\Banner:load');
$app->get('/bannerview', 'Codando\Route\Banner:mostrou');

$app->post('/newsletter', '\Codando\Route\Newsletter:insert');

$app->get('/:url-:id', '\Codando\Route\Pagina:load');

$app->get('/cep/:cep', '\Codando\Route\Cep:index');

$app->get('/cidades', '\Codando\Route\Cidade:listAll');
