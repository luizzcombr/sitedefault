<?php
    $config = include './_config/configuracao.php';
    $dominio = $config['sgc']['dominio']; //final e referencia da configuração
    $sgc_url = $config['sgc']['system']; // sistema de gerenciamento de site
    
    $sess_name = session_name();
	if (session_start()) {
		setcookie($sess_name, session_id(), null, '/', null, null, true);
	}

	$_SESSION['cacheoff'] = true;
    
    if (!isset($_SESSION['isIFrameSessionStarted']))
    {
        $_SESSION['isIFrameSessionStarted'] = 1;
        $redirect = rawurlencode('http://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
        header('Location: http://'. $sgc_url .'/start-session.php?dominio='. $dominio .'&redirect=' . $redirect);
        exit;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Sistema de Gerenciamento de Conteudo</title>
        <meta name="googlebot" content="noindex">
        <meta name="description" content="Sistema de gerenciamento de conteudo.">
        <meta name="robots" content="noindex,nofollow">
        <meta name="rating" content="general">
        <meta name="distribution" content="iu">
        <meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="expires" content="Sat, 26 Jul 1997 05:00:00 GMT">
        <meta http-equiv="cache-control" content="no-cache">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <style type="text/css"> html, body { margin: 0; padding: 0; height: 100%; } #bar { height: 0px; background: red; }iframe {position: absolute;top: 0; left: 0; width: 100%; height: 100%;border: none; padding-top: 0px; box-sizing: border-box; -moz-box-sizing: border-box; -webkit-box-sizing: border-box;}</style>
    </head>
    <body><iframe id="iframe" src="#"></iframe></body>
    <script type="text/javascript">
		var hash = location.hash;
        hash = hash.replace('#','');
		document.getElementById('iframe').src = 'http://<?= $sgc_url ?>/?dominio=<?= $dominio ?>&redirect=' + hash;
		window.addEventListener("message",function(a){"http://<?= $sgc_url ?>"===a.origin&&(location.hash=a.data)},!1);
	</script>
</html>