<?php

return array(
    'notperfil' => 'Profile not found.',
    'notlogado' => 'User not logged in',
    'login' => array(
        'selrecaptcha' => 'Select the recaptcha',
        'emailpass' => 'Email or password is incorrect.',
        'aguarde' => 'wait ...',
        'informe' => 'Enter email and password.'
    ),
    'insert' => array(
        'selrecaptcha' => 'Select the recaptcha',
        'allcampos' => 'All fields are mandatory(*)',
        'minpass' => 'Password must be more than 4 characters.',
        'isnick' => 'Nickname already used!',
        'not' => 'Could not register.',
        'yes' => 'Registration successful, wait ..'
    ),
    'update' => array(
        'allcampos' => 'All fields are mandatory(*)',
        'not' => 'Register unchanged',
        'yes' => 'Registration successfully changed'
    ),
    'recupera' => array(
        'invalidemail' => 'Registration not found.',
        'acesselink' => 'Follow the link to change your password',
        'casonao' => 'If you requested not to ignore this message .',
        'tituloemail' => 'Password recovery application',
        'assuntoemail' => 'Bíblia Book - Recover Password',
        'yes' => 'An email was sent to you with instructions.',
        'not' => 'It was not possible , try later.'
    ),
    'loadrecupera' => array(
    ),
    'updatesenha' => array(
        'invalidemail' => 'Registration not found.',
        'allcampos' => 'Enter new password',
        'confsenhadiff' => 'password confirmation is different from the password',
        'not' => 'Password has not been updated',
        'yes' => 'Password successfully changed'
    ),
    'bemvindo' => array(
        'titulo' => 'Bíblia Book - Welcome'
    ),
    'isexist' => array(
        'email' => 'There is already a record with this email.'
    )
);
