<?php

/*
 * LOGIN
 */

return array(
    'bemvindo' => 'Welcome to social network with bible.',
    'sobre' => 'Social network to share and relate to the Bible.',
    'title_idioma' => 'Select your language',
    'menu' => array(
      'sobre' => 'About',
      'idioma' => 'Language'
    ),
    'login' => array(
        'titulo' => 'Login',
        'email' => 'E-mail *',
        'senha' => 'Password *',
        'lembrar-me' => 'Remember me',
        'esquece-senha' => 'Forgot your password?',
        'esquece-senha-back' => 'I have my password!',
        'bt-recuperar' => 'To recover',
        'enviar' => 'Send'
    ),
    'cadastro' => array(
        'titulo' => 'Sign up',
        'nome' => 'Name *',
        'sobrenome' => 'Last name *',
        'username' => 'Nickname *',
        'email' => 'E-mail *',
        'telefone' => 'Phone',
        'senha' => 'Password *',
        'termo' => 'By creating an account , you automatically agree to our Terms and that you have read our Data Policy , including our Cookie Use.',
        'increva' => 'Send'
    )
);
