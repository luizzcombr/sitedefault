<?php

/*
 * LOGIN
 */

return array(
    'bemvindo' => 'Bem vindo a rede social com a bíblia.',
    'sobre' => 'Rede Social para compartilhar e relacionar com a bíblia.',
    'title_idioma' => 'Selecione seu idioma',
    'menu' => array(
      'sobre' => 'Sobre',
      'idioma' => 'Language'
    ),
    'login' => array(
        'titulo' => 'Entrar',
        'email' => 'E-mail *',
        'senha' => 'Senha *',
        'lembrar-me' => 'Lembrar-me',
        'esquece-senha' => 'Esqueceu sua senha?',
        'esquece-senha-back' => 'Tenho minha senha!',
        'bt-recuperar' => 'Recuperar',
        'enviar' => 'Entrar'
    ),
    'cadastro' => array(
        'titulo' => 'Novato? Inscreva-se',
        'nome' => 'Nome *',
        'sobrenome' => 'Sobrenome *',
        'email' => 'E-mail *',
        'username' => 'Apelido *',
        'telefone' => 'Celular',
        'senha' => 'Senha *',
        'termo' => 'Ao criar uma conta, você automaticamente concorda com nossos Termos e que você leu nossa Política de Dados, incluindo nosso Uso de Cookies.',
        'increva' => 'Inscreva - se'
    )
);
