<?php

return array(
    'notperfil' => 'Perfil não encontrado',
    'notlogado' => 'Usuario não logado',
    'login' => array(
        'selrecaptcha' => 'Selecione o recaptcha',
        'emailpass' => 'E-mail ou senha está incorreta.',
        'aguarde' => 'aguarde ...',
        'informe' => 'Informe e-mail e senha.'
    ),
    'insert' => array(
        'selrecaptcha' => 'Selecione o recaptcha',
        'allcampos' => 'Todos os campos são obrigatórios(*)',
        'minpass' => 'Senha deve conter mais de 4 caracteres.',
        'isnick' => 'Apelido já usado!',
        'not' => 'Cadastro não inserido.',
        'yes' => 'Cadastro realizado com sucesso, aguarde ..'
    ),
    'update' => array(
        'allcampos' => 'Informe os campos são obrigatórios(*)',
        'not' => 'Cadastro não alterado',
        'yes' => 'Seus dados foram alterados com sucesso'
    ),
    'recupera' => array(
        'invalidemail' => 'Cadastro não encontrado',
        'acesselink' => 'Acesse o link para alterar sua senha',
        'casonao' => 'Caso não solicitou ignore esta mensagem.',
        'tituloemail' => 'Pedido de recuperação de senha',
        'assuntoemail' => 'Bíblia Book - Recuperar Senha',
        'yes' => 'Um e-mail foi enviado para você com instruções.',
        'not' => 'Não foi possivel, tente mais tarde.'
    ),
    'loadrecupera' => array(
    ),
    'updatesenha' => array(
        'invalidemail' => 'Cadastro não encontrado.',
        'allcampos' => 'Informe a nova senha',
        'confsenhadiff' => 'Confirmação de senha está diferente da senha',
        'not' => 'Senha não foi atualizada',
        'yes' => 'Senha alterado com sucesso'
    ),
    'bemvindo' => array(
        'titulo' => 'Bíblia Book - Bem Vindo'
    ),
    'isexist' => array(
        'email' => 'Já existe um cadastro com este email.'
    )
);
