<?php

$_config_all = include './_config/configuracao.php';

$ip = NULL;

if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} elseif (!empty($_SERVER['REMOTE_ADDR'])) {
    $ip = $_SERVER['REMOTE_ADDR'];
}

define('USER_IP', $ip);

//START
ob_start();
ini_set("log_errors_max_len", "512");

if (USER_IP == "127.0.0.1") {
    ini_set("display_errors", 1);
	ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
} else {
    ini_set("display_errors", 0);
	ini_set('display_startup_errors', 0);
    error_reporting(0);
}

session_start();

use Codando\App;
	
if (USER_IP == "127.0.0.1") {
    $_SESSION['cacheoff'] = true;
}

// Diz para o PHP que estamos usando strings UTF-8 até o final do script
@mb_internal_encoding('UTF-8');

// Diz para o PHP que nós vamos enviar uma saída UTF-8 para o navegador
@mb_http_output('UTF-8');

ini_set('default_charset', $_config_all['php']['default_charset']);

foreach ((array) $_config_all['header'] as $h => $v)
    @header($h . ":" . $v);

setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Campo_Grande');

if (!defined('COD_DIR_ROOT')) {

    if (isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/bot|crawl|slurp|spider|face|google|yahoo|page speed insights|yandex|yeti|msn/i', $_SERVER['HTTP_USER_AGENT'])){
        define('GOOBOT', TRUE);
    } else {
        define('GOOBOT', FALSE);
    }

    define('COD_URL', $_config_all['dominio']['root']);
    define('COD_DIR_ROOT', str_replace('\\', '/', dirname(dirname(__FILE__))));
    define('COD_DIR_APP', COD_DIR_ROOT . '/app');
    define('COD_DIR_LOG', COD_DIR_ROOT . '/app/_log');
    define('COD_DIR_VENDOR', COD_DIR_ROOT . '/vendor');
	define('COD_DIR_CORE', COD_DIR_APP . '/_core');
	define('COD_DIR_CACHE', COD_DIR_ROOT . '/app/cache');
    define('SESSION_PATH', session_save_path());

    require_once(COD_DIR_VENDOR . '/autoload.php');

    require_once(COD_DIR_APP . '/_core/helps/functions.php');

    /* @var $app \Codando\App */
    $app = App::getInstance();

    //Carrega
    $app::getConfig();

    //Carrega app/_config/modulo.yml
    $app::getModels();
	
    $_config_site = $app::getConfig('dominio');

    define('COD_APP_CDN', $_config_all['dominio']['cdn']);
    define('COD_APP_404_IMG', $_config_all['dominio']['notfound_image']);
    define('COD_SESSIONAME', $_config_all['dominio']['session_name']);

    //DESCRYPT AND CSRF
    cryp()->inputsDec(); 

    define('COD_APP_MOBILE', FALSE);

    if (function_exists("set_time_limit") == TRUE AND @ ini_get("safe_mode") == 0) {
        @set_time_limit(100);
    }
}