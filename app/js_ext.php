<?php

error_reporting(0);
ini_set('session.use_cookies', '0');

switch ($_GET['t']){
    case 'facebook':
        $url = "https://connect.facebook.net/pt_BR/sdk.js";
        break;
    case 'ga':
        $url = "https://www.google-analytics.com/analytics.js";
        break;
    case 'jivosite':
        $widget = isset($_GET['widget']) ? $_GET['widget']:NULL;
        $url = "https://code.jivosite.com/script/widget/" . $widget;
        break;
}

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
$output = curl_exec($ch);
curl_close($ch);

$modified = filemtime(__FILE__);

$offset = 31536000; // Cache for 1 weeks

header('Expires: ' . gmdate("D, d M Y H:i:s", time() + $offset) . ' GMT');

if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) >= $modified) {

    @header('Accept-Encoding: gzip');
    @header('Vary: Accept-Encoding');
    @header("HTTP/1.0 304 Not Modified");
    @header('Content-type: text/css');
    @header('Cache-Control:');
    @header_remove('Set-Cookie');
    exit;
} else {

    header('Cache-Control: max-age=' . $offset);
    header('Content-type: application/javascript');
    header('Vary: Accept-Encoding');
    header('Pragma:');
    header("Last-Modified: " . gmdate("D, d M Y H:i:s", $modified) . " GMT");
    $etag = md5($modified);
    @header("Etag: " . $etag);
    @header_remove('Set-Cookie');
    ob_start();
    print $output;
    ob_end_flush();
}