<?php

require(dirname(__FILE__) . '/bootstrap.php');

@set_time_limit(-1);

$xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";
$xml .= "<urlset xmlns:news=\"http://www.google.com/schemas/sitemap-news/0.9\" xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\" xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9
                        http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">";
$xml .= "<url>
                <loc>" . $_config_site['root'] . $_config_site['path'] . "</loc>
                <changefreq>weekly</changefreq>
              </url>\r\n";

//Gera link da configuração
foreach ($_config_all['sitemap'] as $link) {

    $xml .= "<url>
            <loc>" . $_config_site['root'] . $_config_site['path'] . $link . "</loc>
            <changefreq>weekly</changefreq>
          </url>\r\n";
}

//Exemplo de Noticias
if(false){
    
/* @var $postList <Array>\Codando\Modulo\Noticia */
$postList = (array) app()->listModulo('noticia', NULL, 2000, "data DESC");


/* @var $postTemp \Codando\Modulo\Noticia */
foreach ($postList as $postTemp) {

    $post_url = $_config_site['root'] . '/noticias/' . $postTemp->getUrltitulo() . '-' . $postTemp->getId();
    list($dia, $mes, $ano) = explode("/", $postTemp->getData());
    $xmlImg = "";

    foreach ($postTemp->getArquivoList() as $imageTemp) {

        $xmlImg .= "<image:image><image:loc>" . $_config_site['root'] . $imageTemp->getImage(800, 600, $postTemp->getUrltitulo()) . "</image:loc></image:image>";
    }

    $news = "\r<news:news>\r\n".
            "\r<news:publication>\r\n".
              "\r<news:name>". $_config_site['dominio']['title'] ."</news:name>\r\n".
              "\r<news:language>pt-BR</news:language>\r\n".
           "\r</news:publication>\r\n".
            "\r<news:access>Registration</news:access>\r\n".
            "\r<news:genres>Blogue</news:genres>\r\n".
            "\r<news:publication_date>" . $ano . "-" . $mes . "-" . $dia . "</news:publication_date>\r\n".
            "\r<news:title>". $postTemp->getTitulo() ."</news:title>\r\n".
            "\r<news:keywords>". implode(", ", explode(" ", $postTemp->getTitulo())) ."</news:keywords>\r\n".
          "\r</news:news>";
    
    $xml .= "\r<url>\r\n";
    $xml .= "\t<loc>" . $post_url . "</loc>\r\n";
    $xml .= "\t" . $xmlImg;
    $xml .= "\t" . $news;
    $xml .= "\t<lastmod>" . $ano . "-" . $mes . "-" . $dia . "T05:00:00+00:00</lastmod>\r\n";
    $xml .= "\t<changefreq>monthly</changefreq>\r\n";
    $xml .= "\t</url>\n";
}

$postList = NULL;
unset($postList);
}

$xml .= "</urlset>";

ob_clean();
ob_start();

header("Content-Type:text/xml");
echo $xml;
