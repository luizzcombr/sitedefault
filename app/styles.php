<?php

$config = include './_config/configuracao.php';

error_reporting(0);

$files = $config['styles'];
    
$modified = 0;

foreach($files as $file) {        
    $age = filemtime($file);
    if($age > $modified) {
        $modified = $age;
    }
}

//$offset = 60 * 60 * 24 * 7; // Cache for 1 weeks

$offset = 31536000; // Cache for 1 weeks

header ('Expires: ' . gmdate ("D, d M Y H:i:s", time() + $offset) . ' GMT');

if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) >= $modified) {
    
    @header('Accept-Encoding: gzip');
    @header('Vary: Accept-Encoding');
    @header("HTTP/1.0 304 Not Modified");
    @header ('Content-type: text/css');
    @header ('Cache-Control:');
    
} else {
    
    header ('Cache-Control: max-age=' . $offset);
    header ('Content-type: text/css');
    header('Vary: Accept-Encoding');
    header ('Pragma:');
    header ("Last-Modified: ".gmdate("D, d M Y H:i:s", $modified )." GMT");
    $etag = md5($modified);
    @header("Etag: ".$etag); 
    
    function compress($buffer) {
        /* remove comments */
        $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
        /* remove tabs, spaces, newlines, etc. */
        $buffer = str_replace(array("\r\n","\r","\n","\t",'  ','    ','     '), '', $buffer);
        /* remove other spaces before/after ; */
        $buffer = preg_replace(array('(( )+{)','({( )+)'), '{', $buffer);
        $buffer = preg_replace(array('(( )+})','(}( )+)','(;( )*})'), '}', $buffer);
        $buffer = preg_replace(array('(;( )+)','(( )+;)'), ';', $buffer);
        
        if( strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') === TRUE ) {

            // gzip the response and prepend the gzip header
            $gz_output = "\x1f\x8b\x08\x00\x00\x00\x00\x00".gzcompress( $buffer, 9);
 
            // purge the output buffer
            //ob_clean();

            $buffer = $gz_output;

        }
        
        return $buffer;
    }

    ob_start('ob_gzhandler');

    foreach($files as $file) {
        if(strpos(basename($file),'.min.')===false) { //compress files that aren't minified
            ob_start("compress");
                include($file);
            ob_end_flush();
        } else {
            include($file);
        }
    }
    
    if( strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') === TRUE ) {
        
         // gzip the response and prepend the gzip header
            $gz_page = ob_get_contents();
            $gz_output = "\x1f\x8b\x08\x00\x00\x00\x00\x00".gzcompress($gz_page, 9);
            $gz_content_length = strlen($gz_page);

            // purge the output buffer
            ob_clean();

            @header('Content-Length: '.$gz_content_length);
            @header('Content-Encoding: gzip');

            print $gz_output;
    }
    
    ob_end_flush();
}