<!DOCTYPE html>
<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7" lang="pt-br"><![endif]-->
<!--[if IE 7]><html class="lt-ie9 lt-ie8" lang="pt-br"><![endif]-->
<!--[if IE 8]><html class="lt-ie9" lang="pt-br"><![endif]-->
<!--[if gt IE 8]><!--><html lang="pt-br"><!--<![endif]-->
    <head>
        <meta charset="UTF-8">
        <title></title>
        <meta name="description" content="">
        <link rel="stylesheet" type="text/css" href="../css/font.css">
        <link rel="stylesheet" type="text/css" href="../css/reset.css">
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <link rel="stylesheet" type="text/css" href="../css/normalize.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" sizes="57x57" href="../img/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="../img/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="../img/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="../img/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="36x36" href="../img/android-icon-36x36.png">
        <link rel="apple-touch-icon" sizes="48x48" href="../img/android-icon-48x48.png">
        <link rel="apple-touch-icon" sizes="72x72" href="../img/android-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="96x96" href="../img/android-icon-96x96.png">
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="../img/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="../img/favicon-16x16.png">
        <link rel="apple-touch-icon" sizes="114x114" href="../img/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="114x114" href="../img/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="../img/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="../img/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="../img/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="../img/apple-icon-180x180.png">
        <link rel="apple-touch-icon" sizes="144x144" href="../img/android-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="192x192" href="../img/android-icon-192x192.png">
        <link rel="apple-touch-icon" sizes="144x144" href="../img/android-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="192x192" href="../img/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="../img/android-icon-192x192.png">
    </head>
    <body>
        <!--TOP-->

