define('form', ['jquery', 'ajax', 'loadcep', 'loadcidade', 'tools', 'mask', 'fileuploader'], function ($, ajax, loadcep, loadcidade) {

    var feedbackLang = {
        'pt': [
            "Preencha os campos obrigatórios(*), corretamente!",
            "<img src='/img/load_d.GIF' style='margin-right: 15px;' /> Enviando, aguarde...",
            "Enviado com sucesso. Obrigado pelo interesse em breve retornaremos.",
            "Desculpe, não foi possível enviar. Tente novamente..."
        ],
        'pt-br': [
            "Preencha os campos obrigatórios(*), corretamente!",
            "Enviando, aguarde...",
            "Enviado com sucesso. Obrigado pelo interesse em breve retornaremos.",
            "Desculpe, não foi possível enviar. Tente novamente..."
        ],
        'en': [
            "Complete the required (*) fields correctly!",
            "Sending wait ...",
            "Sent. Thanks for the interest will soon return.",
            "Sorry, could not send. Try again ..."
        ],
        'es': [
            "Preencha os campos obrigatórios(*), corretamente!",
            "Envío de espera ...",
            "Enviados. Gracias por el interés volverán pronto.",
            "Lo sentimos, no se pudo enviar. Inténtalo de nuevo ..."
        ]
    };

    var enviando = false;
    var lg = window.lang;

    var feedback = feedbackLang[lg];

    function valid(self) {

        var validate = true;

        $("input[required],textarea[required],input.required,textarea.required,select[required]", self)
                .filter('[type="email"]')
                .each(function () {

                    var inpSelf = $(this);
                    inpSelf.parent().removeClass('error');
                    var reg = new RegExp(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/);

                    if (inpSelf.val() != '' && reg.test(inpSelf.val()) == false) {

                        validate = false;
                        inpSelf.addClass('erro');
                        inpSelf.focus();
                    }

                    reg = undefined;
                })
                .end()
                .each(function () {

                    if ($.trim($(this).val()) == '' || $(this).val() == '') {

                        validate = false;
                        $(this).addClass('erro');
                    }
                });

        return validate;
    }

    function upload(self) {

        var _self = $(self);
        var inptFileList = _self.find('.file-uploader[data-name]');
        var msg = self.find("#success");

        _self.on('click', '[data-arquivo][data-href] .file-uploader', function () {
            window.open($(this).parent().attr('data-href'));
        });

        inptFileList.each(function () {

            var inptFile = $(this);
            var ismult = inptFile.attr('data-mult');
            var upappend = inptFile.attr('data-append');

            if (qq != undefined && qq.hasOwnProperty('FileUploader') && inptFile.length == 1 && inptFile.attr('data-name')) {

                var inptHidd = $('<input type="hidden" name="' + inptFile.attr('data-name') + '" value=""/>');
                var uploaded = $("#" + inptFile.attr('data-name') + "-uploader");

                if (ismult == undefined)
                    _self.append(inptHidd);


                var uploader = new qq.FileUploader({
                    element: inptFile.get(0),
                    action: _self.data().upload,
                    inputName: "arquivotemp",
                    uploadButtonText: '<span style="width: 100%;display: inline-block;">' + inptFile.attr('data-text') + '</span>',
                    multiple: false,
                    onSubmit: function (id, fileName) {
                        inptFile.trigger('submitFile');
                        uploaded.addClass('loading').removeAttr('hidden');
                        uploaded.find('.label').text('Enviando...');
                    },
                    onProgress: function (id, fileName, loaded, total) {

                        uploaded.find('.label').html('Carregando ' + (total / (loaded * 100)));
                        uploaded.attr('data-loader', loaded);
                    },
                    onComplete: function (id, fileName, responseJSON) {

                        if (responseJSON.hasOwnProperty('filename')) {

                            var name = responseJSON.filename;

                            if (name.length > 13) {

                                name = name.substr(0, 5) + '..' + name.substr(-5);
                            }

                            uploaded.removeClass('loading').addClass('upload-temp');
                        }

                        if (ismult !== undefined) {

                            uploaded.find('.label').html(inptFile.attr('data-text'));

                        } else {

                            uploaded.find('.label').html(name + ' - ' + ((uploaded.attr('data-loader') / 1024) / 1024).toFixed(1) + 'MB');
                            inptHidd.val(responseJSON.filename);
                        }

                        inptFile.trigger('completeFile');
                        uploaded.removeAttr('data-loader');

                    },
                    onCancel: function (id, fileName) {

                        //ploaded.find('span.text').text('Anexar');
                    },
                    onError: function (id, fileName, xhr) {
                        inptFile.trigger('completeFile');
                        uploaded.find('.label').text('Error!');

                    },
                    showMessage: function (message) {

                        uploaded.find('.label').text(message || 'Error!');

                    }
                });

            }
        });
    }

    return function () {

        var self = $(this), rtd = false;

        if (self.length > 0) {

            if (self.data().hasOwnProperty('upload')) {

                upload(self);
            }

            if ($.fn.mask) {

                $('[data-mask]', self).each(function () {
                    $(this).mask($(this).attr('data-mask'));
                });

                var SPMaskBehavior = function (val) {
                    return val.replace(/\D/g, '').length === 11 ? '(00)00000-0000' : '(00)0000-00009';
                },
                        spOptions = {
                            onKeyPress: function (val, e, field, options) {
                                field.mask(SPMaskBehavior.apply({}, arguments), options);
                            }
                        };

                $('[data-celular]').mask(SPMaskBehavior, spOptions);

                $('[data-decimal]').mask("#.##0,00", {reverse: true});

                $('[data-km]').mask("#.##0.000", {reverse: true});
            }


            if (self.find('#cep').length == 1) {
                self.find('input#cep').on('change', loadcep);
            }
            
            if (self.find('#estado').length == 1) {
                self.find('input#estado').on('change', loadcidade);
            }

            jQuery("form input,form textarea").on("invalid", function (event) {
                if (event.type == 'invalid') {
                    $(event.currentTarget).addClass('erro');
                }
            });

            self.submit(function (e) {

                if (e.preventDefault)
                    e.preventDefault();

                if (enviando === false) {

                    enviando = true;

                    var msg = (self.attr('data-feedback') ? $(self.attr('data-feedback')) : self.find(".success,.feedback")), validate = valid(self);

                    if (validate === false) {

                        msg.html(feedback[0]).removeAttr('hidden');

                    } else {

                        var options = {
                            url: self.attr('data-action'),
                            beforeSend: function () {

                                msg.removeAttr('style').html(feedback[1]).removeAttr('hidden');

                            },
                            success: function (data) {

                                if (data.status == true) {

                                    msg.removeAttr('style').html(data.msg || feedback[2]).removeAttr('hidden');


                                    if (data.redir) {
                                        setTimeout(function () {
                                            location.href = (data.redir);
                                            location.replace(data.redir);
                                        }, 2500);

                                    } else if (self.attr('data-reset') !== 'false')
                                        self.get(0).reset();

                                } else {

                                    msg.attr('style', 'color:red;font-weight:600;').html((data.msg || feedback[3])).removeAttr('hidden');
                                }


                                enviando = false;

                                return false;
                            },
                            dataType: 'json',
                            data: self.serializeArray(),
                            context: self
                        };

                        ajax(options).error(function () {
                            enviando = false;

                            msg.html((feedback[3])).removeAttr('hidden');

                        });

                        options = undefined;
                    }
                }

                return false;
            });

            rtd = true;
        }

        $(".select-personalizado select").change(function () {
            var valor = $(this).find("option:selected").text();
            $(this).parent().find(".label").text(valor);
        }).change();

        return rtd;
    };

});