define(function(){
    var self;

    function loadCEP() {
		self = $(this).closest('form');
        var cep = this.value;

        if (cep.length > 5 && self.find('#endereco').val() == "") {

            $.ajax({
                url: '/cep/' + cep,
                dataType: 'json',
                crossDomain: true,
                contentType: "application/json",
                statusCode: {
                    200: function (data) {
            
                        if (data.hasOwnProperty('cidade')){
                            self.find('#cidade option:contains("' + (data.cidade) + '")').attr('selected', 'selected');
                            self.find('#cidade').change();
                        }

                        if (data.hasOwnProperty('logradouro'))
                            self.find('#endereco').val(data.logradouro);

                        if (data.hasOwnProperty('bairro'))
                            self.find('#bairro').val(data.bairro);

                        if (data.hasOwnProperty('estado')) {
                            self.find('#estado').find('option[data-sigla^="' + data.estado + '"]').attr('selected', 'selected');
                        }

                        self.find("#numero").focus();
                    }
                    , 400: function (msg) {
                        console.log(msg);
                    } // Bad Request
                    , 404: function (msg) {
                        console.log("CEP não encontrado!!");
                    } // Not Found
                }
            });
        }
    }

    
    return loadCEP;
})