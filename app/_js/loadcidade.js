define(function () {
    var self;

    function listcidade() {

        var estado = $('option:selected', this).val();
        var cidade = $('#cidade');

        cidade.empty();

        cidade.append('<option value="">Aguarde..</option>');

        $.getJSON('/cidades', {'estado': estado}, function (cidades) {
            cidade.empty();
            if (cidades && cidades.list) {
                $.each(cidades.list, function () {
                    cidade.append('<option value="' + this.id + '">' + this.nome + '</option>');
                });
            } else {
                cidade.append('<option value="">Não foi possivel carregar</option>');
            }
        });
    }

    return listcidade;
});