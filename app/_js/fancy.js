define(function () {

    var html = '<div class="fancy" style="display: block; transition: opacity 200ms; opacity: 1;"> <p class="botao-fechar" data-ix="fechar-fancy">X</p> <div class="form-fancy w-form"></div> <div class="fundo-fechar" data-ix="fechar-fancy"></div></div>';

    function open(contend) {

        var newfancy = $(html);

        newfancy.find('.form-fancy').html(contend);

        $('body').append(newfancy);
        
        newfancy.find('.botao-fechar').click(function () {
            newfancy.fadeOut(function () {
                newfancy.remove();
            });
        });
        
        return {
            'close': function () {
                newfancy.fadeOut(function () {
                    newfancy.remove();
                });
            }
        };
    }

    return function () {

        return {
            'open': open
        };

    };

});