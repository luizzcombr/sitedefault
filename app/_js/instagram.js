define(function(){
    var self;
    
    return function() {
        self = $(this);
        
        $.getJSON('/api/instagram', function(info){
            if(info && info.hasOwnProperty('bfmsoficial') && info.bfmsoficial.hasOwnProperty('user')){
                var imgs = [];
                $.each(info.bfmsoficial.user.edge_owner_to_timeline_media.edges, function(){
                    imgs.push('<a href="https://www.instagram.com/p/'+this.node.shortcode+'/" target="_blank"><img src="'+this.node.thumbnail_resources[0].src+'" alt="" class="image-4"></a>');
                });
                self.find('.fotos-insta .mascara').html(imgs.join(''));
            }
        });
        
    };
});