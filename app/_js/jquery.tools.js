
define('tools', ['jquery'], function ($) {

    $.fn.goScroll = function () {
        var _self = this;
        if (_self.closest('div[role="dialog"].modal').length == 0)
            $('html, body').stop(true, true).animate({
                scrollTop: _self.offset().top - 20
            }, 500);
        return _self;
    }

    $.fn.toggleHidden = function () {

        return (this.attr('hidden') !== undefined ? this.revomeAttr('hidden') : this.attr('hidden', ''));

    };
    /**
     window.alert = function(a, s) {
     s = s != undefined ? s : ($("#al er t - info-erro").length == 0 ? $("div:first").prepend('<div id = "alert-info-erro"/>') : $("div:first")).find("#alert-info-erro");
     $(s).html('<div class="alert"><button type="button" class="close" data-dismis s = " alert">×</button> ' + a + ' </div>').goScroll().fadeIn();
     }
     
     window.info = function(i, s) {
     s = s != undefined ? s : ($("div:first").find("#al er t - info-erro").length == 0 ? $("div:first").prepend('<div id = "alert-info-erro"/>') : $("div:first")).find("#alert-info-erro");
     $(s).html('<div class="alert alert-info"><button type="button" class="close" data-dismis s = " alert">×</button> ' + i + ' </div>').goScroll().fadeIn();
     }
     
     window.error = function(e, s) {
     s = s != undefined ? s : ($("div:first").find("#al er t - info-erro").length == 0 ? $("div:first").prepend('<div id = "alert-info-erro"/>') : $("div:first")).find("#alert-info-erro");
     $(s).html('<div class="alert alert-error"><button type="button" class="close" data-dismis s = " alert">×</button> ' + e + ' </div>').goScroll().fadeIn();
     }
     
     
     * Função transforma elemento.id, id do elemento no padrao camelCase
     */
    $.fn.camelCase = function (type) {
        var self = $(this).get(0);
        if (typeof self !== "undefined") {
            camelCase = self.id.split(/-|_/);
            $.each(camelCase, function (i, l) {
                if (type != 'lower' || (type == 'lower' && i !== 0)) {
                    camelCase[i] = (l + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
                        return $1.toUpperCase();
                    });
                }
            });
            return camelCase.join('');
        } else {
            return undefined;
        }
    };

    /**
     * Função obter elementos
     */
    var el = function () {

        var self = $(this), _elementos = {}, _ars = arguments;
        _elementos[self.camelCase('lower')] = self;
        self.find(_ars.lenght > 0 && typeof (_ars[0]) == "string" ? _ars[0 ] : '*').each(function () {
            if (this.id) {
                _elementos[$(this).camelCase('lower')] = $(this);
            }
        });
        for (var extra in  _ars) {
            if (extra > 0) {
                var _ext = $(_ars[1 ]);
                if (_ext.length == 1) {
                    _elementos[_ext.camelCase('lower')] = _ext;
                } else if (_ext.length > 1) {
                    _elementos['list' + _ext.camelCase()] = _ext;
                }
                delete _ext;
            }

        }

        return _elementos;
    };
    $.fn.el = el;
});