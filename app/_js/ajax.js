define('ajax', ['jquery'], function($) {
    
    $(document).ajaxError(function(event, XMLHttpRequest, ajaxOptions, thrownError) {
        log(thrownError);
    });

   return function (configuracao) {

        var _config = {
            url: null,
            data: {},
            success: null,
            dataType: 'json',
            context: null,
            type: 'POST',
            cache: false
        };

        _config = $.extend(_config, (typeof configuracao == "object" ? configuracao : arguments));

        var xhr = $.ajax(_config);

            xhr.done(function(data) {
                    if (this.tagName == 'form') {
                        $(this).trigger('submitdone', data);
                    }
                })
                .fail(function(textStatus) {
                    if (this.tagName == 'form') {
                        $(this).trigger('submitfail', textStatus);
                    }
                })
                .always(function(data) {
                    if (this.tagName == 'form') {
                        $(this).trigger('submitalways', data);
                    }
                });

        _config = undefined;

        return xhr;
    };

});