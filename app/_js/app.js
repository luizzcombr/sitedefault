define('app', ['jquery'], function () {

    var jQuery = $ = require('jquery'),
            el = {
                datasrc: jQuery("img[data-original]"),
                fancy: jQuery("a[data-rel^='fancybox'],a[data-rel].fancybox"),
                load: jQuery("[data-controller]")
            },
    init = {};

    init.data = function () {

        if (el.datasrc) {

            function loadImage(el, fn) {
                var src = el.getAttribute('data-original');
                if (src) {
                    var img = new Image();
                    img.onload = function () {
                        if (!!el.parent)
                            el.parent.replaceChild(img, el);
                        else
                            el.src = src;

                        el.removeAttribute('data-original');

                        fn ? fn() : null;
                    };
                    img.src = src;
                }
            }
            ;

            function elementInViewport(el) {
                var rect = el.getBoundingClientRect();

                return (
                        rect.top >= 0
                        && rect.left >= 0
                        && rect.top <= (window.innerHeight || document.documentElement.clientHeight)
                        );
            }

            var images = new Array()
                    , processScroll = function () {
                        for (var i = 0; i < images.length; i++) {
                            if (elementInViewport(images[i])) {
                                loadImage(images[i], function () {
                                    images.splice(i, i);
                                });
                            }
                        };
                        if (images.length === 0) {
                            $(window).off('DOMContentLoaded load resize scroll');
                        }
                    };

            // Array.prototype.slice.call is not callable under our lovely IE8 
            el.datasrc.each(function () {
                images.push(this);
            });


            processScroll();

            $(window).off('DOMContentLoaded load resize scroll').on('DOMContentLoaded load resize scroll', processScroll);

        }
    };

    init.frame_breaker = function () {
        if (window.location.host !== window.top.location.host) {
            window.top.location.host = window.location.host;
        }
    };

    init.fancybox = function () {

        if (el.fancy.length) {
            //fancybox.iframe
            el.fancy.filter('[href*="youtube.com"]').each(function () {

                var i = $(this);

                var url = i.attr('href').replace(/(>|<)/gi, '').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
                if (url[2] !== undefined) {
                    var ID = url[2].split(/[^0-9a-z_]/i);
                    ID = ID[0];

                    i.addClass('fancybox.iframe');
                    i.attr('href', 'http://www.youtube.com/embed/' + ID);
                }
                else
                    i.attr('target', '_blank');

            });

            el.fancy.filter('[data-rel]').each(function () {

                $(this).attr('rel', $(this).attr('data-rel'));
            });

            require(['fancybox'], function () {

                if (jQuery.fn.fancybox) {

                    el.fancy.fancybox({
                        padding: 10,
                        margin: [20, 60, 20, 60],
                        helpers: {
                            title: {
                                type: 'inside',
                                position: 'top'
                            }
                        }
                    });


                }

            });

        }
    };

	init.jserro = function(){
			
		var ifrm = document.createElement("iframe");
				ifrm.setAttribute("src", "/jserro");
				ifrm.style.width = "0px";
				ifrm.style.height = "0px";
				document.body.appendChild(ifrm);

		var jsErro = (function(iframe){
			function send(msg){
				iframe.src = iframe.src.split('?')[0] + '?jserro=' + msg;
			}
			return {'send':send};
		}(ifrm));

		window.onerror = function (msg, file, line, col, error) {
			jsErro.send( msg +' file:' + file + '@' + line + ':' + col +' URL ' + encodeURI(location.search + location.hash) );
		};
	}
	
    function construct() {

        el = {
            fancy: jQuery("a[data-rel^='fancybox'],a[data-rel].fancybox"),
            load: jQuery("[data-controller]")
        };

        for (var i in init) {

            try {
                init[i]();

            } catch (err) {

                log("Erro no app.init." + i + " " + "Descricao do erro: " + err.description + "Exception: " + err);
            }
        }

        if ((el.load instanceof jQuery) === true && el.load.length) {

            el.load.each(function () {

                var _contex = this;

                var reqSplit = jQuery(_contex).attr('data-controller').split('|');

                $.each(reqSplit, function (i, item) {
                    var req = item;
                    require([req], function (_func) {

                        try {
                            if (_func.call)
                                _func.call(_contex);
                            var r = _contex.removeAttribute('data-controller');
                        } catch (e) {

                            console.log(req + ' Try Error =>', e);
                            _contex.setAttribute('error', e);
                        } finally {

                            console.log(req + ' finally =>', r);
                            _contex.setAttribute('finally', r);
                        }

                    });
                });


            });
        }
		
        if (typeof(window.performance) === "object" && typeof(ga) === "function") {
            var timeSincePageLoad = Math.round(performance.now());
            ga('send', 'timing', 'JS Dependencies', 'load', timeSincePageLoad);
        }
    }

    return function () {

        construct();
    };

});