define(['jquery.touchswipe'], function () {

    return function () {

        var conf = {
            player: $(this),
            lista: {},
            itens: 0,
            nItens: 0,
            larguraItem: 0,
            loadImg: true,
            isTime: true
        }, dados = {
            atual: 0,
            carregados: 0
        };

        conf.lista = conf.player.find('[data-list]');

        conf.loadImg = conf.player.data().hasOwnProperty('notloadimg') === true ? false : true;
        
        conf.isTime = conf.player.data().hasOwnProperty('nottime') === true ? false : true;
        
        if (conf.lista)
            conf.itens = conf.lista.find('[data-item]');

        conf.nItens = conf.itens.length;

        if (conf.itens)
            conf.larguraItem = parseInt(conf.itens.eq(0).width());

        if (conf.player) {
            conf.btAnte = conf.player.find('[data-prev]');
            conf.btProx = conf.player.find('[data-next]');
            conf.listaController = conf.player.find('[data-paginacao]');
        }

        function showBts() {
            if (conf.btProx && conf.btAnte) {
                conf.btAnte.css('visibility', 'visible');
                conf.btProx.css('visibility', 'visible');
            }
        }

        function hideBts() {
            if (conf.btProx && conf.btAnte) {
                conf.btAnte.css('visibility', 'hidden');
                conf.btProx.css('visibility', 'hidden');
            }
        }

        function animation() {

            if (dados.carregados >= conf.nItens) {

                buildController();

                conf.itens.eq(0).fadeIn().removeClass('hide');

                refreshController();
				
				if ($.fn.swipe) {
                        conf.lista.swipe({
                            swipeLeft: function (event, direction, distance, duration, fingerCount, fingerData) {
                                showPrev();
                            },
                            swipeRight: function (event, direction, distance, duration, fingerCount, fingerData) {
                                showNext();
                            },
                            threshold: 0
                        });
                    }
				
                if (conf.nItens > 1) {
                    showBts();
                    setTimer();
                    bindBts();
                } else {
                    clearTimer();
                    unbindBts();
                    hideBts();
                }
            }
        }

        function refreshController() {
            if (conf.listaController && conf.btsController) {
                conf.listaController.find('.ativo').removeClass('ativo');
                conf.btsController.eq(dados.atual).addClass('ativo');
            }
        }

        function effect(indice, sentido) {

            var dEsq = 0, atual, prox;

            if (indice != dados.atual) {


                unbindBts();
                clearTimer();

                atual = conf.itens.eq(dados.atual);
                prox = conf.itens.eq(indice);

                prox.css('z-index', 8).show().removeClass('hide');

                atual.fadeOut(function () {
                    prox.css('z-index', 9);

                    dados.atual = indice;

                    refreshController();

                    setTimer();
                    bindBts();

                });

            }
        }

        function setTimer() {
            if(conf.isTime)
            dados.timeout = setTimeout(showNext, 8000);
        }

        function clearTimer() {
            clearTimeout(dados.timeout);
        }

        function showNext() {
            if (dados.atual + 1 < conf.nItens) {
                effect(dados.atual + 1);
            } else {
                effect(0);
            }
        }

        function showPrev() {
            if (dados.atual - 1 >= 0) {
                effect(dados.atual - 1, true);
            } else {
                effect(conf.nItens - 1, true);
            }
        }

        function startLoad() {

            var images = new Array();

            if (conf.itens){
                conf.itens.each(function () {
                        console.log($(this).children().length);
                    if($(this).children().length == 0){
                         console.log($(this).children().length);
                        $(this).remove();
                    }
                    
                    var imagens = this.src ? $(this) : $(this).find('img');

                    if (conf.loadImg === true) {
                        imagens.each(function () {
                            var imagem = $(this);
                            var ilink = imagem.attr('src');

                            var imagescache = new Image();
                            imagescache.src = ilink;

                            imagescache.onload = function () {

                                imagem.attr('src', imagescache.src);
                                dados.carregados++;
                                animation();
                            };
                        });
                    } else {
                        dados.carregados++;
                        animation();
                    }

                });
                
                conf.itens = conf.lista.find('[data-item]');
                conf.nItens = conf.itens.length;
            }
        }

        function unbindBts() {

            if (conf.btProx && conf.btAnte) {
                conf.btProx.off('click');
                conf.btAnte.off('click');
            }

            if (conf.btsController)
                conf.btsController.off('click');

        }

        function bindBts() {
            if (conf.btProx && conf.btAnte) {
                conf.btProx.off('click').on('click', showNext);
                conf.btAnte.off('click').on('click', showPrev);
            }


            if (conf.btsController && conf.itens.length > 1)
                conf.btsController.off('click').on('click', function () {

                    var indice = parseInt($(this).attr('data-index'));

                    var sentido;

                    if (indice > dados.atual) {
                        sentido = true;
                    } else {
                        sentido = false;
                    }

                    effect(indice, sentido);

                });
        }

        function buildController() {
            conf.btsController = conf.listaController.find('a');

            if (conf.listaController && conf.btsController) {

                if (conf.itens.length > 1) {
                    conf.listaController.html('');
                    conf.itens.each(function () {
                        var _iten = $(this);

                        var index = _iten.index();
                        conf.listaController.append('<a data-index="' + index + '" class="pg"> <i></i>' + index + '</a>');
                    });

                    conf.btsController = conf.listaController.find('a');
                } else {

                    conf.listaController.hide().css('visibility', 'hidden');
                }

            }
        }

        startLoad();
    };
});