define('login', function () {
    var self;
    
    function loginFinal(r) {

        FB.api('/me?fields=id,name,email,permissions,read_custom_friendlists', function (response) {

            $.post('/action/facelogin', {'code': r.authResponse.accessToken}, function (data) {
                //console.log(data);
            });

            //console.log(response, r);
        }, {scope: 'email'});
    }

    return function () {
        self = $(this);
 
//        var recaptchalogin;
//        var recaptchacadastro;
//        var recaptchacall = function () {
//
//            recaptchalogin = grecaptcha.render('recaptchalogin', {
//                'sitekey': '6LdLphATAAAAAGEaTTDRJMf05OkZ0xB7pynzth1I',
//                'theme': 'light',
//                'lang': window.lang
//            });
//
//            recaptchacadastro = grecaptcha.render('recaptchacadastro', {
//                'sitekey': '6LdLphATAAAAAGEaTTDRJMf05OkZ0xB7pynzth1I',
//                'theme': 'light',
//                'lang': window.lang
//            });
//        };
//        window.recaptchacall = recaptchacall;

        var self = $(this);

        $('#esqueci-senha').click(function(e){
            
            e.preventDefault();

            if ($(this).hasClass('add-cadstro') == false) {

                self.find('#email').attr('name', 'recemail');
                self.find('.btn-login').text(self.find('.btn-login').attr('data-recuperar'));
                self.find('.lembrar').remove();
                self.find('#senha').css('display', 'none').removeAttr('required').attr('disabled', true);
                $(this).addClass('add-cadstro');
            } else {
                location.hash = '';
                location.href = '/';
            }

            return false;
        });

        if (location.hash == '#esqueci-senha') {
            self.find('#esqueci-senha').click();
        }

        $("#btn-facebook").click(function () {
            window.FB.getLoginStatus(function (response) {
                if (response.status === 'connected') {
                    // Logged into your app and Facebook.

                    console.log(response);
                    loginFinal(response);


                } else if (response.status === 'not_authorized') {
                    // The person is logged into Facebook, but not your app.
                    console.log('Please log ' +
                            'into this app.');
                } else {
                    // The person is not logged into Facebook, so we're not sure if
                    // they are logged into this app or not.
                    console.log('Please log ' +
                            'into Facebook.');
                }
            });

        });

        window.fbAsyncInit = function () {
            FB.init({
                appId: '912937708773577',
                cookie: true, // enable cookies to allow the server to access 
                xfbml: true, // parse social plugins on this page
                version: 'v2.2' // use version 2.2
            });
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/pt_BR/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        
//        (function (d, s, id) {
//            var js, fjs = d.getElementsByTagName(s)[0];
//            if (d.getElementById(id))
//                return;
//            js = d.createElement(s);
//            js.id = id;
//            js.src = "//www.google.com/recaptcha/api.js?onload=recaptchacall&render=explicit";
//            fjs.parentNode.insertBefore(js, fjs);
//        }(document, 'script', 'recaptcha-jssdk'));

    };

});


