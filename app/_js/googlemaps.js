define('googlemaps', function() {

    return function() {
        var self = $(this);
        var latlong = self.attr('data-latlong').split(',');
        var myLatlng = new google.maps.LatLng(latlong[0], latlong[1]);
        var mapOptions = {
            zoom: 14,
            center: myLatlng
        };
        var map = new google.maps.Map(self.get(0), mapOptions);

        var marker = new google.maps.Marker({
            position: myLatlng,
            title: self.attr('data-title')
        });

        // To add the marker to the map, call setMap();
        marker.setMap(map);
    }
});