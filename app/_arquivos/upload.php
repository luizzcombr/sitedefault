<?php

set_time_limit(1800); // 30 minutos
ini_set("max_input_time", "1800"); // 30 Minutos
ini_set("post_max_size", "50"); // 50 Mb

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers:Origin, X-Requested-With, XMLHttpRequest, Content-Type, Accept, Authorization, x-file-name, x-mime-type');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');

class qqUploadedFileXhr {

    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
    function save($path) {

        $input = fopen("php://input", "r");
        $temp = tmpfile();
        $realSize = stream_copy_to_stream($input, $temp);
        fclose($input);

        if ($realSize != $this->getSize()) {
            return false;
        }

        $target = fopen($path, "w");
        fseek($temp, 0, SEEK_SET);
        stream_copy_to_stream($temp, $target);
        fclose($target);

        return true;
    }

    function getName() {
        return $_GET['arquivotemp'];
    }

    function getSize() {
        if (isset($_SERVER["CONTENT_LENGTH"])) {
            return (int) $_SERVER["CONTENT_LENGTH"];
        } else {
            throw new Exception('Getting content length is not supported.');
        }
    }

}

class qqUploadedFileForm {

    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
    function save($path) {
        if (!move_uploaded_file($_FILES['arquivotemp']['tmp_name'], $path)) {
            return false;
        }
        return true;
    }

    function getName() {
        return $_FILES['arquivotemp']['name'];
    }

    function getSize() {
        return $_FILES['arquivotemp']['size'];
    }

}

class qqFileUploader {

    private $allowedExtensions = array();
    private $forbiddenExtensions = array("php", "phps", "exe", "bat", "js", "msi", "sh", "php3", "php4", "shtml");
    private $sizeLimit = 52428800;
    private $file;

    function __construct(array $allowedExtensions = array(), $sizeLimit = 52428800) {

        $allowedExtensions = array_map("strtolower", $allowedExtensions);

        $this->allowedExtensions = $allowedExtensions;
        $this->sizeLimit = $sizeLimit;

        if (isset($_GET['arquivotemp'])) {
            $this->file = new qqUploadedFileXhr();
        } elseif (isset($_FILES['arquivotemp'])) {
            $this->file = new qqUploadedFileForm();
        } else {
            $this->file = false;
        }
    }

    /**
     * Returns array('success'=>true) or array('error'=>'error message')
     */
    function handleUpload($uploadDirectory, $replaceOldFile = FALSE) {

        if (!is_writable($uploadDirectory)) {
            return array('error' => "Erro no servidor. Diretório de upload não permite escrita.");
        }

        if (!$this->file) {
            return array('error' => 'Nenhum arquivo foi enviado.');
        }

        $size = $this->file->getSize();

        if ($size == 0) {
            return array('error' => 'O arquivo está vazio');
        }

        if ($size > $this->sizeLimit) {
            return array('error' => 'O arquivo é muito grande');
        }

        $pathinfo = pathinfo($this->file->getName());
        $filename = $pathinfo['filename'];
        //$filename = md5(uniqid());
        $ext = $pathinfo['extension'];

        if (in_array(strtolower($ext), $this->forbiddenExtensions)) {
            return array('error' => 'A extensão do arquivo enviado não é permitido.');
        }

        if (count($this->allowedExtensions) > 0 && !in_array(strtolower($ext), $this->allowedExtensions)) {
            $these = implode(', ', $this->allowedExtensions);
            return array('error' => 'A extensão do arquivo é inválida, é permitido apenas ' . $these . '.');
        }

        if (!$replaceOldFile) {

            $filename = $this->stringToSlug($filename);

            /// don't overwrite previous files that were uploaded
            while (file_exists($uploadDirectory . $filename . '.' . $ext)) {
                $filename .= rand(10, 99);
            }
        }

        if ($this->file->save($uploadDirectory . $filename . '.' . $ext)) {
            return array('success' => true, 'filename' => $filename . '.' . $ext);
        }
        
        return array('error' => 'Não foi possível salvar o arquivo. O upload foi cancelado, ou o servidor encontrou um erro.');
    }

    /**
     * Converte uma string para uma URL
     * @param string $string
     * @return string
     */
    private function stringToSlug($string) {

        $new_string = strtolower(trim($string));
        $new_string = preg_replace('/[^a-z0-9-]/', '-', $new_string);
        $new_string = preg_replace('/-+/', "-", $new_string);

        return $new_string;
    }

    public function removeTempFile($directory) {

        $now = time();
        $timeExpireFile = 86400; // 24 horas

        $iterator = new DirectoryIterator($directory);

        foreach ($iterator as $entry) {

            if ($entry->getFilename() != "." && $entry->getFilename() != ".." && ($now - $entry->getMTime()) > $timeExpireFile && $entry->isWritable()) {
                @unlink($entry->getRealPath());
            }
        }

        unset($iterator);
    }

}

$allowedExtensions = array("jpeg", "jpg", "png", "gif", "doc", "docx", "bmp", "dot", "xls", "xlsx", "pdf", "txt", "ppt", "pptx");

$sizeLimit = 10 * 1024 * 1024;

$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);

$result = $uploader->handleUpload(__DIR__ . '/temp/');
header('Content-type: application/x-json');
// to pass data through iframe you will need to encode all html tags
echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);

$uploader->removeTempFile(__DIR__ . '/temp/');
exit;