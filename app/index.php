<?php
require(dirname(__FILE__) . '/bootstrap.php');

\Slim\Slim::registerAutoloader();

$logWriter = new \Slim\LogWriter(fopen('./_log/slim.log', 'a'));

$app = new \Slim\Slim(array(
    'log.write' => $logWriter,
    'log.enabled' => true,
    'debug' => true
));

# === Routes
require_once './_config/route.php';

$app->run();

db()->_disconect();
exit;