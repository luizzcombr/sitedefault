<?php

namespace Codando\Controller;

class Cadastro extends Controller {

    private $feedback;
    private $dir = 'cadastro';

    public function _valid($istermo = true) {

        $this->clearMessages();

        $dado_required = array(
            'nome' => 'xss%s',
            'cnpjcpf' => '%s',
            'cep' => '%s',
            'endereco' => '%s',
            'numero' => '%s',
            'bairro' => '%s',
            'telefone' => 'xss%s',
            'id_cidade' => '%d',
            'email' => 'EMAIL'
        );
        
        if($istermo === true){
            $dado_required['password'] = '%s';
            $dado_required['confirmarsenha'] = '%s';
            $dado_required['termos'] = '%s';
        }

        $dado_opt = array('complemento' => 'xss%s', 'fotoperfil' => 'FILETEMP');

        $dado = input()->_toPost($dado_required, 'xss%s', true);

        if (is_array($dado) === false) {

            $this->error = true;
            $this->messageList[] = 'Informe os campos são obrigatórios(*) e o arquivo(s)';
            return false;
        }

        $dado = array_merge(input()->_toPost($dado_opt, 'xss%s', false), $dado);

        //Termos
        if ($dado['termos'] != 'on' && $istermo == true) {

            $this->error = true;
            $this->messageList[] = 'Aceite os termos do site.';
            return false;
        }

        $telefoneClean = preg_replace("/[^0-9]/", '', $dado['telefone']);

        $ddd = substr($telefoneClean, 1, 2);
        $telefone = substr($telefoneClean, 2);

        if (strlen($ddd) < 2) {
            $this->error = TRUE;
            $this->messageList[] = " DDD do Telefone do cadastro esta invalido.";
            return false;
        }

        if (strlen($telefone) < 8) {
            $this->error = TRUE;
            $this->messageList[] = " Telefone do cadastro esta invalido.";
            return false;
        }

        //Senha
        if (strlen($dado['senha']) < 4 && $istermo == true) {

            $this->error = true;
            $this->messageList[] = 'Senha deve conter mais de 4 caracteres.';
            return false;
        }

        //Senha
        if ($dado['senha'] != $dado['confirmarsenha']  && $istermo == true) {

            $this->error = true;
            $this->messageList[] = 'Senha deve ser igual a confirmação de senha.';
            return false;
        }


        if($istermo == false){
            
            //Email
             if ($this->_isExist(array('email' => $dado['email'], 'emaildiff' => $this->modulo->getEmail()), 'email = :email AND email <> :emaildiff') == TRUE) {

                 $this->error = TRUE;
                 return FALSE;
             }
        }else if ($this->_isExist(array('email' => $dado['email'])) == true) {

            $this->error = true;
            return false;
        }

        unset($dado['termos'], $dado['confirmarsenha'], $dado['fotoperfil']); //Campo não salvos

        return $dado;
    }

    public function _insert() {

        $dado = $this->_valid();
        
        if (is_array($dado) === false) {
            return false;
        }
        
        $_config = \Codando\App::getConfig('cadastro');

        $dado['password'] = password_hash($dado['password'], PASSWORD_BCRYPT);
        $dado['datacadastro'] = date('Y-m-d H:i:s');

        //Inserir cadastro
        $id_cadastro = db()->insert('cadastro', $dado);

        $this->_load($id_cadastro);

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = 'Cadastro não inserido.';
            return false;
        }

        //lastlogin
        $this->lastLogin();

        $this->messageList[] = 'Cadastro da loja salvo com sucesso';
        $this->error = false;

        $this->_updateImagem('logomarca', 2);
        $this->_updateImagem('fotocapa', 1);

        $this->_emailBemvindo();

        return true;
    }

    private function lastLogin() {

        $ip = $_SERVER['REMOTE_ADDR']? : ($_SERVER['HTTP_X_FORWARDED_FOR']? : $_SERVER['HTTP_CLIENT_IP']);
        $date = date('Y-m-d H:i');
        $user = $_SERVER['HTTP_USER_AGENT'];

        $lastlogin = $ip . '@' . $date . ' in ' . $user;

        db()->update('cadastro', array('lastlogin' => $lastlogin), ' id_cadastro = :cads ', array('cads' => $this->modulo->getId()));

        input()->setSession('logado', $this->modulo->getId());
        input()->setSession('nome', $this->modulo->getNome());
        input()->setSession('tipologado', $this->modulo->getTipo());
        input()->setSession('url', $this->modulo->getUrl());
    }

    public function _update() {

        $this->_loadLogado();

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = 'Usuario não logado';
            return false;
        }

        $dado = $this->_valid(false);

        if (is_array($dado) === false) {
            return false;
        }
		
        //Senha
        if (is()->nul($dado['password']) === true) {

            $dado['password'] = NULL;
            unset($dado['password']);
        } else {

            $dado['password'] = password_hash($dado['password'], PASSWORD_BCRYPT);
        }

        $update = db()->update('cadastro', $dado, 'id_cadastro = :id_cadastro', array('id_cadastro' => $this->modulo->getId()));

        $this->messageList[] = 'Cadastro alterado com sucesso';
        $this->error = false;

        $this->_updateImagem('fotoperfil', 1);

        return true;
    }

    public function _updateSenha() {

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false)
            $this->_loadLogado();

        $dado_required = array(
            'password' => 'xss%s',
            'confsenha' => 'xss%s'
        );

        $dado = input()->_toPost($dado_required, 'xss%s', true);

        if (is_array($dado) === false) {

            $this->messageList[] = "Informe os dados da senha.";
            $this->error = false;
            return false;
        }

        //Senha
        if (md5($dado['password']) != md5($dado['confsenha'])) {

            $this->error = true;
            $this->messageList[] = 'Confirmação de senha está diferente da senha.';
            return false;
        }

        $dado['confsenha'] = NULL;
        unset($dado['confsenha']);

        $dado['password'] = password_hash($dado['password'], PASSWORD_BCRYPT);

        $update = db()->update('cadastro', $dado, 'id_cadastro = :id_cadastro', array('id_cadastro' => $this->modulo->getId()));

        if ($update === false) {

            $this->error = true;
            $this->messageList[] = "Senha não atualizada";
            return false;
        }

        $this->messageList[] = "Senha alterado com sucesso";
        $this->error = false;
    }

    public function _updateCapa(){
        
        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false)
            $this->_loadLogado();
        
        $this->_updateImagem('fotocapa', 1);
    }
    
    public function _updateLogomarca(){
        
        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false)
            $this->_loadLogado();
        
        $this->_updateImagem('logomarca', 2);
    }
    
    public function _login() {

        $this->clearMessages();

        global $retornoList;

        $recuperar = input()->_toPost('recemail');

        if ($recuperar != NULL) {
            return $this->_recupera($recuperar);
        }

        $_dado = array(
            'email' => 'EMAIL',
            'senha' => '%s'
        );

        $dado = input()->_toPost($_dado);

        if (is_array($dado) === false) {
            $this->error = true;
            $this->messageList[] = 'Informe os dados de login.';
            return false;
        }

        /* @var $this->modulo Codando\Modulo\Cadastro */
        $this->modulo = app()->loadModulo('cadastro', array(' email = :email AND status = 1 ', array('email' => $dado['email'])));

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false || password_verify($dado['senha'], $this->modulo->getSenha()) === false) {

            $this->error = true;
            $this->messageList[] = 'Dados invalidos, porfavor verifique seu dados.';
            $this->modulo = NULL;
            return false;
        }

        //lastlogin
        $this->lastLogin();
        $this->error = false;
        $this->messageList[] = 'Aguarde ...';
        return true;
    }

    public function _cadastroFacebook($name, $email) {

        $this->clearMessages();

        if (is_null($name) === true || is_null($email) === true) {
            $this->error = true;
            $this->messageList[] = 'Não foi possivel realizar o login com facebook.';
            return false;
        }

        //Email
        if ($this->_isExist(array('email' => $email)) == true) {

            $this->error = true;
            return false;
        }

        $senha = geraSenha(2, true, false, false) . geraSenha(3, false, false, true);
        $_config = \Codando\App::getConfig('cadastro');

        $dado['nome'] = $name;
        $dado['email'] = $email;
        $dado['id_plano'] = $_config['planogratis'];
        $dado['senha'] = password_hash($senha, PASSWORD_BCRYPT);
        $dado['datacadastro'] = date('Y-m-d H:i:s');
        $dado['tipo'] = 1; //Particular
        $dado['id_cidade'] = 1; //Particular
        //Inserir cadastro
        $id_cadastro = db()->insert('cadastro', $dado);

        $this->_load($id_cadastro);

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = 'Cadastro não inserido.';
            return false;
        }

        //lastlogin
        $this->lastLogin();

        $this->error = false;
        $this->messageList[] = "aguarde ...";

        global $retornoList;

        $retornoList['redir'] = '/';

        $this->_emailBemvindo();

        return true;
    }

    public function _loginFacebook($email) {

        $this->clearMessages();

        if (is_null($email) === true) {
            $this->error = true;
            $this->messageList[] = 'Não foi possivel realizar o login com facebook.';
            return false;
        }

        /* @var $this->modulo Codando\Modulo\Cadastro */
        $this->modulo = app()->loadModulo('cadastro', array('email = :email AND status = 1 ', array('email' => $email)));

//            if (password_verify($dado['senha'], $this->modulo->getSenha())) {
//                //Senha correta
//            } else {
//                $this->error = true;
//                $this->messageList[] = "E-mail ou senha está incorreta.";
//                return false;
//            }

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = "Cadastro não encontrado.";
            return false;
        }

        //lastlogin
        $this->lastLogin();

        $this->error = false;
        $this->messageList[] = "aguarde ...";

        global $retornoList;

        $retornoList['redir'] = '/';

        return true;
    }

    public function _recupera($email) {

        $this->clearMessages();

        $this->modulo = app()->loadModulo('cadastro', array('email = :email AND status = 1 ', array('email' => $email)));

        /* @var $this->modulo Codando\Modulo\Cadastro */
        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = 'Cadastro não encontrado.';
            sleep(2);
            return false;
        }

        $transport = \Swift_MailTransport::newInstance();
        $datetime = date('Y-m-d H:i');

        do {
            $senhahash = cryp()->enc($this->modulo->getId() . '@:@' . $datetime);
        } while (strrpos($senhahash, '+') !== false);

        $emailFields = array(
            'Acesse o link para alterar sua senha' => 'http://tratofeitobr.com.br/recuperar/' . $senhahash,
            'Caso não solicitou ignore esta mensagem.' => NULL
        );

        $html_email = tpl()->display('contato_send', array('emailFields' => $emailFields, 'titulo' => 'Pedido de recuperação de senha'), false);

        $mailer = \Swift_Mailer::newInstance($transport);

        $message = \Swift_Message::newInstance('Recuperar a senha')
                ->setFrom(array('contato@tratofeitobr.com.br' => 'Recuperar a senha'))
                ->setTo(array($email))
                ->addPart($html_email, 'text/html');

        if ($mailer->send($message)) {

            $this->error = false;
            $this->messageList[] = 'Um e-mail foi enviado para você com instruções.';
            return true;
        }

        $transport = $message = $mailer = NULL;

        $this->error = true;
        $this->messageList[] = 'Não foi possivel, tente mais tarde.';
        return false;
    }

    public function _loadRecupera() {

        $senhaHash = input()->_toGet('senha');

        $recuperar = cryp()->dec($senhaHash);

        $dados = explode('@:@', $recuperar);

        if (count($dados) != 2) {

            $this->error = true;
            $this->messageList[] = "Cadastro não encontrado";
            return false;
        }

        $strtotime1 = strtotime('+1 day', strtotime($dados[1]));
        $strtotime2 = strtotime(date('Y-m-d H:i'));

        if ($strtotime1 < $strtotime2) {
            $this->error = true;
            $this->messageList[] = "Link expirado";
            return false;
        }

        $this->modulo = app()->loadModulo('cadastro', array('id_cadastro = :id_cadastro AND status = 1', array('id_cadastro' => $dados[0])));

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = "Cadastro não encontrado";
            return false;
        }

        $this->error = false;
        $this->messageList[] = "Logado com sucesso";

        input()->setSession('logado-senha', $this->modulo->getId());
    }

    public function _updateRecuperarSenha() {

        global $retornoList;

        $this->modulo = app()->loadModulo('cadastro', array('id_cadastro = :id_cadastro AND status = 1', array('id_cadastro' => input()->getSession('logado-senha'))));

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false) {
            $this->error = true;
            $this->messageList[] = 'Cadastro não encontrado';
            return false;
        }

        $dado_required = array(
            'senha' => 'xss%s',
            'confsenha' => 'xss%s'
        );

        $dado = input()->_toPost($dado_required, 'xss%s', true);

        if (is_array($dado) === false) {
            $this->error = true;
            $this->messageList[] = 'Informe a nova senha';
            return false;
        }

        //Senha
        if ($dado['senha'] !== $dado['confsenha']) {

            $this->error = true;
            $this->messageList[] = 'Confirmação de senha está diferente da senha';
            return false;
        }

        $dado['confsenha'] = NULL;
        unset($dado['confsenha']);

        $dado['senha'] = password_hash($dado['senha'], PASSWORD_BCRYPT);

        $update = db()->update('cadastro', $dado, 'id_cadastro = :id_cadastro', array('id_cadastro' => $this->modulo->getId()));

        if ($update === false) {

            $this->error = true;
            $this->messageList[] = 'Senha não foi atualizada';
            return false;
        }

        $this->messageList[] = 'Senha alterado com sucesso';
        $retornoList['redir'] = '/login';
        $this->error = false;
    }

    public function _list($order = 'id_cadastro DESC', $where = array()) {

        $parsWhere = array();
        $sqlWhere = NULL;

        $busca = input()->_toGet('q', 'xss%s', false);

        if (is()->nul($busca) === false) {

            $parsWhere['busca'] = "%" . $busca . "%";
            $where[] = '( LOWER(nome) LIKE LOWER(:busca) OR LOWER(email) LIKE LOWER(:busca) )';
        }

        if (count($parsWhere) > 0) {

            $sqlWhere = array(implode(' AND ', $where), $parsWhere);
        } else if (count($where) > 0) {

            $sqlWhere = implode(' AND ', $where);
        }

        $count = app()->countModulo('cadastro', $sqlWhere);

        if ($count < 1)
            return $this->moduloList = array();

        $this->setPaginar($count);

        $limit = array($this->getOffSetPaginar(), $this->getLimitPaginar());

        $this->moduloList = app()->listModulo('cadastro', $sqlWhere, $limit, $order);
    }

    public function isLogado() {
        return is()->id(input()->getSession('logado')) ? input()->getSession('logado') : false;
    }

    public function _loadLogado() {

        if ($this->isLogado() !== false) {
            $this->_load($this->isLogado());
        }
    }

    public function _load($con_id = NULL) {

        $con_id = $con_id !== NULL ? $con_id : input()->_toGet('id_cadastro', '%d', true);

        if ($con_id !== NULL) {

            $this->modulo = app()->loadModulo('cadastro', array('id_cadastro = :id_cadastro', array('id_cadastro' => $con_id)));

            if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false) {

                $this->error = true;
                $this->messageList[] = "Cadastro não encontrado";
                return false;
            }
            return false;
        }

        $this->error = true;
        $this->messageList = input()->getMsg();
    }

    public function _isExist($dados, $where = 'email = :email', $msg = NULL) {

        $msg = $msg === NULL ? 'E-mail já esta sendo utilizado.' : $msg;

        $_where = array($where, $dados);

        $modulo = app()->loadModulo('cadastro', $_where);

        if (is_instanceof('Codando\Modulo\Cadastro', $modulo) === true) {

            $this->error = true;
            $this->messageList[] = $msg;
        }

        return is_instanceof('Codando\Modulo\Cadastro', $modulo);
    }

    private function _emailBemvindo() {

        $_config = \Codando\App::getConfig('emailsend');

        // Create the Transport
        $transport = \Swift_MailTransport::newInstance();

        /* @var $cadastro \Codando\Modulo\Cadastro */
        $cadastro = $this->modulo;

        $emailArray = array($cadastro->getEmail());

        $html_email = tpl()->display('bemvindo_send', array(''), false, false);

        $mailer = \Swift_Mailer::newInstance($transport);

        $message = \Swift_Message::newInstance('Bem vindo')
                ->setFrom(array($_config['return'] => 'SITE'))
                ->setTo($emailArray)
                ->addPart($html_email, 'text/html');

        if ($mailer->send($message)) {

            return true;
        }

        $transport = $message = $mailer = NULL;

        return false;
    }

    public function _loadConfirma() {

        $emailHash = input()->_toGet('email');

        $email = cryp()->dec($emailHash);

        $this->modulo = app()->loadModulo('cadastro', array('email = :email AND status = 2', array('email' => $email)));

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = "Cadastro não encontrado";
            return false;
        }

        $update = db()->update('cadastro', array('status' => '1'), 'id_cadastro = :id_cadastro', array('id_cadastro' => $this->modulo->getId()));
        input()->setSession('logado-confirmacao', $this->modulo->getId());
    }

    public function _sendNewConf() {

        $email = input()->_toPost('email', 'EMAIL');

        if ($email === NULL) {

            $this->error = true;
            $this->messageList[] = "E-mail não encontrado";
            return false;
        }

        $this->modulo = app()->loadModulo('cadastro', array('email = :email AND status = 2', array('email' => $email)));

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = "Cadastro não encontrado";
            return false;
        }

        if ($this->_sendConfirmacao($this->modulo) === true) {

            $this->messageList[] = "Enviamos um email para '" . strip_tags($this->modulo->getEmail()) . "' com a confirmação do seu cadastro.";
            $this->error = false;
            return false;
        }
    }

    /**
     * Enviar confirma de cadastro
     * @param \Codando\Modulo\Cadastro $cadastro Cadastro inativo
     * @return Boolean true Enviado, FALSO não enviado
     */
    public function _sendConfirmacao($cadastro) {

        if (is_instanceof('Codando\Modulo\Cadastro', $cadastro) === true) {

            $_config = \Codando\App::getConfig('emailsend');

            // Create the Transport
            $transport = \Swift_SmtpTransport::newInstance();

            /*
              $transport = \Swift_SmtpTransport::newInstance($_config['smtp'], $_config['port'])
              ->setSourceIp('0.0.0.0')
              ->setUsername($_config['email'])
              ->setPassword($_config['senha']);
             */

            $emailArray = explode(',', $cadastro->getEmail());

            do {
                $pro_hash = cryp()->enc($cadastro->getEmail());
            } while (strrpos($pro_hash, '+') !== false);

            $emailFields = array(
                'Link para confirma' => COD_URL . 'confirmacao-email/' . $pro_hash
            );

            $html_email = tpl()->display('contato_send', array('titulo' => 'Está é uma confirmação do seu cadastro realizado no site:' . COD_URL, 'emailFields' => $emailFields), false);

            $mailer = \Swift_Mailer::newInstance($transport);

            $message = \Swift_Message::newInstance('Confirmação de cadastro')
                    ->setFrom(array($_config['return'] => 'Confirmação de cadastro'))
                    ->setTo($emailArray)
                    ->addPart($html_email, 'text/html');

            if ($mailer->send($message)) {

                return true;
            }

            return false;
        }
    }

    public function _updateImagem($nome = 'fotoperfil', $ordem = 1) {

        $this->clearMessages();

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false)
            return false;

        $dado_required = array();
        $dado_required[$nome] = 'FILETEMP';

        $dado = input()->_toPost($dado_required, 'xss%s', true);

        if (is_array($dado) === false) {

            //$this->messageList[] = "Selecione uma imagem.";
            //$this->error = true;
            return false;
        }

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false) {

            $this->messageList[] = "Você não esta logado.";
            $this->error = true;
            return false;
        }

        $image = getimagesize(COD_DIR_APP . "/_arquivos/temp/" . $dado[$nome]);

        if ($image[0] > 3000 || $image[1] > 3000) {

            $this->error = true;
            $this->messageList[] = "Sua imagem de perfil deve ter a resolução menor que 3000 pixel";
            return false;
        }

        /* @var $arquivoTemp \Codando\Modulo\Arquivo */
        foreach ($this->modulo->getArquivoList() as $arquivoTemp) {

            if (strpos($arquivoTemp->getUrl(), $nome) != false) {

                $del = db()->delete('arquivo', ' id_modulo = 43 AND id_est = :cadastro AND id_arquivo = :arq ', array('arq' => $arquivoTemp->getId(), 'cadastro' => $this->modulo->getId()));

                if ($del === true) {
                    @unlink(COD_DIR_APP . "/_arquivos/" . $this->dir . "/" . $arquivoTemp->getUrl());
                }

                break;
            }
        }

        $upload = $this->_upload($dado[$nome], $nome, $ordem);

        if ($upload === false)
            return $upload;
    }

    public function _upload($fileTemp, $prefix, $ordem = 1) {

        $ext = array('jpeg', 'jpg', 'png');

        if (is_instanceof('Codando\Modulo\Cadastro', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = "Cadastro não encontrado";
            return false;
        }

        if ($fileTemp === NULL || file_exists(COD_DIR_APP . '/_arquivos/temp/' . $fileTemp) === false) {

            $this->error = true;
            $this->messageList[] = "Arquivo temporario não encontrado.";
            return false;
        }

        $pathinfor = pathinfo(COD_DIR_APP . '/_arquivos/temp/' . $fileTemp);

        if (is_array($pathinfor) === false) {
            $this->error = true;
            $this->messageList[] = "Arquivo temporario corrompido.";
            return false;
        }

        $extRe = $pathinfor['extension'];
        $filenameRe = $prefix . md5(uniqid()) . date('YmdHis');

        if (in_array(strtolower($extRe), $ext) === false) {

            $this->error = true;
            $this->messageList[] = "Arquivo não permitido";
            return false;
        }

        if (strrpos($filenameRe, $this->dir) === false) {

            $filenameRe = $this->dir . "-" . $filenameRe;
        }

        while (file_exists(COD_DIR_APP . '/_arquivos/' . $this->dir . '/' . $filenameRe . '.' . $extRe) === true) {
            $filenameRe .= rand(10, 999);
        }

        $new_arquivo = array(
            'url' => $filenameRe . '.' . $extRe,
            'id_est' => $this->modulo->getId(),
            'id_modulo' => 43, //Cadastro
            'ordem' => $ordem
        );

        if (rename(COD_DIR_APP . '/_arquivos/temp/' . $fileTemp, COD_DIR_APP . '/_arquivos/' . $this->dir . '/' . $filenameRe . '.' . $extRe)) {

            $id_arquivo = db()->insert('arquivo', $new_arquivo);

            return $id_arquivo;
        }

        return false;
    }

    public function cleanStateController() {

        $this->clearMessages();
        $this->modulo = NULL;
        $this->moduloList = array();
    }

    public function __construct() {

        $this->feedback = array();
    }

    public function __destruct() {

        unset($this->modulo, $this->moduloList);
    }

}
