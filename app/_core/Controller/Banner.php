<?php

namespace Codando\Controller;

class Banner extends Controller {

    public static function loadByZona($id_zona) {

        $banner = NULL;
        $prints = (array) unserialize(input()->getSession('banner-' . $id_zona));

        $where = 'status = 1 AND id_zona = :id_zona AND (datafim >= :date) AND (datainicio <= :date)'; //datainicio <= :date AND

        $notIn = NULL;
        $prints = array_filter($prints);
        if (is_array($prints) == true && count($prints) >= 1) {
            $notIn = ' AND id_banner NOT IN (' . implode(' ,', $prints) . ') ';
        }

        $paraWhere = array('id_zona' => $id_zona, 'date' => date('Y-m-d H:i:s'));

        $bannerList = app()->listModulo('banner', array($where . $notIn, $paraWhere), NULL, 'id_banner DESC');

        if (count($bannerList) == 0) {

            $bannerList = app()->listModulo('banner', array($where, $paraWhere), NULL, 'id_banner DESC');
            $prints = array();
        }

        if (count($bannerList) >= 1) {

            //shuffle($bannerList); // Mistura

            $banner = array_shift($bannerList);
            if (is_modulo('banner', $banner) === true) {
                $prints[] = $banner->getId();
            }
        }

        input()->setSession('banner-' . $id_zona, serialize($prints));

        return $banner;
    }

    public static function listView($id_zona = null, $limittd = null, $ordem = null) {

        $where = 'id_zona = :id_zona AND ( datafim >= :date ) AND (datainicio <= :date)';

        $prints = (array) unserialize(input()->getSession('banner-' . $id_zona));

        $notIn = NULL;
        $prints = array_filter($prints);
        if (is_array($prints) == true && count($prints) >= 1) {
            $notIn = ' AND id_banner NOT IN (' . implode(' ,', $prints) . ') ';
        }

        $paraWhere = array('id_zona' => $id_zona, 'date' => date('Y-m-d  H:i:s'));


        return app()->listModulo('banner', array($where . $notIn, $paraWhere), $limittd, $ordem);
    }

    public function updadeAcesso($banner) {

        if (is_modulo('banner', $banner) === true) {
            db()->update('banner', array('acesso' => ($banner->getAcesso() + 1)), ' id_banner = :id_banner ', array('id_banner' => $banner->getId()));
        }
    }

    public function updadeVisualizacao($bannerListId) {
        db()->getConnection()->executeUpdate("UPDATE banner SET exibido = (exibido + 1) WHERE id_banner IN ('" . implode("','", $bannerListId) . "') ;");
    }

    public function _load($id = NULL) {

        if (is()->id($id) !== true) {
            $this->error = true;
            $this->messageList[] = "Banner não encontrado";
            return false;
        }

        $this->modulo = app()->loadModulo('banner', array(' id_banner = :id_banner ', array('id_banner' => (int) $id)));

        if (is_instanceof('banner', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = "Banner não encontrado";
            return false;
        }
    }

    public function cleanStateController() {

        $this->clearMessages();
        $this->modulo = NULL;
        $this->moduloList = array();
    }

    public function __construct() {
        
    }

    public function __destruct() {
        
    }

}
