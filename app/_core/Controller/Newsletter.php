<?php

 namespace Codando\Controller;

 class Newsletter extends Controller {

     public function _insert() {

         $_newsletter = array(
             'email' => 'EMAIL'
         );

         $newsletter = input()->_toPost($_newsletter, true);

         if (is_array($newsletter) === FALSE) {

             $this->error = true;
             $this->messageList[] = 'Informe o seu e-mail.';
             sleep(3);
             return FALSE;
         }

         $newsletterTemp = app()->loadModulo('newsletter', array('email = :email', array('email' => $newsletter['email'])));

         if (is_instanceof('Codando\Modulo\Newsletter', $newsletterTemp) === TRUE) {
             $this->error = true;
             $this->messageList[] = "Email já cadastrado";
             sleep(3);
             return FALSE;
         }

         $id_newsletter = db()->insert('newsletter', $newsletter);

         $this->_load($id_newsletter);

         if (is_instanceof('Codando\Modulo\Newsletter', $this->modulo) === FALSE) {

             $this->error = true;
             $this->messageList[] = "Newsletter não inserido";
         } else {

             $this->error = false;
         }
         
     }

     public function _load($new_id) {

         $this->modulo = app()->loadModulo('newsletter', array('id_newsletter = :id_newsletter', array('id_newsletter' => $new_id)));

         if (is_instanceof('Codando\Modulo\Newsletter', $this->modulo) === False) {

             $this->error = true;
             $this->messageList[] = "Newsletter não encontrado";
         }
     }

     public function cleanStateController() {

         $this->clearMessages();
         $this->modulo = NULL;
         $this->moduloList = array();
     }

     public function __construct() {

     }

     public function __destruct() {

         unset($this->modulo, $this->moduloList);
     }

 }
 