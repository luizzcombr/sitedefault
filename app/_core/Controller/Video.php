<?php

namespace Codando\Controller;

class Video extends Controller {

    public function _load($vid_id = NULL) {

        $vid_url = input()->_toGet('vid_url', 'xss%s', true);
        $vid_id = $vid_id !== NULL ? $vid_id : input()->_toRequest('id_foto', '%d', true);

        if ($vid_url !== NULL && $vid_id !== NULL) {

            $this->modulo = app()->loadModulo('video', array('url_titulo = :url_titulo AND id_video = :id_video', array('id_video' => $vid_id, 'url_titulo' => $vid_url)));

            if (is_instanceof('Codando\Modulo\Video', $this->modulo) === False) {

                $this->error = true;
                $this->messageList = "Video não encontrado";
            }
        } else {

            $this->error = true;
            $this->messageList = input()->getMsg();
        }
    }

    public function _list($order = 'id_video DESC', $where = NULL) {

        $data = date('Y-m-d');
        $parsWhere = array();
        $sqlWhere = NULL;
        $busca = input()->_toRequest('q', 'xss%s', false);

        $parsWhere['data'] = $data;
        $where[] = '( data <= :data )';

        if (is()->nul($busca) === false) {

            $parsWhere['busca'] = "%" . $busca . "%";
            $where[] = '( LOWER(titulo) LIKE LOWER(:busca) OR LOWER(data) LIKE LOWER(:busca) )';
        }

        if (count($parsWhere) > 0) {

            $sqlWhere = array(implode(' AND ', $where), $parsWhere);
        } else if (count($where) > 0) {

            $sqlWhere = implode(' AND ', $where);
        }

        $count = app()->countModulo('video', $sqlWhere);

        $this->setPaginar($count);

        $limit = array($this->getOffSetPaginar(), $this->getLimitPaginar());

        $this->moduloList = app()->listModulo('video', $sqlWhere, $limit, $order);
    }

    public function updadeAcesso($video) {

        if (is_instanceof('Codando\Modulo\Video', $video) === true) {

            db()->update('video', array('acesso' => ($video->getAcesso() + 1)), ' id_video = :id_video ', array('id_video' => $video->getId()));
        }
    }

    public function cleanStateController() {

        $this->clearMessages();
        $this->modulo = NULL;
        $this->moduloList = array();
    }

    public function __construct() {
        
    }

    public function __destruct() {

        $this->modulo = $this->moduloList = $this->cidade = NULL;

        unset($this->modulo, $this->moduloList);
    }

}
