<?php

namespace Codando\Controller;

class AppController extends Controller {

    /* @var $extends \Codando\Modulo\Modulo */
    private $extends;

    public function _load($id = NULL) {
        
        $primary_key = $this->extends->getPrimarykey();
        
        $_id = $id !== NULL ? $id : input()->_toGet($primary_key, '%d', true);

        if ($_id !== NULL) {

            $this->modulo = app()->loadModulo($this->extends->getUrl(), array($primary_key . ' = :id', array('id' => $_id)));

            if (is_instanceof($this->extends->getClasseNome(), $this->modulo) === TRUE) {

                return true;
            }

            $this->error = true;
            $this->messageList = $this->extends->getUrl() . " não encontrado";

            return FALSE;
        } else {

            $this->error = true;
            $this->messageList = input()->getMsg();
        }
    }

    public function _list($order = NULL, $where = array()) {

        $parsWhere = array();
        $sqlWhere = NULL;
        $order  = $order === NULL ? $this->extends->getPrimarykey() . ' DESC':$order;
        
        $busca = input()->_toGet('q', 'xss%s', FALSE);

        if (is()->nul($busca) === FALSE) {

            $parsWhere['busca'] = "%" . $busca . "%";
            $where[] = '( LOWER(titulo) LIKE LOWER(:busca))';
        }

        if (count($parsWhere) > 0) {

            $sqlWhere = array(implode(' AND ', $where), $parsWhere);
        } else if (count($where) > 0) {

            $sqlWhere = implode(' AND ', $where);
        }

        $count = app()->countModulo($this->extends->getUrl(), $sqlWhere);

        $this->setPaginar($count);

        $limit = array($this->getOffSetPaginar(), $this->getLimitPaginar());

        $this->moduloList = app()->listModulo($this->extends->getUrl(), $sqlWhere, $limit, $order);
    }
    
    public function setExtends($name){
        $this->extends = app()->getModulo($name);
    }

    public function cleanStateController() {

        $this->clearMessages();
        $this->modulo = NULL;
        $this->moduloList = array();
    }


    public function __construct() {

    }

    public function __destruct() {

        $this->modulo = $this->moduloList = NULL;

        unset($this->modulo, $this->moduloList, $this->extends);
    }

}
