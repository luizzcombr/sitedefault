<?php

namespace Codando\Controller;

class Contato extends Controller {

    public $feedback;

    public function _insert() {

        $_contato = array(
            'nome' => 'xss%s',
            'email' => 'EMAIL',
            'telefone' => 'xss%s',
            'mensagem' => 'xss%s'
        );

        $contato = input()->_toPost($_contato, '%s', true);

        if (is_array($contato) === false) {
            $this->error = true;
            $this->messageList[] = "Informe os campos obrigatorios";
            return false;
        }

        if ($this->_isSend($contato['email'], $contato['mensagem']) === true) {
            $this->error = true;
            $this->messageList[] = 'Contato já enviado.';
            return false;
        }

        $contato['data'] = date('Y-m-d');

        $id_contato = db()->insert('contato', $contato);

        $this->_load($id_contato);

        if (is_instanceof('Codando\Modulo\Contato', $this->modulo) === false) {

            $this->error = true;
            $this->messageList[] = "Não foi possivel, tente mais tarde.";

            return false;
        }

        $this->_send();

        return true;
    }

    public function _load($con_id) {

        $this->modulo = app()->loadModulo('contato', array('id_contato = :id_contato', array('id_contato' => $con_id)));

        if (is_instanceof('Codando\Modulo\Contato', $this->modulo) === False) {

            $this->error = true;
            $this->messageList[] = $this->feedback['pt'][2];
			return false;
        }
    }

    public function _isSend($email, $mensagem) {

        $this->modulo = app()->loadModulo('contato', array('MD5(mensagem) = :mensagem AND email = :email ', array('mensagem' => md5($mensagem), 'email' => $email)));

        if (is_instanceof('Codando\Modulo\Contato', $this->modulo) === true) {

            $this->messageList[] = 'Mensagem já enviada';
            return true;
        }

        return false;
    }

    public function _send() {

        $_config = \Codando\App::getConfig('emailsend');

        $_config_site = \Codando\App::getConfig('dominio');
        
        /* @var $deparmento Departamento */
        $deparmento = app()->loadModulo('departamento', array('id_departamento = :id_departamento', array('id_departamento' => $_config['departament_contato'])));

        if (is_instanceof('Codando\Modulo\Departamento', $deparmento) === false) {
            return false;
        }

        /* @var $contato \Codando\Modulo\Contato */
        $contato = $this->modulo;

        $emailArray = explode(',', $deparmento->getEmail());

        $emailFields = array(
            'Nome' => $contato->getNome(),
            'Telefone' => $contato->getTelefone(),
            'Email' => $contato->getEmail(),
            'Mensagem' => nl2br($contato->getMensagem()),
            'Gerenciar' => $_config_site['root'] . '/sgc#/carregar/contato/' . $contato->getId(),
        );

        $html_email = tpl()->display('contato_send', array('emailFields' => $emailFields), false);

        $mailer = $this->swiftTransport();

        $message = \Swift_Message::newInstance($deparmento->getNome())
                ->setFrom(array($_config['return'] => $deparmento->getNome()))
                ->setTo($emailArray)
                ->addPart($html_email, 'text/html');


        if ($mailer->send($message)) {

            return true;
        } else {

            return false;
        }
    }

    public function cleanStateController() {

        $this->clearMessages();
        $this->modulo = NULL;
        $this->moduloList = array();
    }

    public function __construct() {


        $this->feedback = array(
            'pt' => array('Preencha os campos obrigatórios(*), corretamente!', 'Contato não inserido', 'Contato não encontrado', 'Contato já foi enviado anteriormente.'),
            'en' => array('Complete the required (*) fields correctly!', 'Not inserted Contact', 'Contact not met'),
            'es' => array('Preencha os campos obrigatórios(*), corretamente!', 'No inserido Contact', 'Contact no alcanzada'),
        );
    }

    public function __destruct() {

        unset($this->modulo, $this->moduloList);
    }

}
