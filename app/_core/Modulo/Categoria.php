<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Categoria 
 * /
 * @package Codando
 */
class Categoria {

    private $id_categoria;
    private $titulo;
    private $url_titulo;

    public function getId() {
        return $this->id_categoria;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getUrltitulo() {
        return $this->url_titulo;
    }

    public function setId($id_categoria) {
        $this->id_categoria = $id_categoria;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setUrltitulo($url_titulo) {
        $this->url_titulo = $url_titulo;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Categoria && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->titulo;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        
    }

}
