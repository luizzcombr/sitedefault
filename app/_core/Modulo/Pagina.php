<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Pagina 
 * /
 * @package Codando
 */
class Pagina {

    use TemArquivo;

    private $id_pagina;
    private $titulo;
    private $texto;

    public function getId() {
        return (int) $this->id_pagina;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getTexto() {
        return $this->texto;
    }

    public function setId($id_pagina) {
        $this->id_pagina = (int) $id_pagina;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setTexto($texto) {
        $this->texto = $texto;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Pagina && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_pagina;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        
    }

}
