<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Noticia 
 * /
 * @package Codando
 */
class Noticia extends Model {

    use TemArquivo;

    private $id_noticia;
    private $titulo;
    private $url_titulo;
    private $data;
    private $status;
    private $resumo;
    private $texto;
    private $id_categoria;

    public function getId() {
        return (int) $this->id_noticia;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getUrltitulo() {
        return $this->url_titulo;
    }

    public function getUrl() {
        return '/noticias/' . $this->url_titulo . '-' . $this->id_noticia;
    }

    public function getData($format = 'd/m/Y H:i') {
        return $this->data !== NULL ? date($format, strtotime($this->data)) : NULL;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getTexto() {
        return $this->texto;
    }

    public function getResumo() {
        return strlen($this->resumo) < 3 ? string()->truncate($this->texto, 150) : $this->resumo;
    }

    public function getCategoria() {
        return $this->id_categoria;
    }

    public function setId($id_noticia) {
        $this->id_noticia = (int) $id_noticia;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setUrltitulo($url_titulo) {
        $this->url_titulo = $url_titulo;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function setTexto($texto) {
        $this->texto = $texto;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function setResumo($resumo) {
        $this->resumo = $resumo;
    }

    public function setCategoria($id_categoria) {
        $this->id_categoria = $id_categoria;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Noticia && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {

        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_noticia;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        
    }

}
