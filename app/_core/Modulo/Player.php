<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Player 
 * /
 * @package Codando
 */
class Player {

    use TemArquivo;

    private $id_player;
    private $titulo;

    public function getId() {
        return $this->id_player;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function setId($id_player) {
        $this->id_player = $id_player;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Player && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_player;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        
    }

}
