<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Newsletter 
 *
 * @package Codando
 */
class Newsletter {

    private $id_newsletter;
    private $email;

    public function getId() {
        return (int) $this->id_newsletter;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setId($id_newsletter) {
        $this->id_newsletter = (int) $id_newsletter;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Newsletter && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_newsletter;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        
    }

}
