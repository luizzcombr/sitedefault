<?php

namespace Codando\Modulo;

/**
 * Classe cria modulo para persitencia dos registos com banco
 * 
 * @package Codando
 */
class Model {

    protected $modulo;

    public function insert() {

        $data = $this->getObjectVars();

        unset($data[$this->modulo->getPrimarykey()]); //Remove ID

        $id_save = db()->insert($this->modulo->getTabela(true), $data);

        if ($id_save > 0) {
            $this->setId($id_save);
        }
    }

    public function create($array) {

        $id_save = db()->insert($this->modulo->getTabela(true), $array);

        if ($id_save > 0) {
            $this->setId($id_save);
        }
    }

    public function delete() {
        $data = $this->getObjectVars();

        $delete = db()->delete($this->modulo->getTabela(true), $this->modulo->getPrimarykey() . ' = :id ', ['id' => $data[$this->modulo->getPrimarykey()]]);

        return $delete;
    }

    public function update() {
        
    }

    public function findId($id) {

        $this->setId($id);

        $findId = app()->loadModulo($this->modulo->getTabela(true), array($this->modulo->getPrimarykey() . ' = :findId ', array('findId' => (int) $id)));

        if ($this->isEquals($findId) === TRUE) {
            return $findId;
        }
    }

    protected function init() {

        $className = strtolower(str_replace("Codando\\Modulo\\", "", get_class($this)));
        $this->modulo = app()->getModulo($className);
        if (is_modulo('modulo', $this->modulo) === false)
            return false;
    }

    public function __toString() {
        
    }

    public function __construct() {
        
    }

    public function __destruct() {
        
    }

}
