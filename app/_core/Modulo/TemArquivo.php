<?php

namespace Codando\Modulo;

/**
 * Trait que representa Arquivo
 * @author luizzcombr
 */
trait TemArquivo {

    protected $arquivoList = array();
	
	/**
     * Obter Arquivo
     * @param boolean $force 
     * @return \Codando\Modulo\Arquivo
     */
    public function getArquivo($force = true) {
       return $force === true ? is_instanceofOrNew('Codando\Modulo\Arquivo', current($this->arquivoList)):current($this->arquivoList);
    }
	
	/**
     * Obter Arquivo Listagem
     * @return \Codando\Modulo\Arquivo[]
     */
    public function getArquivoList() {
        return $this->arquivoList;
    }

    public function setArquivo(Arquivo $arquivo) {
        $this->arquivoList[$arquivo->getId()] = $arquivo;
    }

    public function setArquivoList($arquivo) {
        $this->arquivoList = $arquivo;
    }

    public function setArquivoAdd(Arquivo $arquivo) {
        $this->arquivoList[] = $arquivo;
    }

}
