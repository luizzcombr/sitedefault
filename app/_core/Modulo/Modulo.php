<?php

namespace Codando\Modulo;

class Modulo {

    private $id_modulo;
    private $classe;
    private $url;
    private $tabela;
    private $view;
    private $arquivo;
    private $arquivo_quantidade;
    private $arquivo_ext;
    private $dependencia;
    private $foreign;
	private $cache;
    private $primary_key;

    public function getId() {
        return (int) $this->id_modulo;
    }

    public function getClasseNome() {
        return $this->classe;
    }

    public function getUrl() {
        return $this->url;
    }
	
	public function getCache() {
        return $this->cache;
    }

    public function getTabela($fc_tb = false) {
        return (is_null($this->view) == false && $fc_tb === FALSE) ? $this->view : $this->tabela;
    }

    public function getView() {
        return $this->view;
    }

    public function getArquivo() {
        return $this->arquivo;
    }

    public function getArquivoQuantidade() {
        return $this->arquivo_quantidade;
    }

    public function getArquivoExt() {
        return $this->arquivo_ext;
    }

    public function getDependencia() {
        return $this->dependencia;
    }

    public function setId($id_modulo) {
        $this->id_modulo = $id_modulo;
        return $this;
    }

    public function setClasseNome($classe) {
        $this->classe = $classe;
        return $this;
    }

    public function setUrl($url) {
        $this->url = $url;
        return $this;
    }
	
	public function setCache($cache) {
        $this->cache = $cache;
		return $this;
    }

    public function setTabela($tabela) {
        $this->tabela = $tabela;
        return $this;
    }

    public function setView($view) {
        $this->view = $view;
        return $this;
    }

    public function setArquivo($arquivo) {
        $this->arquivo = (bool) $arquivo;
        return $this;
    }

    public function setArquivoQuantidade($arquivo_quantidade) {
        $this->arquivo_quantidade = (int) $arquivo_quantidade;
        return $this;
    }

    public function setArquivoExt($arquivo_ext) {
        $this->arquivo_ext = $arquivo_ext;
        return $this;
    }

    public function setDependencia($dependencia) {
        $this->dependencia = (bool) $dependencia;
        return $this;
    }

    public function setForeign($foreign) {
        $this->foreign = $foreign;
        return $this;
    }

    public function setPrimarykey($primary_key) {
        $this->primary_key = $primary_key;
        return $this;
    }

    public function getForeign() {
        return $this->foreign;
    }

    public function getPrimarykey() {
        return $this->primary_key;
    }

    public function __construct() {
        
    }

    public function __toString() {
        return $this->url;
    }

}
