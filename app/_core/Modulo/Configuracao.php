<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Configuracao 
 *
 * @package Codando
 */
class Configuracao {

    private $id_configuracao;
    private $titulo;
    private $configuracao;

    public function getId() {
        return (int) $this->id_configuracao;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getConfiguracao() {
        return $this->configuracao;
    }

    public function setId($id_configuracao) {
        $this->id_configuracao = (int) $id_configuracao;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setConfiguracao($configuracao) {
        $this->configuracao = $configuracao;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Configuracao && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_configuracao;
    }

    public function __construct() {
        
    }

    public function __destruct() {

    }

}