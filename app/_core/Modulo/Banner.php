<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Banner 
 * /
 * @package Codando
 */
class Banner {

    private $id_banner;
    private $titulo;
    private $link;
    private $id_zona;
    private $datainicio;
    private $datafim;
    private $resumo;
    private $arquivo;
    private $acesso;

    public function getId() {
        return $this->id_banner;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getAcesso() {
        return $this->acesso;
    }

    public function getLink() {
        return is_null($this->link) === false ? '/ads/' . $this->id_banner : null;
    }

    public function getExLink() {
        return $this->link;
    }

    public function getZona() {
        return $this->id_zona;
    }

    public function getDatainicio($format = 'd/m/Y') {
        return $this->datainicio !== NULL ? date($format, strtotime($this->datainicio)) : NULL;
    }

    public function getDatafim($format = 'd/m/Y') {
        return $this->datafim !== NULL ? date($format, strtotime($this->datafim)) : NULL;
    }

    public function getAdsense() {
        return $this->resumo;
    }

    public function getArquivo() {
        return $this->arquivo;
    }

    public function setId($id_banner) {
        $this->id_banner = $id_banner;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setLink($link) {
        $this->link = $link;
    }

    public function setZona($id_zona) {
        $this->id_zona = $id_zona;
    }

    public function setDatainicio($datainicio) {
        $this->datainicio = $datainicio;
    }

    public function setDatafim($datafim) {
        $this->datafim = $datafim;
    }

    public function setAdsense($resumo) {
        $this->resumo = $resumo;
    }

    public function setArquivo(Arquivo $arquivo) {
        $this->arquivo = $arquivo;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Banner && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_banner;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        
    }

}
