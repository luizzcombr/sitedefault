<?php

namespace Codando\Modulo;

/**
 * Classe que representa objeto Zona 
 * /
 * @package Codando
 */
class Zona {

    private $id_zona;
    private $titulo;

    public function getId() {
        return (int) $this->id_zona;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function setId($id_zona) {
        $this->id_zona = (int) $id_zona;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function isEquals($isEqual) {
        return ($isEqual instanceof Zona && $this->getId() == $isEqual->getId());
    }

    public function getObjectVars() {
        return get_object_vars($this);
    }

    public function __toString() {
        return (string) $this->id_zona;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        
    }

}
