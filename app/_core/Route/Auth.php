<?php

namespace Codando\Route;

use Slim\Slim as Slim;

class Auth {

    private $app;

    public static function notlogado() {

        $app = Slim::getInstance();

        if (is()->id(input()->getSession('logado')) === true) {
            $app->redirect('/');
            $app->stop();
            return false;
        }

        return true;
    }

    public static function logado() {

        $app = Slim::getInstance();

        if (is()->id(input()->getSession('logado')) === false) {
            $actual_link = $_SERVER['REQUEST_URI'];

            input()->setSession('return', $actual_link);

            $app->redirect('/login?return=' . urlencode($actual_link));
            $app->stop();
        }

        return true;
    }

    public function logout() {
        
        unset($_SESSION);
        session_destroy();

        $this->app->redirect('/login');
    }

    public function __construct() {

        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
