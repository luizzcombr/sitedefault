<?php

namespace Codando\Route;

use Codando\Controller as Controller,
    Slim\Slim as Slim;

class Contato extends Controller\Contato {
    
    private $app;
    
    public function index() {

        if ($this->app->request->isGet()) {

            view('contato_view', array('menuCurrent' => 'contato'));
            return;
        }

        $responde = array('status' => false);

        $this->_insert();

        if (is_instanceof('Codando\Modulo\Contato', $this->modulo)) {
            $responde = array('status' => true, 'redir' => '/contato', 'contato' => $this->modulo->getObjectVars());
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function __construct() {

        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
