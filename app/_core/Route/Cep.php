<?php

namespace Codando\Route;

use Slim\Slim as Slim;

class Cep {

    private $app;

    public function index($cep) {
        
        db()->_disconect();
        
        $cep = preg_replace("/[^0-9]/", '', $cep);
        
        $curl = OpenURL('http://viacep.com.br/ws/'.$cep.'/json/');
        
        $dados = @json_decode($curl, true);
        
        if(is_array($dados) && isset($dados['localidade']) == true && isset($dados['uf'])){
            db()->_reconect();
            /* @var $cidade \Codando\Modulo\Cidade */
            $cidade = app()->loadModulo('cidade', " nome LIKE '%".$dados['localidade']."%' AND id_estado IN ( select e.id_estado from estado e where e.sigla LIKE '%".$dados['uf']."%' ) ");
            
            if(is_modulo('cidade', $cidade)){
                $dados['cidade'] = $cidade->getObjectVars();
            }
        }
        
        header('Content-type: application/json');
        echo json_encode($dados);
        exit;
    }
    
    public function __construct() {

        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
