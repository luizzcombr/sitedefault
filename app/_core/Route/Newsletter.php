<?php

namespace Codando\Route;

use Codando\Controller as Controller,
    Slim\Slim as Slim;

class Newsletter extends Controller\Newsletter {
    
    private $app;
    
    public function index() {

        $responde = array('status' => false);

        $this->_insert();

        if (is_instanceof('Codando\Modulo\Newsletter', $this->modulo)) {
            $responde = array('status' => true, 'newsletter' => $this->modulo->getObjectVars());
        }

        $responde["msg"] = (implode("<br/>", $this->getMessageList()));

        header('Content-type: application/json');
        echo json_encode($responde);
    }

    public function __construct() {

        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
