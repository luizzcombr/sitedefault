<?php

namespace Codando\Route;

use Slim\Slim as Slim;

class Home  {

    private $app;

    public function index() {

        tpl()->display('index', array('menuCurrent' => 'index'));
    }
	
	public static function erro404(){
		
		tpl()->display('erro', array('menuCurrent' => 'erro'));
	}
	
	public function jserro(){
		
		if (isset($_GET['jserro']) && strlen($_GET['jserro']) > 3) {

			$ip = $_SERVER['REMOTE_ADDR'] ?: ($_SERVER['HTTP_X_FORWARDED_FOR'] ?: $_SERVER['HTTP_CLIENT_IP']);
			$date = date('Y-m-d H:i');
			$user = $_SERVER['HTTP_USER_AGENT'];

			@error_log("\n\t\r" . $ip . '@' . $date . ' in ' . $user . ' => ' . $_GET['jserro'], 3, COD_DIR_LOG . "/jserro.log");
		}
		
		exit;
	}

    public function __construct() {

        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
