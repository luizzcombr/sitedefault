<?php

namespace Codando\Route;

use Codando\Controller as Controller,
    Slim\Slim as Slim;

class Banner extends Controller\Banner {

    private $app;

    public function load($id) {

        $this->_load($id);

        if (is_modulo('banner', $this->modulo) === false) {
            $this->app->redirect('/');
            $this->app->stop();
            return false;
        }
        /* @var $banner \Codando\Modulo\Banner */
        $banner = $this->modulo;

        $this->updadeAcesso($banner);
        db()->_disconect();
        redirecionar($banner->getExLink());
        exit;
    }

    public function mostrou() {

        $postListId = explode(';', isset($_GET['v']) ? $_GET['v'] : NULL);
        $bannerListId = array();
        foreach ($postListId as $bannerId) {
            $bannerListId[] = intval($bannerId);
        }

        $bannerListId = array_filter($bannerListId);
        
        if (is_array($bannerListId) == true && count($bannerListId) >= 1)
            $this->updadeVisualizacao($bannerListId);
        
        var_dump($bannerListId);
        
        db()->_disconect();
    }

    public function __construct() {

        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
