<?php

namespace Codando\Route;

use Codando\Controller as Controller,
    Slim\Slim as Slim;

class Pagina extends Controller\Pagina {

    private $app;

    public function listAll() {

        $this->_list();

        tpl()->display('pagina_list', array('menuCurrent' => 'pagina',
            'paginacao' => $this->getPaginacao('/paginas'),
            'paginaList' => $this->moduloList)
        );
    }

    public function load($url, $id) {

        $this->_load($id);


        if (is_instanceof('Codando\Modulo\Pagina', $this->modulo) === false) {
            $this->app->redirect('/erro404');
            $this->app->stop();
            return false;
        }

        $menuCurrent = 'pagina';

        $pagina = $this->modulo;

        tpl()->display('pagina_view', compact('menuCurrent', 'pagina'));
    }

    public function sobre() {

        $this->_load(1);


        if (is_instanceof('Codando\Modulo\Pagina', $this->modulo) === false) {
            $this->app->redirect('/erro404');
            $this->app->stop();
            return false;
        }

        $menuCurrent = 'pagina';

        $pagina = $this->modulo;

        meta()->addMeta('title', $this->modulo->getTitulo());

        tpl()->display('pagina_view', compact('menuCurrent', 'pagina'));
    }

    public function __construct() {

        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
