<?php

namespace Codando\Route;

use Slim\Slim as Slim;

class Build {

    private $app;

    public function classe($modulourl) {
        
        
        $responde = array('status' => false);
        
        $modulo = app()->getModulo($modulourl, false);
        $describe = new \Codando\System\Describe();
        
        $responde['build'] = $describe->createClass($modulo);
        
        header('Content-type: application/json');
        echo json_encode($responde);
        
    }

    public function __construct() {

        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
