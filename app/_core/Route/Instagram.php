<?php

namespace Codando\Route;

use Slim\Slim as Slim;
use phpFastCache\CacheManager;

class Instagram {

    private $app;

    public function index($user = 'bfmsoficial') {

        db()->_disconect();

        //cache Local Temp
        CacheManager::setDefaultConfig([
            "path" => COD_DIR_CACHE,
        ]);

        $cache = CacheManager::getInstance('files');
        
        $ig_cache = $cache->getItem($user);
        
        $dados = [];
        
        //criar o cache
        if (is_null($ig_cache->get()) === true) {
            
            $insta_source = @file_get_contents('http://instagram.com/' . $user);

            $shards = explode('window._sharedData = ', $insta_source);
            $insta_json = explode(';</script>', $shards[1]);
            $insta_array = json_decode($insta_json[0], TRUE);
        
            $ig_cache->set($insta_json[0]);
            $cache->save($ig_cache);
            
        }else{//exist cache
            
            $insta_source = $ig_cache->get();
            $insta_array = json_decode($insta_source, TRUE);
        }
        
        //obtem nivel do user
        if(is_array($insta_array) && isset($insta_array['entry_data']) == true){
            $dados[$user] = $insta_array['entry_data']['ProfilePage'][0]['graphql'];
        }

        header('Content-type: application/json');
        echo json_encode($dados);
        exit;
    }

    public function __construct() {

        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
