<?php

namespace Codando\Route;

use Slim\Slim as Slim;

class Cidade {

    private $app;

    public function listAll() {
        
        $responde = array('status' => false);

        $estado = (int) input()->_toGet('estado', '%d');

        if ($estado >= 1) {

            $cidadeList = (array) app()->listModulo('cidade:notforeign', array(' id_estado = :id ', array('id' => $estado)));

            $responde["list"] = array();

            /* @var $cidade \Codando\Modulo\Cidade */
            foreach ($cidadeList as $cidade) {

                $responde["list"][] = array('id' => $cidade->getId(), 'nome' => $cidade->getNome());
            }
        }

        db()->_disconect();

        header('Content-type: application/json');
        echo json_encode($responde);
        
    }

    public function __construct() {

        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
