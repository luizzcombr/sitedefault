<?php

namespace Codando\Route;

use Codando\Controller as Controller,
    Slim\Slim as Slim;

class Noticia extends Controller\Noticia {

    private $app;

    public function listAll() {

        $this->_list();

        tpl()->display('noticia_list', array('menuCurrent' => 'noticia',
            'paginacao' => $this->getPaginacao('/noticias'),
            'noticiaList' => $this->moduloList)
        );
    }

    public function load($url, $id) {

        $this->_loadAtivo($id);
        
        if (is_instanceof('Codando\Modulo\Noticia', $this->modulo) === false) {
            $this->app->redirect('/erro404');
            $this->app->stop();
            return false;
        }
		
		meta()->addMeta('title', 'Notícias')
          ->addMeta('title', $this->modulo->getTitulo())
          ->addMeta('description', $this->modulo->getResumo());
    

		/* @var $imagemTemp \Codando\Modulo\Arquivo  */
		$imagemTemp = is_instanceof('Codando\Modulo\Arquivo', $this->modulo->getArquivo()) ? $this->modulo->getArquivo() :NULL;

		meta()->setMeta('og:url', $config['root'] . $this->modulo->getUrl());

		If($imagemTemp != NULL){
			meta()->setMeta('og:image', $config['root'] . $imagemTemp->getImage(600, 600));    
		}
		
        tpl()->display('noticia_view', array('menuCurrent' => 'noticia', 'noticia' => $this->modulo));
    }

    public function __construct() {

        $this->app = Slim::getInstance();
    }

    public function __destruct() {
        
    }

}
