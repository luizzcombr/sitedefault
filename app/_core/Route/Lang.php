<?php

namespace Codando\Route;

use Slim\Slim as Slim;

class Lang {

    public static function check() {

        $app = Slim::getInstance();
        $translate = \Codando\App::getConfig('translate');
        
        $urlUri =$app->request()->getResourceUri();
        
        $languri = substr($urlUri, 1, 2);

        $lang = in_array($languri, array_keys($translate['routes'])) ? $languri : NULL;
       
        if ($lang === NULL || isset($translate['routes'][$lang]) == false) {
            $app->redirect('/' . $translate['default']);
            $app->stop();
            return;
        }
        
        $ididioma = $translate['routes'][$lang];

        $idioma = app()->loadModulo('idioma:cache', [' id_idioma = :ididioma ', compact('ididioma')]);

        if (is_modulo('idioma', $idioma) === false) {
            $app->redirect('/' . $translate['default']);
            $app->stop();
            return;
        }
        
        $urlcurrent = substr($urlUri, 3, strlen($urlUri));
        
        tpl()->addvar(compact('idioma','lang','urlcurrent'));

        $app->config('idioma', $idioma);
        $app->config('lang', $lang);
        
        input()->setSession('id_idioma', $idioma->getId());
        
        trans()->lang($lang);

        return true;
    }

    public function __construct() {

    }

    public function __destruct() {
        
    }

}
