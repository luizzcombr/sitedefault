<?php

namespace Codando\Route;

use \Slim\Middleware as SlimMiddleware;

class Visitante extends SlimMiddleware {

    public function __construct() {}

    public function call() {

        if (!$this->app->request->isGet()) {
            $this->next->call();
            return;
        }

        $HTTP_USER_AGENT = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : NULL;
        $ref = (isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : NULL);  
        $visitante = md5('urbs' . USER_IP . $HTTP_USER_AGENT);

        $key = $this->app->request()->getResourceUri();

        $dbconnection = db()->getConnection();
        $dbconnection->executeUpdate(" REPLACE INTO visitante (ip, data, useragent, token, referencia, lastpage) VALUES ('".USER_IP."', '".date('Y-m-d H:i:s')."', '".$HTTP_USER_AGENT."', '".$visitante."', '".$ref."', '".$key."') ; ");

        input()->setSession('visitante', $visitante);
        
        $this->next->call();
    }

}
