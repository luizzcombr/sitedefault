<?php

namespace Codando\System;

/**
 * Classe para gerenciamento de Debug com o site
 */
class Debug {

    private static $instance = NULL;

    public static function write($msg, $caller) {
        //args
        $debugs = isset($caller['file']) ?  $caller['file']:'';
        $debugs .= isset($caller['line']) ?  '['.$caller['line'].']':'';
        $debugs .= isset($caller['class']) ?  ':'.$caller['class']:'';
        $debugs .= isset($caller['function']) ?  '->'.$caller['function']:'';
        $debugs .= isset($caller['args']) ? '('.implode('","', array_values($caller['args'])) . ')':'';
        
        @error_log(date('Y-m-d-H:i:s') . " - " . USER_IP . " - " . $debugs ." - " . $msg . "\r\n", 3, COD_DIR_LOG . "/debug.log");        
    }

    public function __construct() {

        self::$instance = $this;
    }

    public function __destruct() {
        
    }

    public function __toString() {
        
    }

    public static function get_conf() {
        static $instance = null;

        return $instance ?: $instance = new static;
    }

}
