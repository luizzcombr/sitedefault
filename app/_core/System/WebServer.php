<?php

namespace Codando\System;

use Codando\Controller as Controller,
    Slim\Slim as Slim;

/**
 * Classe para gerenciamento da API
 * @version 1.0
 */
class WebServer {

    private $token = 'uNfuibA3UllnB4W46eGpewQrqq8jMJl8';

    public function run($modulo, $action, $id = NULL) {

        $token = isset($_REQUEST['token']) ? $_REQUEST['token'] : NULL;

        if ($token === $this->token) {

            @error_log(date('Y-m-d-H:i') . " MODULO " . $modulo . " ACTION " . $action . " " . $id . " \r\n", 3, COD_DIR_LOG . "/WS.log");

            $acmodulo = ucwords($modulo);

            switch ($action) {

                case 'del':
                    $this->{$action . $acmodulo}();
                    break;

                case 'update':
                    $this->{$action . $acmodulo}();
                    break;

                case 'insert':
                    $this->{$action . $acmodulo}();
                    break;

                case 'list':
                    $this->{$action . $acmodulo}();
                    break;

                case 'load':
                    $this->{$action . $acmodulo}($id);
                    break;
            }
        } else {

            $this->json([$token, $modulo, $action, $id]);
        }
    }

    private function loadAnuncio($id) {

        $anuncioController = new Controller\Anuncio();
        $anuncioController->_load($id);

        /* @var $anuncio \Codando\Modulo\Anuncio */
        $anuncio = $anuncioController->getModulo();

        if (is_modulo('anuncio', $anuncio)) {

            $anunciocontatoList = app()->loadModulo('anunciotelefone', ' id_anuncio = ' . $anuncio->getId());

            $anuncio->setAnunciocontatoList($anunciocontatoList);

            $this->json([ 'anuncio' => $anuncio->getObjectVars()]);
            return;
        }

        $this->json([ 'erro' => 'nao encontrado']);
    }
    
    private function insertCadastro() {

        $cadastroController = new Controller\Cadastro();
        $cadastroController->_insert(false);
        
        /* @var $cadastro \Codando\Modulo\Cadastro */
        $cadastro = $cadastroController->getModulo();

        if (is_modulo('cadastro', $cadastro)) {
            
            $this->json([ 'cadastro' => $cadastro->getObjectVars()]);
            return;
        }

        $this->json([ 'erro' => 'nao encontrado']);
    }
    
    private function insertAnuncio() {

        $anuncioController = new Controller\Anuncio();
        $anuncioController->_insert();
        
        /* @var $anuncio \Codando\Modulo\Anuncio */
        $anuncio = $anuncioController->getModulo();

        if (is_modulo('anuncio', $anuncio)) {
            
            $this->json([ 'anuncio' => $anuncio->getObjectVars()]);
            return;
        }

        $this->json([ 'erro' => 'nao encontrado']);
    }
    
    private function listCidade() {

        $cidadeController = new Controller\Cidade();
        $cidadeController->setRegistrosPorPagina(10);
        $cidadeController->_list();

        /* @var $cidadeList <Array>\Codando\Modulo\Cidade */
        $cidadeList = $cidadeController->getModuloList();

        $cidades = [];

        /* @var $cidadeTemp \Codando\Modulo\Cidade */
        foreach ($cidadeList as $cidadeTemp) {
            array_push($cidades, $cidadeTemp->getObjectVars());
        }

        $paginaatual = (int) (isset($_REQUEST['p']) ? $_REQUEST['p'] : 1);
        $totalpagina = $cidadeController->getTotalPaginar();

        $this->json([ 'cidades' => $cidades, 'totalpagina' => $totalpagina, 'paginaatual' => $paginaatual]);
    }
    
    private function listAnuncio() {

        $anuncioController = new Controller\Anuncio();
        $anuncioController->setRegistrosPorPagina(10);
        $anuncioController->_list();

        /* @var $anuncioList <Array>\Codando\Modulo\Anuncio */
        $anuncioList = $anuncioController->getModuloList();

        $anuncios = [];

        /* @var $anuncioTemp \Codando\Modulo\Anuncio */
        foreach ($anuncioList as $anuncioTemp) {
            array_push($anuncios, $anuncioTemp->getObjectVars());
        }

        $paginaatual = (int) (isset($_REQUEST['p']) ? $_REQUEST['p'] : 1);
        $totalpagina = $anuncioController->getTotalPaginar();

        $this->json([ 'anuncios' => $anuncios, 'totalpagina' => $totalpagina, 'paginaatual' => $paginaatual]);
    }

    private function loadCategoria($id) {

        $categoriaController = new Controller\Categoria();
        $categoriaController->_load($id);

        /* @var $categoria \Codando\Modulo\Categoria */
        $categoria = $categoriaController->getModulo();

        if (is_modulo('anuncio', $categoria)) {

            $this->json([ 'categoria' => $categoria->getObjectVars()]);
            return;
        }

        $this->json([ 'erro' => 'nao encontrado']);
    }

    private function listCategoria() {

        $categoriaController = new Controller\Categoria();
        $categoriaController->setRegistrosPorPagina(10);
        $categoriaController->_list();

        /* @var $categoriaList <Array>\Codando\Modulo\Categoria */
        $categoriaList = $categoriaController->getModuloList();

        $categorias = [];

        /* @var $categoriaTemp \Codando\Modulo\Categoria */
        foreach ($categoriaList as $categoriaTemp) {
            array_push($categorias, $categoriaTemp->getObjectVars());
        }

        $paginaatual = (int) (isset($_REQUEST['p']) ? $_REQUEST['p'] : 1);
        $totalpagina = $categoriaController->getTotalPaginar();

        $this->json([ 'categorias' => $categorias, 'totalpagina' => $totalpagina, 'paginaatual' => $paginaatual]);
    }
    
    private function listSubcategoria() {

        $subcategoriaController = new Controller\Subcategoria();
        $subcategoriaController->setRegistrosPorPagina(10);
        $subcategoriaController->_list();

        /* @var $subcategoriaList <Array>\Codando\Modulo\Subcategoria */
        $subcategoriaList = $subcategoriaController->getModuloList();

        $subcategorias = [];

        /* @var $subcategoriaTemp \Codando\Modulo\Subcategoria */
        foreach ($subcategoriaList as $subcategoriaTemp) {
            array_push($subcategorias, $subcategoriaTemp->getObjectVars());
        }

        $paginaatual = (int) (isset($_REQUEST['p']) ? $_REQUEST['p'] : 1);
        $totalpagina = $subcategoriaController->getTotalPaginar();

        $this->json([ 'subcategorias' => $subcategorias, 'totalpagina' => $totalpagina, 'paginaatual' => $paginaatual]);
    }

    private function loadCadastro($id) {

        $cadastroController = new Controller\Cadastro();
        $cadastroController->_load($id);

        /* @var $cadastro \Codando\Modulo\Cadastro */
        $cadastro = $cadastroController->getModulo();

        if (is_modulo('cadastro', $cadastro)) {

            $this->json([ 'cadastro' => $cadastro->getObjectVars()]);
            return;
        }

        $this->json([ 'erro' => 'nao encontrado']);
    }

    private function listCadastro() {

        $cadastroController = new Controller\Cadastro();
        $cadastroController->setRegistrosPorPagina(10);
        $cadastroController->_list();

        /* @var $cadastroList <Array>\Codando\Modulo\Cadastro */
        $cadastroList = $cadastroController->getModuloList();

        $cadastros = [];

        /* @var $cadastroTemp \Codando\Modulo\Cadastro */
        foreach ($cadastroList as $cadastroTemp) {
            array_push($cadastros, $cadastroTemp->getObjectVars());
        }

        $paginaatual = (int) (isset($_REQUEST['p']) ? $_REQUEST['p'] : 1);
        $totalpagina = $cadastroController->getTotalPaginar();

        $this->json([ 'cadastros' => $cadastros, 'totalpagina' => $totalpagina, 'paginaatual' => $paginaatual]);
    }

    private function json($responde = array()) {
        header('Content-type: application/json');
        echo json_encode((array) $responde);
        Slim::getInstance()->stop();
        return;
    }

    public function __construct() {
        
    }

    public function __destruct() {
        
    }

}
