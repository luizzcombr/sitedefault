<?php

namespace Codando\System;

/*
 * @version 1.0 
 */

class Video {

    private static $instance = NULL;

    /**
     * get youtube video ID from URL
     *
     * @param string $url
     * @return string Youtube video id or FALSE if none found. 
     */
    public function youtube_id_from_url($url) {
        $url = preg_replace('~
        # Match non-linked youtube URL in the wild. (Rev:20111012)
        https?://         # Required scheme. Either http or https.
        (?:[0-9A-Z-]+\.)? # Optional subdomain.
        (?:               # Group host alternatives.
          youtu\.be/      # Either youtu.be,
        | youtube\.com    # or youtube.com followed by
          \S*             # Allow anything up to VIDEO_ID,
          [^\w\-\s]       # but char before ID is non-ID char.
        )                 # End host alternatives.
        ([\w\-]{11})      # $1: VIDEO_ID is exactly 11 chars.
        (?=[^\w\-]|$)     # Assert next char is non-ID or EOS.
        (?!               # Assert URL is not pre-linked.
          [?=&+%\w]*      # Allow URL (query) remainder.
          (?:             # Group pre-linked alternatives.
            [\'"][^<>]*>  # Either inside a start tag,
          | </a>          # or inside <a> element text contents.
          )               # End recognized pre-linked alts.
        )                 # End negative lookahead assertion.
        [?=&+%\w-]*        # Consume any URL (query) remainder.
        ~ix', '$1', $url);
        return $url;
    }

    public function vimeo_id_from_url($link) {

        $video_id = substr(parse_url($link, PHP_URL_PATH), 1);
        $id = (array) explode('/', $video_id);

        return end($id);
    }

    public function getVimeoInfo($link) {

        $id = $this->vimeo_id_from_url($link);

        if (!function_exists('curl_init'))
            die('CURL is not installed!');

        $ch = @curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://vimeo.com/api/v2/video/" . $id . ".php");
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $output = unserialize(curl_exec($ch));
        $output = $output[0];
        curl_close($ch);
        return $output;
    }

    public function videoThumb($url) {

        $arq = new \Codando\Modulo\Arquivo();

        if (strpos($url, 'youtube') > 0) {

            $id = youtube_id_from_url($url);
            if ($id != NULL) {
                return "http://i.ytimg.com/vi/" . $id . "/hqdefault.jpg";
            }
        } elseif (strpos($url, 'vimeo') > 0) {
            $info = getVimeoInfo($url);
            if (is_array($info) && isset($info['thumbnail_medium'])) {
                return $info['thumbnail_medium'];
            }
        }

        return $arq->getImage();
    }

    public function videoPlay($url) {

        if (strpos($url, 'youtube') > 0) {

            $id = $this->youtube_id_from_url($url);
            if ($id != NULL) {
                return "//www.youtube.com/embed/" . $id;
            }
        }
        
        if (strpos($url, 'vimeo') > 0) {
            
            $id = $this->vimeo_id_from_url($url);
            
            if ($id != NULL) {
                return "//player.vimeo.com/video/" . $id;
            }
        }
        
    }

    public static function get_video() {

        static $instance = null;

        return $instance ? : $instance = new static;
    }

}
