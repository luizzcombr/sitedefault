<?php

namespace Codando\System;

/**
 * Classe para criptografia basica
 * @author Luizz.com.br
 */
class CriptBasic {

    private static $instance;

    /**
     * Encryption key. 
     * 
     * @access  private 
     * @var     string 
     */
    private $password = "wwwluizzcombr";

    /**
     * Cipher($algo, $mode, $source) 
     * 
     * Cipher constructor. Sets the algorithm being used, the encryption 
     * mode, and the IV. 
     * 
     * @param   string $algo 
     * @param   string $mode 
     * @param   integer $source (randomization source) 
     * @access  public 
     * @return  void  
     */
    public function __construct() {
        
        $date = date('YmdH');
        
        $this->password = 'rdhdh465rd4h56rds4h56dr' . $date . base64_encode((isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'boot') . getenv('REMOTE_ADDR')) . 'gsegse4ges489ges498gse984g98es49j8rt4jt';
 
        return $this;
    }

    public function set_key($password) {
        $this->password = $password;
        return $this;
    }

    public function get_rnd_iv($iv_len) {
        $iv = '';
        while ($iv_len-- > 0) {
            $iv .= chr(mt_rand() & 0xff);
        }
        return $iv;
    }

    public function enc($plain_text, $iv_len = 8) {
        $plain_text .= "\x13";
        $n = strlen($plain_text);
        if ($n % 16)
            $plain_text .= str_repeat("\0", 16 - ($n % 16));
        $i = 0;
        $enc_text = $this->get_rnd_iv($iv_len);
        $iv = substr($this->password ^ $enc_text, 0, 512);
        while ($i < $n) {
            $block = substr($plain_text, $i, 16) ^ pack('H*', sha1($iv));
            $enc_text .= $block;
            $iv = substr($block . $iv, 0, 512) ^ $this->password;
            $i += 16;
        }
        return base64_encode($enc_text);
    }

    public function dec($enc_text, $iv_len = 8) {
        $enc_text = base64_decode($enc_text);
        $n = strlen($enc_text);
        $i = $iv_len;
        $plain_text = '';
        $iv = substr($this->password ^ substr($enc_text, 0, $iv_len), 0, 512);
        while ($i < $n) {
            $block = substr($enc_text, $i, 16);
            $plain_text .= $block ^ pack('H*', sha1($iv));
            $iv = substr($block . $iv, 0, 512) ^ $this->password;
            $i += 16;
        }
        return stripslashes(preg_replace('/\\x13\\x00*$/', '', $plain_text));
    }

    public function inputsDec() {

        if (isset($_POST['_csrf_token_']) === true) {
            
            $csrf = $this->dec($_POST['_csrf_token_']);
            
            foreach ($_POST as $key => $value) {
                
                if($key == '_csrf_token_'){
                    continue;
                }
                
                $_key = $this->dec($key);
                
                if(strpos($_key, $csrf) !== false){
                    $_key = str_replace($csrf, "", $_key);
                    $_POST[$_key] = $value;
                }
            }

        }
    }

    /**
     * 
     * @return CriptBasic
     */
    public static function get_cryp() {

        if (is_instanceof('CriptBasic', self::$instance) === FALSE) {

            self::$instance = new CriptBasic();
        }

        return self::$instance;
    }

}
