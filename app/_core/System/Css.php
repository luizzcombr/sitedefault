<?php

namespace Codando\System;

/**
 * Classe para gerenciamento de Css com o site
 */

class Css {
    
    /**
     * Instacia da css
     */
    private static $instance = NULL;

    /**
     * Lista dos arquivos css a serem carregados 
     */
    private $load = array('all'=>array(), 'braille'=>array(), 'embossed'=>array(), 'handheld'=>array(), 'print'=>array(), 'projection'=>array(), 'screen'=>array(), 'speech'=>array(), 'tty'=>array(), 'tv'=>array());
    
    /**
     * Codigo css a ser inserido na pagina (vai ser inserido no rodape)
     */
    private $style = NULL;

    /**
     * Diretorio de css 
     */
    private $base_css = "/css/";
    
    private $browser;

    
    /**
     * Adicionar mais uma chamada de um script
     * @param array || string $load
     * @return Javascript 
     */
    public function _addLoad($load, $media = "all") {

        if (is_array($load) === true) {
            foreach ($load as $loadT) {
                if (in_array($loadT, $this->load[$media]) === false) {
                    $this->load[$media][] = $loadT;
                }
            }
        } else {
            if (in_array($load, $this->load[$media]) === false)
                $this->load[$media][] = $load;
        }

        return $this;
    }
    
    
    public function _addStyle($code) {
        $this->style .= "\n\t" . $code . "\n\t";
        return $this;
    }

    public function _print() {
        
        $load_mul = array();
        $return = "\n";
        foreach ($this->load as $k => $link) {
            foreach ($link as $load){
               if(preg_match('/^(http:|https:)$/',$load))
                $return .= "\t<link rel=\"stylesheet\" type=\"text/css\" href=\"".$this->base_css."/".$load."\" media=\"".$k."\" />\n";
               else
                 $load_mul[] = $load;  
            }
            if(count($load_mul)>0)
            $return .= "\t<link rel=\"stylesheet\" type=\"text/css\" href=\"".$this->base_css.implode(',', $load_mul)."\" media=\"".$k."\" />\n";
            $load_mul = array();
        }

        if($this->style !== NULL)
        if(($this->browser['nome'] == 'IE' && $this->browser['versao'] >= 9) || $this->browser['nome'] != 'IE'){
            $return .= "\n\t<link rel=\"stylesheet\" type=\"text/css\" href=\"data:text/css;base64,".base64_encode($this->style)."\" />\n";
        }else if($this->browser['nome'] == 'IE' && $this->browser['versao'] < 9 ){
            $return .= "\n\t<style type=\"text/css\">".$this->style."</style>";
        }                    

        return $return;
    }

    public function __construct($base = "/css/") {
        
        if (function_exists('browser') === true) {
            $this->browser = browser();
        }
        
        $this->base_css = $base;

        self::$instance = $this;
    }

    public function __destruct() {}

    public function __toString() {
        return $this->_print();
    }

    public function __clone() {
        trigger_error('Clone is not allowed.', E_USER_ERROR);
    }

    public function __wakeup() {
        trigger_error('Unserializing is not allowed.', E_USER_ERROR);
    }
    
    public static function get_css() {
        static $instance = null;

        return $instance ?: $instance = new static;
    }
    
};
