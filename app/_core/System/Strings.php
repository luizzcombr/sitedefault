<?php

namespace Codando\System;

/**
 * @version 2.0 
 */
class Strings {

    private static $instance = NULL;

    public function trocaCaracterEspecial($texto) {

        $trocar = array(
            "/ÁÀÃÂÄ/i" => "A",
            "/áàãâä/i" => "a",
            "/ÉÈÊË/i" => "E",
            "/éèêë/i" => "e",
            "/ÍÏ/i" => "I",
            "/íï/i" => "i",
            "/ÓÒÕÔÖ/i" => "O",
            "/óòõôö/i" => "o",
            "/ÚÙÛÜ/i" => "U",
            "/úùûü/i" => "u",
            "/Ç/i" => "C",
            "/ç/i" => "c",
            "/Ñ/i" => "N",
            "/ñ/i" => "n"
        );

        $texto = preg_replace(array_keys($trocar), array_values($trocar), $texto);

        return $texto;
    }

    public function truncate($str, $length, $etc = '...') {

        $str = trim(html_entity_decode($str, ENT_QUOTES, 'UTF-8'));

        $end = array(' ', '.', ',', ';', ':', '!', '?');
        $length -= strlen($etc);

        if (strlen($str) <= $length)
            return $str;

        if (!in_array($str{$length - 1}, $end) && !in_array($str{$length}, $end))
            while (--$length && !in_array($str{$length - 1}, $end));

        return strip_tags(rtrim(substr($str, 0, $length))) . $etc;
    }

    public function truncateHtml($s, $l, $e = '...', $isHTML = true) {
        $i = 0;
        $tags = array();

        if ($isHTML) {

            preg_match_all('/<[^>]+>([^<]*)/', $s, $m, PREG_OFFSET_CAPTURE | PREG_SET_ORDER);

            foreach ($m as $o) {
                if ($o[0][1] - $i >= $l)
                    break;
                $t = substr(strtok($o[0][0], " \t\n\r\0\x0B>"), 1);
                if ($t[0] != '/')
                    $tags[] = $t;
                elseif (end($tags) == substr($t, 1))
                    array_pop($tags);
                $i += $o[1][1] - $o[0][1];
            }
        }

        return substr($s, 0, $l = min(strlen($s), $l + $i)) . (count($tags = array_reverse($tags)) ? '</' . implode('></', $tags) . '>' : '') . (strlen($s) > $l ? $e : '');
    }

    public function htmlwrap($str, $width = 60, $break = "\n", $nobreak = "") {

        // Split HTML content into an array delimited by < and > 
        // The flags save the delimeters and remove empty variables 
        $content = preg_split("/([<>])/", $str, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);

        // Transform protected element lists into arrays 
        $nobreak = explode(" ", strtolower($nobreak));

        // Variable setup 
        $intag = false;
        $innbk = array();
        $drain = "";

        // List of characters it is "safe" to insert line-breaks at 
        // It is not necessary to add < and > as they are automatically implied 
        $lbrks = "/?!%)-}]\\\"':;&";

        // Is $str a UTF8 string? 
        $utf8 = (preg_match("/^([\x09\x0A\x0D\x20-\x7E]|[\xC2-\xDF][\x80-\xBF]|\xE0[\xA0-\xBF][\x80-\xBF]|[\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}|\xED[\x80-\x9F][\x80-\xBF]|\xF0[\x90-\xBF][\x80-\xBF]{2}|[\xF1-\xF3][\x80-\xBF]{3}|\xF4[\x80-\x8F][\x80-\xBF]{2})*$/", $str)) ? "u" : "";

        while (list(, $value) = each($content)) {
            switch ($value) {

                // If a < is encountered, set the "in-tag" flag 
                case "<": $intag = true;
                    break;

                // If a > is encountered, remove the flag 
                case ">": $intag = false;
                    break;

                default:

                    // If we are currently within a tag... 
                    if ($intag) {

                        // Create a lowercase copy of this tag's contents 
                        $lvalue = strtolower($value);

                        // If the first character is not a / then this is an opening tag 
                        if ($lvalue{0} != "/") {

                            // Collect the tag name    
                            preg_match("/^(\w*?)(\s|$)/", $lvalue, $t);

                            // If this is a protected element, activate the associated protection flag 
                            if (in_array($t[1], $nobreak))
                                array_unshift($innbk, $t[1]);

                            // Otherwise this is a closing tag 
                        } else {

                            // If this is a closing tag for a protected element, unset the flag 
                            if (in_array(substr($lvalue, 1), $nobreak)) {
                                reset($innbk);
                                while (list($key, $tag) = each($innbk)) {
                                    if (substr($lvalue, 1) == $tag) {
                                        unset($innbk[$key]);
                                        break;
                                    }
                                }
                                $innbk = array_values($innbk);
                            }
                        }

                        // Else if we're outside any tags... 
                    } else if ($value) {

                        // If unprotected... 
                        if (!count($innbk)) {

                            // Use the ACK (006) ASCII symbol to replace all HTML entities temporarily 
                            $value = str_replace("\x06", "", $value);
                            preg_match_all("/&([a-z\d]{2,7}|#\d{2,5});/i", $value, $ents);
                            $value = preg_replace("/&([a-z\d]{2,7}|#\d{2,5});/i", "\x06", $value);

                            // Enter the line-break loop 
                            do {
                                $store = $value;

                                // Find the first stretch of characters over the $width limit 
                                if (preg_match("/^(.*?\s)?([^\s]{" . $width . "})(?!(" . preg_quote($break, "/") . "|\s))(.*)$/s{$utf8}", $value, $match)) {

                                    if (strlen($match[2])) {
                                        // Determine the last "safe line-break" character within this match 
                                        for ($x = 0, $ledge = 0; $x < strlen($lbrks); $x++)
                                            $ledge = max($ledge, strrpos($match[2], $lbrks{$x}));
                                        if (!$ledge)
                                            $ledge = strlen($match[2]) - 1;

                                        // Insert the modified string 
                                        $value = $match[1] . substr($match[2], 0, $ledge + 1) . $break . substr($match[2], $ledge + 1) . $match[4];
                                    }
                                }

                                // Loop while overlimit strings are still being found 
                            } while ($store != $value);

                            // Put captured HTML entities back into the string 
                            foreach ($ents[0] as $ent)
                                $value = preg_replace("/\x06/", $ent, $value, 1);
                        }
                    }
            }

            // Send the modified segment down the drain 
            $drain .= $value;
        }

        // Return contents of the drain 
        return $drain;
    }

    public function slugify($text) {

        //http://stackoverflow.com/questions/2955251/php-function-to-make-slug-url-string
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

        // trim
        $text = trim($text, '-');

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // lowercase
        $text = strtolower($text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    public function __clone() {
        trigger_error('Clone is not allowed.', E_USER_ERROR);
    }

    public function __wakeup() {
        trigger_error('Unserializing is not allowed.', E_USER_ERROR);
    }

    public static function get_string() {

        if (self::$instance instanceof Strings === FALSE) {

            self::$instance = new Strings();
        }

        return self::$instance;
    }

}
