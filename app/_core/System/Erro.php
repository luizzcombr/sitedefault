<?php

namespace Codando\System;

/*
 * @version 2.0 
 */

class Erro {

    private static $instance = NULL;

    public function stackTrace(Exception $e) {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $jarr['timer'] = '15000';
            $jarr['status'] = 'no';
            $jarr['info']['stack'][$i = 0] = '<strong>Exception:</strong> ' . $e->getMessage() . '<br />';
            foreach ($e->getTrace() as $t)
                $jarr['info']['stack'][$i] = '#' . $i++ . ' ' . basename(isset($t['file']) ? $t['file'] : __FILE__) . ':' . $t['line'];
            $json = json_encode($jarr);

            exit($json);
        } else {
            $this->setStyle();

            $count = 0;
            $stack = '<div class="code">';
            $stack .= '<strong>Exception:</strong> ' . $e->getMessage() . '<br />';
            foreach ($e->getTrace() as $t)
                $stack .= '<code>&nbsp;<strong>#' . $count++ . '</strong> ' . $t['file'] . ':' . $t['line'] . '</code><code class="trace">' . $this->highlightSource($t['file'], $t['line']) . '</code>';
            $stack .= '</div>';

            exit($stack);
        }
    }

    private function setStyle() {
        $css = '<style type="text/css">';
        $css .= 'body, code  { background:#FAFAFA; font:normal 12px/1.7em Bitstream Vera Sans Mono,Courier New,Monospace; margin:0; padding:0; }';
        $css .= '#code h2 { display:block; color:#000; background:#FFF; font-size:20px; margin:0; padding:10px; border-bottom:solid 1px #999; }';
        $css .= '#code h7{ display:block; color:#FFF; background:#000; font-size:12px; margin:0; padding:2px 5px; }';
        $css .= '.code { margin:8px; padding:0; }';
        $css .= 'code{ display:block; font:inherit; background:#EFEFEF; border:solid 1px #DDD; border-right-color:#BBB; border-bottom:none; margin:10px 10px 0 10px; overflow:auto; }';
        $css .= '.trace,.debug { background:#FFF; border:solid 1px #BBB; border-left-color:#DDD; border-top:none; margin:0 10px 15px 10px; }';
        $css .= '.debug { padding:5px; }';
        $css .= '.number{ color:#AAA; background:#EFEFEF; min-width:40px; padding:0 5px; margin-right:5px; float:left; text-align:right; cursor:default; }';
        $css .= '.highlight{ background:#FFC; }';
        $css .= '</style>';

        print $css;
    }

    private function highlightSource($fileName, $lineNumber, $showLines = 5) {
        $offset = max(0, $lineNumber - ceil($showLines / 2));
        $lines = file_get_contents($fileName);
        $lines = highlight_string($lines, true);
        $lines = array_slice(explode('<br />', $lines), $offset, $showLines);
        $trace = null;

        foreach ($lines as $l):
            $offset++;
            $line = '<div class="number">' . sprintf('%4d', $offset) . '</div>' . $l . '<br />';
            $trace .= ($offset == $lineNumber) ? '<div class="highlight">' . $line . '</div>' : $line;
        endforeach;

        return $trace;
    }

    public function __clone() {
        trigger_error('Clone is not allowed.', E_USER_ERROR);
    }

    public function __wakeup() {
        trigger_error('Unserializing is not allowed.', E_USER_ERROR);
    }

    public static function get_erro() {

        if (self::$instance instanceof Erro === FALSE) {

            self::$instance = new Erro();
        }

        return self::$instance;
    }

}
