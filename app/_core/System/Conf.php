<?php

namespace Codando\System;

/**
 * Classe para gerenciamento de Meta com o site
 * @version 2.0 
 */
class Conf {
    /*
     * Instacia da Conf
     */

    private static $instance = NULL;

    public function addMeta($name, $content) {

        if (is_array($name) === false) {
            $name = array('name', $name);
        }

        if (strtolower($name[1]) == 'title') {
            $this->title = $content . ($this->title != NULL ? " - " . $this->title : NULL);
        }

        if (array_key_exists($name[1], $this->meta) === false) {
            $this->meta[$name[1]] = array($name[0] => $name[1], 'content' => $content);
        } else {
            $this->meta[$name[1]]['content'] = $content . ($this->meta[$name[1]]['content'] != NULL ? " - " . $this->meta[$name[1]]['content'] : NULL);
        }

        return $this;
    }

    public function setMeta($name, $content) {

        if (array_key_exists($name, $this->meta) === true) {
            $this->meta[$name]['content'] = $content;
        } else {
            $this->addMeta($name, $content);
        }

        return $this;
    }

    public function getMeta($name, $unset = false) {

        $meta = isset($this->meta[$name]) === true ? $this->meta[$name] : NULL;

        if ($meta !== NULL && is_array($this->meta[$name])) {
            $meta = "\n\t<meta";
            foreach ($this->meta[$name] as $key => $value) {
                if (is_null($value) === FALSE && is_null($key) === FALSE) {
                    $meta .= " " . $key . "=\"" . str_replace("\"", "'", $value) . "\"";
                }
            }
            $meta .= "/>";
        }

        if ($unset !== false)
            unset($this->meta[$name]);

        return $meta;
    }

    public function setLink($nome, $value) {
        $this->link[$nome] = $value;
    }

    private function _print() {

        $html = array();

        //$html[] = 
        $this->getMeta('title', true);
        $html[] = "\n\t<meta charset=\"utf-8\">";
        $html[] = "\n\t<title>" . $this->title . "</title>";
        $this->addMeta(array('property', 'og:title'), $this->title)
                ->addMeta(array('property', 'og:description'), (array_key_exists('description', $this->meta) ? $this->meta['description']['content'] : NULL));

        foreach ($this->meta as $k => $v) {
            $html[] = $this->getMeta($k, true);
        }

        foreach ($this->link as $k => $v) {
            $html[] = "\n\t<link rel=\"" . $k . "\" href=\"" . $v . "\"/>";
        }

        return implode("", $html) . "\n";
    }

    public function __toString() {
        return $this->_print();
    }

    public function __construct() {

        self::$instance = $this;

        if (function_exists('browser') === true) {
            $this->browser = browser();
        }

        global $_config_site;

        $_config = $_config_site;

        $this->addMeta('title', $_config['title'])
                ->addMeta('web_author', $_config['web_author'])
                ->addMeta('copyright', $_config['copyright'])
                ->addMeta('language', $_config['language'])
                ->addMeta('description', $_config['description'])
                ->addMeta('google', $_config['google-translate'])
                ->addMeta('robots', $_config['google-robots'])
                ->addMeta(array('property', 'og:site_name'), $_config['title'])
                ->addMeta(array('property', 'og:image'), $_config['og_logo']);

        if ($this->browser['nome'] == 'IE') {
            $this->addMeta(array('http-equiv', 'imagetoolbar'), "no");
        }

        $_config = NULL;
    }

    public function __destruct() {
        
    }

    public function __clone() {
        trigger_error('Clone is not allowed.', E_USER_ERROR);
    }

    public function __wakeup() {
        trigger_error('Unserializing is not allowed.', E_USER_ERROR);
    }

    public static function get_meta() {

        static $instance = null;

        return $instance ?: $instance = new static;
    }

}
