<?php

namespace Codando\System;

/**
 * Description of Cache
 *
 * @author Luizz
 */
class Cache {

    private $time;
    private $dir_cache;
    private $ext;

    public function get($name, $callback = NULL, $t = FALSE, $p = array()) {

        $time = $t === FALSE ? $this->time : $t;

        $file = $this->dir_cache . $name . '.' .$this->ext;

        if (file_exists($file) === FALSE) {

            if ($callback !== NULL) {

                $returnCall = call_user_func($callback, $p);

                $this->createCache($name, $returnCall);

                return $returnCall;
            } else {

                return TRUE;
            }
        }

        $now = time();
        $since = filemtime($file);

        if ($time !== NULL && ($now - $since > $time)) {

            if ($callback !== NULL) {

                $returnCall = call_user_func($callback, $p);

                $this->createCache($name, $returnCall);

                return $returnCall;
            } else {

                return TRUE;
            }
        }

        return $this->readCache($name);
    }

    public function createCache($name, $output = NULL) {

       if($output == NULL){
           @unlink($this->dir_cache . $name . '.' .$this->ext);
           return FALSE;
       }

        if (!$fp = @fopen($this->dir_cache . $name . '.' .$this->ext, 'wb')) {
            return FALSE;
        }

        flock($fp, LOCK_EX);
        fwrite($fp, serialize($output));
        flock($fp, LOCK_UN);
        fclose($fp);
        $fp = NULL;
        unset($fp);
    }

    public function readCache($name) {

        $file = $this->dir_cache . $name . '.' .$this->ext;

        if (!file_exists($file)) {
            return FALSE;
        }

        if (function_exists('file_get_contents')) {
            return unserialize(file_get_contents($file));
        }

        if (!$fp = @fopen($file, 'rb')) {
            return FALSE;
        }

        flock($fp, LOCK_SH);

        $data = '';
        if (filesize($file) > 0) {
            $data = fread($fp, filesize($file));
        }

        flock($fp, LOCK_UN);
        fclose($fp);
        $fp = NULL;
        unset($fp);

        return unserialize($data);
    }


    public function __construct($time, $dir_cache, $ext = 'txt') {
        $this->time = $time;
        $this->dir_cache = $dir_cache;
        $this->ext = $ext;
    }

}
