<?php

namespace Codando\System;

/**
 * Classe Translate 
 * /
 * @package Codando
 */
class Translate {
    
    private $trans_config;
    private $lang = 'en';
    private $pages = array();
    private $feedbacks = array();
    private $dir;
    
    public function __construct() {

        $this->trans_config = \Codando\App::getConfig('translate');
        
        if (is_array($this->trans_config)) {
			
            $this->dir = COD_DIR_APP . '/' . $this->trans_config['dir'] . '/';

        } else {

            exit('Translate not config');
        }
    }
    
    public function lang($lang){
        $this->lang = $lang;
    }

    public function page($page){
        
        $page = str_replace('\\', '', $page);
        $page = str_replace('.', '', $page);
 
        if(isset($this->pages[$page]) === false && file_exists($this->dir . $this->lang  . '/' . $page . '.php') === true){
            $this->pages[$page] = include $this->dir . $this->lang  . '/' . $page . '.php';
        }else{
            $this->pages[$page] = array();
        }
        
        return $this->pages[$page];
    }
    
    public function feedback($controller){
        $controller = str_replace('\\', '', $controller);
        $controller = str_replace('.', '', $controller);
        
        if(isset($this->feedbacks[$controller]) === false && file_exists($this->dir . $this->lang  . '/feedback/' . $controller . '.php') === true){
            $this->feedbacks[$controller] = include $this->dir . $this->lang  . '/feedback/' . $controller . '.php';
        }else{
            $this->feedbacks[$controller] = array();
        }
        
        return $this->feedbacks[$controller];
    }
    
    public function __destruct() {
        $this->config = NULL;
        unset($this->output);
    }

    public static function get_trans() {
        static $instance = null;

        return $instance ? : $instance = new static;
    }

}