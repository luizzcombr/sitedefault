<?php

namespace Codando\System;

/**
 * Description of Paginar
 * @version 2.0 
 */
class Paginar {

    private $numRows;
    private $rowsLimit;
    private $pagnum;
    private $offset;
    private $limit;
    private $numPages;
    private $baseUrl;
    private $numMaxShowPag;
    private $numMaxAditionalPages;

    public function getOffSet() {

        return $this->offset;
    }

    public function getLimit() {

        return $this->limit;
    }

    public function getNumPages() {

        return $this->numPages;
    }

    public function setbaseUrl($url) {

        $this->baseUrl = $url;
    }

    public function calcular() {

        $this->numPages = ceil($this->numRows / $this->rowsLimit);

        if ($this->pagnum > $this->numPages) {

            $this->pagnum = $this->numPages;
        } else if ($this->pagnum < 1) {

            $this->pagnum = 1;
        }

        $this->offset = $this->rowsLimit * ($this->pagnum - 1);
        $this->limit = $this->rowsLimit;

        $this->numMaxAditionalPages = floor($this->numMaxShowPag / 2);

        $this->pagStart = 1;
        $numMaxShowPag = $this->numPages < $this->numMaxShowPag ? $this->numPages : $this->numMaxShowPag;
        $this->pagEnd = $numMaxShowPag;

        if ($this->pagnum > $this->numMaxAditionalPages) {

            $aditionalPages = $this->numMaxAditionalPages;

            if (($this->pagnum + $aditionalPages) > $this->numPages) {

                $aditionalPages = $this->numPages - $this->pagnum;
            }

            $this->pagEnd = $this->pagnum + $aditionalPages;
            $this->pagStart = $this->pagEnd - $numMaxShowPag + 1;
        }
    }

    public function getPaginar() {

        if ($this->numPages > 1) {

            return $this->getPaginator();
        } else {

            return NULL;
        }
    }

    public function getPaginator() {

        $namePag = 'paginar sessão';
        
        if(defined('IDI_URL') === TRUE)
            switch (IDI_URL) {
                case 'pt':
                    $namePag = 'paginar sessão';
                    break;
                case 'en':
                    $namePag = 'page session';
                    break;
                case 'es':
                    $namePag = 'página de registro';
                    break;
            }
        
        return tpl()->display('paginacao', array( 'namePag' => $namePag, 'baseUrl' => $this->baseUrl, 'pagnum' => $this->pagnum, 'numPages'=>$this->numPages, 'numMaxShowPag' => $this->numMaxShowPag ),  false);
        
    }

    public function getnumMaxShowPag() {

        return $this->numMaxShowPag;
    }

    public function getPagStart() {

        return $this->pagStart;
    }

    public function getPagEnd() {

        return $this->pagEnd;
    }

    public function getPagNum() {

        return $this->pagnum;
    }

    public function setNumMaxPag($num) {

        $this->numMaxShowPag = $num;
    }

    public function __construct($numRows, $rowsLimit, $pagnum = NULL, $numMaxShowPag = 10) {

        $this->numRows = $numRows;
        $this->rowsLimit = $rowsLimit;
        $this->pagnum = $pagnum;

        $this->numMaxShowPag = $numMaxShowPag;

        $this->calcular();
    }

    public function __destruct() {

    }

}
