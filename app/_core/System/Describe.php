<?php

namespace Codando\System;

use \PDO;

/**
 * Classe para describe de class Modulo
 * @version 2.0 
 */
class Describe {

    private static $instance;
    private $describe = array();
    private $reg = '/\{(?:[^{}]|(?R))*\}/';
    private $dbconnection;

    public function __construct() {

        $this->erro = false;
        $this->dbconnection = db()->getConnection();
    }

    public function getLabel($describe) {
        $label = $describe->comment != NULL ? $describe->comment : str_replace('_', ' ', ucfirst(strtolower($describe->field)));

        $label = preg_replace($this->reg, '', $label);

        return utf8_encode($label);
    }

    public function isJson($describe) {

        if ($describe->comment !== NULL) {
            $matches = array();
            preg_match($this->reg, $describe->comment, $matches);
            return count($matches) > 0 ? TRUE : FALSE;
        }
        return FALSE;
    }

    public function getJson($describe) {

        $matches = array();

        preg_match($this->reg, $describe->comment, $matches);

        return (array) json_decode(current($matches), true);
    }

    public function getJsonOptions($describe, $value = NULL) {

        $json = $this->getJson($describe);

        $option = array();

        foreach ($json as $key => $val) {

            $option[] = '<option value="' . $json[$key] . '" ' . ($value == $json[$key] ? 'selected' : NULL) . '>' . ($key) . '</option>';
        }

        return implode('', $option);
    }

    public function createClass($modulo) {

        $clssName = $modulo->getClasseNome();

        if (file_exists(COD_DIR_APP . "/_core/Modulo/" . $clssName . ".php") === FALSE) {

            $sourceClassTemp = "<?php";
            $sourceClassTemp .= "\n/**";
            $sourceClassTemp .= "\n* Classe que representa objeto " . str_replace("\\Codando\\Modulo\\", "", $clssName) . " ";
            $sourceClassTemp .= "\n* @package Codando";
            $sourceClassTemp .= "\n*/";
            $sourceClassTemp .= "\n";
            $sourceClassTemp .= "\tnamespace Codando\Modulo;\n";
            $sourceClassTemp .= "\n";
            $sourceClassTemp .= "\tclass " . str_replace("\\Codando\\Modulo\\", "", $clssName) . "{\n";
            $atributosClass = array();
            $getClass = array();
            $setClass = array();
            $describeListTemp = (object) $this->tableDescribeToObjeto($modulo->getTabela(true));
            $describe = array();
            $toString = 'return NULL;';

            foreach ($describeListTemp as $describeTemp) {

                $ex = explode("_", $describeTemp->field);

                $nomeMetodo = $this->toGetMethod($describeTemp->field, '');

                $explodeType = explode('(', $describeTemp->type);
                $describeTemp->type = reset($explodeType);
                $explodeType = NULL;
                unset($explodeType);

                if ($describeTemp->is_key == "PRI") {

                    if ($describeTemp->type == 'int') {

                        $atributosClass[] = "\t\n\tprivate $" . $describeTemp->field . ";\n";
                        $getClass[] = "\t\n\tpublic function getId(){\n\t\treturn (int)\$this->" . $describeTemp->field . ";\n\t}";
                        $setClass[] = "\t\n\tpublic function setId(\$" . $describeTemp->field . "){\n\t\t\$this->" . $describeTemp->field . " = (int)\$" . $describeTemp->field . ";\n\t}";
                        $toString = "\n\t\treturn (string)\$this->" . $describeTemp->field . ";\n\t";
                    }
                } else if ($describeTemp->is_key == "MUL") {
                    $mul_modulo = end($ex);
                    $atributosClass[] = "\t\n\tprivate $" . $describeTemp->field . ";\n";
                    $nomeMetodo = ucwords($mul_modulo);
                    $mul_modulo = NULL;
                    unset($mul_modulo);

                    if ($describeTemp->type == 'varchar') {

                        $getClass[] = "\t\n\tpublic function get" . $nomeMetodo . "(){\n\t\treturn \$this->" . $describeTemp->field . ";\n\t}";
                        $setClass[] = "\t\n\tpublic function set" . $nomeMetodo . "(\$" . $describeTemp->field . "){\n\t\t\$this->" . $describeTemp->field . " = \$" . $describeTemp->field . ";\n\t}";
                    } else {

                        $getClass[] = "\t\n\tpublic function get" . $nomeMetodo . "(){\n\t\treturn \$this->" . $describeTemp->field . ";\n\t}";
                        $setClass[] = "\t\n\tpublic function set" . $nomeMetodo . "(\$" . $describeTemp->field . "){\n\t\t\$this->" . $describeTemp->field . " = \$" . $describeTemp->field . ";\n\t}";
                    }
                } else {

                    switch ($describeTemp->type) {
                        case 'date':
                            $getClass[] = "\t\n\tpublic function get" . $nomeMetodo . "(\$format='d/m/Y'){\n\t\treturn \$this->" . $describeTemp->field . " !== NULL ? date(\$format,strtotime(\$this->" . $describeTemp->field . ")):NULL;\n\t}";
                            break;
                        case 'datetime':
                            $getClass[] = "\t\n\tpublic function get" . $nomeMetodo . "(\$format='d/m/Y H:i'){\n\t\treturn \$this->" . $describeTemp->field . " !== NULL ? date(\$format,strtotime(\$this->" . $describeTemp->field . ")):NULL;\n\t}";
                            break;
                        default:
                            $getClass[] = "\t\n\tpublic function get" . $nomeMetodo . "(){\n\t\treturn \$this->" . $describeTemp->field . ";\n\t}";
                            break;
                    }

                    $atributosClass[] = "\t\n\tprivate $" . $describeTemp->field . ";\n";
                    $setClass[] = "\t\n\tpublic function set" . $nomeMetodo . "(\$" . $describeTemp->field . "){\n\t\t\$this->" . $describeTemp->field . " = \$" . $describeTemp->field . ";\n\t}";
                }


                $describe[] = serialize(array(
                    'field' => $describeTemp->field,
                    'is_key' => $describeTemp->is_key,
                    'is_null' => $describeTemp->is_null,
                    'type' => $describeTemp->type,
                    'comment' => $describeTemp->comment,
                    'max_length' => $describeTemp->max_length,
                    'default' => $describeTemp->defaults));
            }

            $describeListTemp = NULL;

            unset($describeListTemp);
            $temarquivo = NULL;
            if ($modulo->getArquivo() == 1) {

                $temarquivo = "\t\n\tuse TemArquivo;\n";
            }

            //$atributosClass[] = "\t\n\tprivate \$decribe = array(\n\t'".implode("',\n\t'",$describe)."'\n\t);\n";
            //$getClass[] = "\t\n\tpublic function getDescribe(){\n\t\treturn \$this->decribe;\n\t}";

            $setClass[] = "\t\n\tpublic function isEquals(\$isEqual) {return (\$isEqual instanceof " . str_replace("\\Codando\\Modulo\\", "", $clssName) . " && \$this->getId() == \$isEqual->getId());}";
            $setClass[] = "\t\n\tpublic function getObjectVars(){ return get_object_vars(\$this); }";
            $setClass[] = "\t\n\tpublic function __toString(){" . $toString . "}";
            $setClass[] = "\t\n\tpublic function __construct(){}";
            $setClass[] = "\t\n\tpublic function __destruct(){}";

            $sourceClassTemp .= $temarquivo . implode("", $atributosClass) . implode("", $getClass) . implode("", $setClass) . "\n}\n";

            try {

                $clssName = str_replace("\\Codando\\Modulo\\", "", $clssName);

                $file = @fopen(COD_DIR_APP . "/_core/Modulo/" . $clssName . ".php", "w+");

                if (!$file || !fwrite($file, ($sourceClassTemp))) {

                    exit("++ ERRO: Ocorreu uma falha ao gravar o arquivo[" . COD_DIR_APP . "/_core/Modulo/" . $clssName . ".php" . "]");
                }

                fclose($file);

                unset($file);
            } catch (Exception $e) {

                return $e->getMessage();
            }
        }

        return TRUE;
    }

    public function toGetMethod($extract, $type = 'get', $sep = "") {
        return $type . ucwords(str_replace(array("id_", "_"), array("", $sep), $extract));
    }

    public function isFieldsHidden($describe) {

        return ($describe->is_key == 'PRI' || $describe->is_key == 'MUL' || substr($describe->field, 0, 3) == 'url' || substr($describe->field, 0, 2) == 'id');
    }

    public function isFieldsForeign($describe) {

        return ($describe->is_key != 'PRI' && $describe->is_key == 'MUL' && substr($describe->field, 0, 2) == 'id');
    }

    public function getIdAndUnset($array, $mod = NULL) {

        $newArray = array();

        if ($mod === NULL)
            $mod = $this->getModulo();

        $pri = $this->describeByFieldPrimariKey($mod->getTabela(true));

        $newArray[$pri] = $array[$pri];

        unset($array[$pri]);

        $is = is();

        foreach ($array as $k => $value) {

            if ($is->nul($value) === true) {
                $array[$k] = NULL;
                unset($array[$k]);
            }
        }
        $is = NULL;
        unset($is);

        return (object) array('fields' => $array, 'where' => $newArray);
    }

    public function tableDescribeToObjeto($table_name, $foreign = false) {

        //$table = $foreign !== false ? $table_name . "_foreign" : $table_name;
        $table = $table_name;
        $config = \Codando\App::getConfig('database');

        if (array_key_exists($table, $this->describe) === true) {

            return $this->describe[$table];
        } else {

            $foreign_sql = "SELECT * FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE CONSTRAINT_NAME != 'PRIMARY' AND TABLE_NAME = '" . $table_name . "';";

            $describe_sql = "SELECT column_name AS field, is_nullable AS is_null, data_type AS type, character_maximum_length AS max_length,  column_key AS is_key,  column_default AS defaults, column_comment AS comment FROM  INFORMATION_SCHEMA.Columns WHERE TABLE_SCHEMA = '" . $config['name'] . "' AND table_name = '" . $table_name . "'";

            $queryF = NULL;

            $queryD = $this->dbconnection->executeQuery($describe_sql);

            if ($foreign !== false) {
                $queryF = $this->dbconnection->executeQuery($foreign_sql);
            }

            $describeTemp = NULL;

            if ($this->erro === false) {

                try {

                    $describeTemp = $queryD->fetchAll(PDO::FETCH_OBJ);

                    if ($foreign !== false) {
                        $describeTemp = (object) array_merge((array) $describeTemp, array('foreign' => $queryF->fetchAll(PDO::FETCH_OBJ)));
                    }
                } catch (PDOException $e) {

                    $this->erro = true;
                    $this->msg['DESCRIBE'] = $e->getMessage();
                    return NULL;
                }

                //Cache form_admin
                $this->describe[$table] = $describeTemp;
            }

            unset($queryD, $queryF);
        }

        return $this->describe[$table];
    }

    /**
     * @todo
     * @param type $table
     * @param type $referencia
     * @return type
     */
    protected function describeDepend($table, $referencia = NULL) {
        return [];
        $return = array();

        try {

            $query = $this->dbconnection->executeQuery("SELECT * FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE REFERENCED_TABLE_NAME = '" . $table . "';");
            $describe = $query->fetchAll(PDO::FETCH_OBJ);

            unset($query);
        } catch (PDOException $e) {

            $this->erro = true;
            $this->msg['DESCRIBE'] = $e->getMessage();
            return NULL;
        }

        foreach ($describe as $foreign) {

            $modulo_temp = new Modulo();
            $modulo_temp->setClasseNome('Modulo');
            $modulo_temp->setTabela('modulo');

            $moduloTemp = db()->_load('modulo', ' tabela = ' . $foreign->table_name);

            unset($modulo_temp);

            if ($moduloTemp instanceof Modulo == true && $moduloTemp->getVisibilidade() >= 3) {

                $moduloList = array();

                if ($referencia !== NULL) {
                    $moduloList = (array) app()->listModulo($moduloTemp, array($foreign->column_name => $referencia->getId()));
                }

                $return[$foreign->table_name] = array(
                    $moduloTemp,
                    $moduloList
                );
            }
        }

        return $return;
    }

    protected function describeByFieldByDefault($table, $type = 1) {

        $fields = array();

        $describe = $this->tableDescribeToObjeto($table);

        if ($describe != NULL && is_array($describe)) {
            foreach ($describe as $describeTemp) {
                $fields[$describeTemp->field] = $describeTemp->defaults;
            }
        }

        if ($type == 1) {
            return ((object) $fields);
        } else {
            return $fields;
        }
    }

    protected function describeByFieldByComments($table, $type = 1) {
        $fields = array();

        $describe = $this->tableDescribeToObjeto($table);

        if ($describe != NULL && is_array($describe)) {
            foreach ($describe as $describeTemp) {
                $fields[$describeTemp->field] = utf8_encode($describeTemp->comment);
            }
        }
        if ($type == 1)
            return ((object) $fields);
        else
            return $fields;
    }

    protected function describeByFieldPrimariKey($table) {
        $fields = NULL;

        $describe = $this->tableDescribeToObjeto($table);

        if ($describe != NULL && is_array($describe)) {
            foreach ($describe as $describeTemp) {
                if ($describeTemp->is_key == 'PRI')
                    $fields = $describeTemp->field;
            }
        }
        return $fields;
    }

}
