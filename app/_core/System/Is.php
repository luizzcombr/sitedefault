<?php
namespace Codando\System;

/*
* @version 2.0 
*/
class Is {

    private static $instance = NULL;
    
    private $er_email = '/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/';

    /**
     * Verifica se a string é nula ou vazia
     * @param string $string String a ser analizada
     * @return bool True para nulo e False para não nulo.
     */
    public function nul($string) {

        $newString = trim($string);

        if (empty($newString) && !is_numeric($string)) {

            return true;
        } else {

            return false;
        }
    }

    /**
     * Verifica se a string numérica é um float válido
     * @param string $float String numérica a ser verificado
     * @return boolean True se for válido, False se for inválido
     */
    public function float($float) {

        $float = str_replace(",", ".", $float);

        return (is_numeric($float) === true);
    }

    /**
     * Verifica se a string numérica é um número inteiro válido
     * @param string $int String numérica a ser verificado
     * @return boolean True se for válido, False se for inválido
     */
    public function int($int) {

        $isInt = false;

        if (is_numeric($int) === true) {

            if ((int) $int == $int) {

                $isInt = true;
            }
        }

        return $isInt;
    }

    /**
     * Verifica se é uma string numérica positiva ( maior do que zero)
     * @param string $string String a ser analizada
     * @return bool True se for string numérica, False se não for numérica
     */
    public function id($string) {
        return ($this->int($string) && $string > 0);
    }

    /**
     * Verifica se o e-mail é valido
     * @param string $email E-mail a ser verificado
     * @return boolean True se for válido, False se for inválido
     */
    public function email($email) {

        $correto = false;

        if (preg_match($this->er_email, $email)) {
            $correto = true;
        }

        return $correto;
    }

    /**
     * Verifica se a data é valida
     * @param string $data Data a ser verificada no formato DD/MM/YYYY
     * @return boolean True se for válido, False se for inválido
     */
    public function date($data) {

        $dateList = explode("/", $data);

        $dia = isset($dateList[0]) ? $dateList[0] : 0;
        $mes = isset($dateList[1]) ? $dateList[1] : 0;
        $ano = isset($dateList[2]) ? $dateList[2] : 0;

        return (@checkdate($mes, $dia, $ano) ? true : false);
    }

    /**
     * Verifica se o intervalo entre duas datas é válido.
     * @param string $data_inicio Data de início no formato DD/MM/YYYY
     * @param string $data_fim Data de início no formato DD/MM/YYYY
     * @return boolean True se for válido, False se for inválido
     */
    public function dateInterval($data_inicio, $data_fim) {

        $data_inicio_en = implode("", array_reverse(explode("/", $data_inicio)));
        $data_fim_en = implode("", array_reverse(explode("/", $data_fim)));

        return ((isNull($data_fim_en) === true) || ($data_inicio_en <= $data_fim_en));
    }

    /**
     * Verifica se a data está dentro do período informado.
     * @param string $date Data a ser verificada no período no formato DD/MM/YYYY
     * @param string $date_start Data inicial do período no formato DD/MM/YYYY
     * @param string $date_end Data final do perído no formado DD/MM/YYYY (Aceita valor nulo)
     * @return boolean True se a data estiver no período, False se não estiver no período.
     */
    public function dateInInterval($date, $date_start, $date_end) {

        $date_en = implode("", array_reverse(explode("/", $date)));
        $date_start_en = implode("", array_reverse(explode("/", $date_start)));
        $date_end_en = implode("", array_reverse(explode("/", $date_end)));

        return ((isNull($date_end_en) === true && $date_en >= $date_start_en) || ($date_en >= $date_start_en && $date_en <= $date_end_en));

        return true;
    }

    /**
     * Verifica se o número de catacteres da string é válido
     * @param string $string String a ser verificada
     * @param int $length Número máximo de caracter permitido para string
     * @return boolean True se for válido, False se for inválido
     */
    public function length($string, $length) {

        $string = trim($string);

        return (strlen($string) <= $length);
    }

    /**
     * Verifica se a URL é válida
     * @param string $string Url a ser verificada
     * @return boolean True se for válido, False se for inválido
     */
    public function url($string) {

        return (preg_match('/^([0-9a-z-]+)$/', $string) ? true : false);
    }

    /**
     * Verifica se o CPF é válido (Ignora ponto e hífen)
     * @param string $cpf CPF a ser verificado
     * @return boolean True se for válido, False se for inválido
     */
    public function cpf($cpf) {

        $cpf = str_pad(preg_replace("([\.\-\/]*)", "", $cpf), 11, '0', STR_PAD_LEFT);

        if (strlen($cpf) != 11 || $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999') {
            return false;
        } else {

            for ($t = 9; $t < 11; $t++) {
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }

                $d = ((10 * $d) % 11) % 10;

                if ($cpf{$c} != $d) {
                    return false;
                }
            }

            return true;
        }
    }

    /**
     * Verifica se o CNPJ é válido (Ignora ponto, híden e barra)
     * @param string $cnpj CNPJ a ser verificado
     * @return boolean True se for válido, False se for inválido
     */
    public function cnpj($cnpj) {

        $cnpj = trim(preg_replace("([\.\-\/]*)", "", $cnpj));

        if (strlen($cnpj) <> 14)
            return false;

        $soma = 0;

        $soma += ($cnpj[0] * 5);
        $soma += ($cnpj[1] * 4);
        $soma += ($cnpj[2] * 3);
        $soma += ($cnpj[3] * 2);
        $soma += ($cnpj[4] * 9);
        $soma += ($cnpj[5] * 8);
        $soma += ($cnpj[6] * 7);
        $soma += ($cnpj[7] * 6);
        $soma += ($cnpj[8] * 5);
        $soma += ($cnpj[9] * 4);
        $soma += ($cnpj[10] * 3);
        $soma += ($cnpj[11] * 2);

        $d1 = $soma % 11;
        $d1 = $d1 < 2 ? 0 : 11 - $d1;

        $soma = 0;
        $soma += ($cnpj[0] * 6);
        $soma += ($cnpj[1] * 5);
        $soma += ($cnpj[2] * 4);
        $soma += ($cnpj[3] * 3);
        $soma += ($cnpj[4] * 2);
        $soma += ($cnpj[5] * 9);
        $soma += ($cnpj[6] * 8);
        $soma += ($cnpj[7] * 7);
        $soma += ($cnpj[8] * 6);
        $soma += ($cnpj[9] * 5);
        $soma += ($cnpj[10] * 4);
        $soma += ($cnpj[11] * 3);
        $soma += ($cnpj[12] * 2);


        $d2 = $soma % 11;
        $d2 = $d2 < 2 ? 0 : 11 - $d2;

        if ($cnpj[12] == $d1 && $cnpj[13] == $d2) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Verifica se a string é um CPF ou CNPJ válido (Ignora ponto, híden e barra)
     * @param string $cpfCnpj CPF ou CNPJ a ser verificado
     * @return boolean True se for válido, False se for inválido
     */
    public function cpfCnpj($cpfCnpj) {

        $cpfCnpj = trim(preg_replace("([\.\-\/]*)", "", $cpfCnpj));

        switch (strlen($cpfCnpj)) {
            case 11: return $this->cpf($cpfCnpj);
                break;
            case 14: return $this->cnpj($cpfCnpj);
                break;
            default: return false;
        }
    }

    /**
     * Verifica se a horario informado é valido
     * @param string $horario Horário a ser verificado no formato HH:MM
     * @return bool Retorna TRUE se o horário for válido, e FALSE se for inválido
     */
    public function horario($horario) {

        $isValidHorario = false;

        if (strlen($horario) == 5) {

            list($hora, $minuto) = explode(":", $horario);

            if ($hora >= 0 && $hora <= 23 && $minuto >= 0 && $minuto <= 59) {
                $isValidHorario = true;
            }
        }

        return $isValidHorario;
    }

    /**
     * Calcula se a data final é maior que a inicial
     * @param string $dataInicial Data a ser verificada no formato DD/MM/YYYY
     * @param string $dataFinal Data a ser verificada no formato DD/MM/YYYY
     * @return bool Retorna TRUE se o período for válido, e FALSE se for inválido
     */
    public function periodo($dataInicial, $dataFinal) {

        $isValidPeriodo = false;

        list($d, $m, $y) = explode("/", $dataInicial);
        $dataInicial = mktime(0, 0, 0, $m, $d, $y);

        list($d, $m, $y) = explode("/", $dataFinal);
        $dataFinal = mktime(0, 0, 0, $m, $d, $y);

        if (($dataFinal - $dataInicial) > 0) {

            $isValidPeriodo = true;
        }

        return $isValidPeriodo;
    }

    /**
     * Validate IP Address
     *
     * Updated version suggested by Geert De Deckere
     *
     * @access	public
     * @param	string
     * @return	string
     */
    public function ip($ip) {
        $ip_segments = explode('.', $ip);

        // Always 4 segments needed
        if (count($ip_segments) != 4) {
            return FALSE;
        }
        // IP can not start with 0
        if ($ip_segments[0][0] == '0') {
            return FALSE;
        }
        // Check each segment
        foreach ($ip_segments as $segment) {
            // IP segments must be digits and can not be
            // longer than 3 digits or greater then 255
            if ($segment == '' OR preg_match("/[^0-9]/", $segment) OR $segment > 255 OR strlen($segment) > 3) {
                return FALSE;
            }
        }

        return TRUE;
    }

    public function php($version = '5.0.0') {
        static $_is_php;
        $version = (string) $version;

        if (!isset($_is_php[$version])) {
            $_is_php[$version] = (version_compare(PHP_VERSION, $version) < 0) ? FALSE : TRUE;
        }

        return $_is_php[$version];
    }

    /**
     *  ajax Request?
     *
     * Test to see if a request contains the HTTP_X_REQUESTED_WITH header
     *
     * @return 	boolean
     */
    public function ajax_request() {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) === true && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest');
    }

    /**
     *  cli Request?
     *
     * Test to see if a request was made from the command line
     *
     * @return 	boolean
     */
    public function cli_request() {
        return (php_sapi_name() == 'cli') or defined('STDIN');
    }

    /**
     * Tests for file writability
     *
     * is_writable() returns TRUE on Windows servers when you really can't write to
     * the file, based on the read-only attribute.  is_writable() is also unreliable
     * on Unix servers if safe_mode is on.
     *
     * @access	private
     * @return	void
     */
    public function really_writable($file) {
        // If we're on a Unix server with safe_mode off we call is_writable
        if (DIRECTORY_SEPARATOR == '/' AND @ini_get("safe_mode") == FALSE) {
            return is_writable($file);
        }

        // For windows servers and safe_mode "on" installations we'll actually
        // write a file then read it.  Bah...
        if (is_dir($file)) {
            $file = rtrim($file, '/') . '/' . md5(mt_rand(1, 100) . mt_rand(1, 100));

            if (($fp = @fopen($file, FOPEN_WRITE_CREATE)) === FALSE) {
                return FALSE;
            }

            fclose($fp);
            @chmod($file, DIR_WRITE_MODE);
            @unlink($file);
            return TRUE;
        } elseif (!is_file($file) OR ($fp = @fopen($file, FOPEN_WRITE_CREATE)) === FALSE) {
            return FALSE;
        }

        fclose($fp);
        return TRUE;
    }
        
    public function __clone() {
        trigger_error('Clone is not allowed.', E_USER_ERROR);
    }

    public function __wakeup() {
        trigger_error('Unserializing is not allowed.', E_USER_ERROR);
    }

    public static function get_is() {

        if (self::$instance instanceof is === FALSE) {

            self::$instance = new is();
        }

        return self::$instance;
    }
}