<?php

namespace Codando\System;

/*
 * @version 1.0 
 */

class Pagseguro {

    private static $instance = NULL;

    public function register(\Codando\Modulo\Cadastro $cadastro, $pagsegurocode, $itens = array(), $notificationURL = null, $redirectUrl = null) {

        \PagSeguroConfig::init();
        \PagSeguroConfig::activeLog(COD_DIR_APP . "/_log/PagSeguroLogs.txt");

        $_config = \Codando\App::getConfig('pagseguro');

        $code = NULL;

        // Instantiate a new payment request
        $paymentRequest = new \PagSeguroPaymentRequest();

        $paymentRequest->setCurrency("BRL");

        $paymentRequest->setReference($pagsegurocode);

        foreach ($itens as $item) {
            $paymentRequest->addItem($item['id'], $item['titulo'], $item['qt'], $item['valor'], 0, 0);
        }

        $telefoneClean = $cadastro->getTelefone();

        $ddd = substr($telefoneClean, 1, 2);
        $telefone = substr($telefoneClean, 2);

        $cpfnj = format()->mask($cadastro->getCpf(), '###.###.###-##');

        $paymentRequest->setSender(
                $cadastro->getNome() . ' ' . $cadastro->getSobrenome(), $cadastro->getEmail(), $ddd, $telefone, 'CPF', $cpfnj, true
        );

        $paymentRequest->setShippingCost(0);
        
        if($redirectUrl !== null)
            $paymentRequest->setRedirectUrl($redirectUrl);
        
        if($notificationURL !== null)
        $paymentRequest->addParameter('notificationURL', $notificationURL);

        try {

            $credentials = new \PagSeguroAccountCredentials($_config['email'], $_config['token']);

            $code = $paymentRequest->register($credentials, true);
            
        } catch (\PagSeguroServiceException $e) {

            return false;
        }

        return $code;
    }

    public function searchByReference($reference) {

        $udados = [];

        $_config = \Codando\App::getConfig('pagseguro');

        try {

            \PagSeguroConfig::init();
            \PagSeguroConfig::activeLog(COD_DIR_APP . "/_log/PagSeguroLogs.txt");

            $credentials = new \PagSeguroAccountCredentials($_config['email'], $_config['token']);

            $searchResult = \PagSeguroTransactionSearchService::searchByReference($credentials, $reference);

            $transactions = $searchResult->getTransactions();

            if (is_array($transactions) === false)
                return false;

            $result = end($transactions);

            if (is_instanceof('PagSeguroTransactionSummary', $result)) {

                $status = 2; //Em Analise

                switch ($result->getStatus()->getValue()) {
                    case '0': //Gerada
                        $status = 2;
                        break;
                    case '2': //Em Analise
                        $status = 2;
                        break;
                    case '3': //Pago
                        $status = 1;
                        break;
                    case '4': //Disponivel
                        $status = 4;
                        break;
                    case '6': //DEVOLVEU
                        $status = 3;
                        break;
                    case '7': //CANCELADO
                        $status = 3;
                        break;
                    case '8': //ESTORNO
                        $status = 3;
                        break;
                    case '9': //CONTESTAÇÃO
                        $status = 3;
                        break;
                }

                $tipopagamento = 4; //Outros

                switch ($result->getType()->getValue()) {
                    case '1': //CC
                        $tipopagamento = 2;
                        break;
                    case '2': //Boleto
                        $tipopagamento = 2;
                        break;
                    case '3': //TRANSFERencia
                        $tipopagamento = 3;
                        break;
                    default: //Outros
                        $tipopagamento = 4;
                        break;
                }

                $udados = compact('status', 'tipopagamento');

                if ($status == 1) {
                    $udados['datapagamento'] = date('Y-m-d H:i:s');
                }

                if ($status == 3) { //Cancelado
                    $udados['status'] = 3;
                    $udados['pagseguro'] = '';
                }
            }

            return false;
            
        } catch (\PagSeguroServiceException $e) {
            
            return false;
        }

        return $udados;
    }

    public static function get_pagseguro() {

        if (self::$instance instanceof Pagseguro === FALSE) {

            self::$instance = new Pagseguro();
        }

        return self::$instance;
    }

}
