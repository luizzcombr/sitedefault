<?php

namespace Codando\System;

use \PDO,
    \Codando\Modulo,
    \Doctrine\DBAL;

/**
 * Classe Banco de Dados, com os methods de comunicação com banco de dados.
 * @version 2.0 
 */
class Db {

    private $erro = false;
    private $msg = array();
    /* @var $doctrine \Doctrine\DBAL\Connection */
    private $doctrine;
    private $sql;
    private $cacheFile;
    private $cache;
    private $cacheoff = false;
    private $config;
    private $log;
    private static $instance;
    private $exception = array(
        'code-1045' => 'Houve uma falha de comunica&ccedil;&atilde;o com a base de dados usando',
        'code-2002' => 'Nenhuma conex&atilde;o p&ocirc;de ser feita porque a m&aacute;quina de destino as recusou ativamente. Este host n&atilde;o &eacute; conhecido.',
        'code-2005' => 'N&atilde;o houve comunica&ccedil;&atilde;o com o host fornecido. Verifique as suas configura&ccedil;&otilde;es.',
        'no-database' => 'Base de dados desconhecida. Verifique as suas configura&ccedil;&otilde;es.',
        'no-instance' => 'N&atilde;o existe uma inst&acirc;ncia do objeto Code DB dispon&iacute;vel. Imposs&iacute;vel acessar os m&eacute;todos.',
        'no-argument-sql' => 'O argumento SQL de consulta est&aacute; ausente.',
        'no-instruction-json' => 'A instrução SQL no formato JSON est&aacute; ausente.',
        'duplicate-key' => 'N&atilde;o foi poss&iacute;vel gravar o registro. Existe uma chave duplicada na tabela.<br />\'%1$s',
        'critical-error' => 'Erro crítico detectado no sistema.',
        'error-badly-json' => 'A query JSON fornecida est&aacute; mal formatada.',
    );

    /**
     * Inicializa e conecta no banco 
     * Utiliza o construtor da superclasse
     */
    public function __construct() {

        $this->_conect('database'); //Config padrão de banco 'database'

        return $this;
    }

    /**
     * Cria a conexão com banco
     * @param type $name Nome da configuracao para conexão
     * @return type
     * @throws Exception
     * @throws \Doctrine\DBAL\DBALException
     */
    private function _conect($name = 'database') {

        if (is_instanceof('Doctrine\DBAL\Connection', $this->doctrine) === false) {

            $_config = \Codando\App::getConfig($name);

            $this->config = $_config;

            //Log set
            $this->log = isset($_config['log']) ? $_config['log'] : true;

            if (USER_IP != "127.0.0.1") {
                $this->log = false;
            }

            if (count($_config) >= 6) {

                $this->cache = new \Codando\System\Cache(1600, COD_DIR_APP . '/cache/db/');

                if (USER_IP == "127.0.0.1" || isset($_SESSION['cacheoff'])) {
                    $this->cacheoff = true;
                }

                try {

                    try {

                        $config = new \Doctrine\DBAL\Configuration();

                        $connectionParams = array(
                            'dbname' => $_config['name'],
                            'user' => $_config['user'],
                            'password' => $_config['password'],
                            'host' => $_config['host'],
                            'driver' => 'pdo_' . $_config['driver'],
                        );

                        $this->doctrine = \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);

                        if (is_instanceof('Doctrine\DBAL\Connection', $this->doctrine) === true) {
                            //$this->doctrine->executeQuery("SET NAMES UTF8") ;
                        }
                    } catch (\Doctrine\DBAL\DBALException $e) {

                        $error = $this->getErrorInfo($e);

                        if ($error['code'] == '2005')
                            throw new Exception($this->exception['code-2005']);
                        elseif ($error['code'] == '2002')
                            throw new Exception($this->exception['code-2002']);
                        elseif ($error['code'] == '1045')
                            throw new Exception(sprintf($this->exception['code-1045'], 'root', '******'));
                        else
                            throw $e;

                        $this->erro = true;
                        $this->msg[] = $e->getMessage();
                        return NULL;
                    }
                } catch (\Doctrine\DBAL\DBALException $e) {

                    $this->getErrorInfo($e);
                }
            }
        }
    }

    /**
     * Disconect
     * Quando o objeto for destruido a conexao e fechada
     */
    public function _disconect() {
        $this->doctrine = null;
    }

    public function _reconect($name = 'database') {

        $this->_disconect();

        $this->clearAll();

        $this->_conect($name);
    }

    /**
     * Destrutor
     * Quando o objeto for destruido a conexao e fechada
     */
    public function __destruct() {
        $this->doctrine = null;
    }

    private function cacheExist($table, $queryBuilder) {

        $_keycache = false;
        $_sql = $queryBuilder->getSQL();

        if ($this->cacheoff === false && $this->config['cache'] === true && in_array($table, $this->config['nocache']) === false && strpos($_sql, 'RAND()') === false && strpos($_sql, 'COUNT(*) AS count') === false) {

            $_keycache = md5($_sql . implode(', ', $queryBuilder->getParameters()) . $queryBuilder->getType());

            $_cache = $this->cache->get($_keycache);

            if ($_cache !== true) {

                return (object) array('cache' => $_cache);
            }
        }

        return (object) array('keycache' => $_keycache, 'execute' => $queryBuilder->execute());
    }

    public function _list($moduloOrTable, array $fieldsNames = array(' * '), array $fieldsWhereValue = array(), $limit = NULL, $order = NULL, $fetch = PDO::FETCH_NUM) {

        $list = array();

        $tabela = is_instanceof('Codando\Modulo\Modulo', $moduloOrTable) ? $moduloOrTable->getTabela(true) : $moduloOrTable;


        try {

            $select = $this->select($tabela, $fieldsNames, current($fieldsWhereValue), end($fieldsWhereValue), $limit, $order);

            if (property_exists($select, 'cache') === true) {
                return $select->cache;
            }

            if (property_exists($select, 'execute') === false) {
                return array();
            }

            $execute = $select->execute;

            if ($execute != NULL && $execute->rowCount() > 0) {

                if (is_instanceof('Codando\Modulo\Modulo', $moduloOrTable) === true) {

                    $execute->setFetchMode(PDO::FETCH_CLASS, $moduloOrTable->getClasseNome());

                    //Obter foreigns
                    while ($registroObj = $execute->fetch()) {

                        if (method_exists($registroObj, 'getId') === true)
                            $list[$registroObj->getId()] = $registroObj;
                    }
                } else {

                    $list = $execute->fetchAll($fetch);
                }

                $execute = NULL;
                unset($execute);
            }
        } catch (PDOException $e) {

            $this->erro = true;
            $this->msg[] = $e->getMessage();
            $this->getErrorInfo($e);
        }

        //Salvando Cache
        if ($select->keycache !== false) {
            $this->cache->createCache($select->keycache, $list);
        }


        return $list;
    }

    public function _load($moduloOrTable, $fieldsWhereValue = array()) {

        $load = NULL;

        $table = is_instanceof('Codando\Modulo\Modulo', $moduloOrTable) ? $moduloOrTable->getTabela(true) : $moduloOrTable;

        try {

            $select = $this->select($table, array(), current($fieldsWhereValue), end($fieldsWhereValue), 1);

            if (property_exists($select, 'cache') === true) {
                return $select->cache;
            }

            if (property_exists($select, 'execute') === false) {
                return array();
            }

            $execute = $select->execute;

            $ismodulo = is_instanceof('Codando\Modulo\Modulo', $moduloOrTable);

            $tableOrClass = $ismodulo === true ? $moduloOrTable->getClasseNome() : "\\Codando\\Modulo\\" . tableToclass($table);

            if ($ismodulo === true)
                $load = $execute->fetchAll(PDO::FETCH_CLASS, $tableOrClass);
            else
                $load = $execute->fetchAll(PDO::FETCH_ASSOC);

            $load = end($load);
        } catch (PDOException $e) {

            $this->erro = true;
            $this->msg[] = $e->getMessage();
            $this->getErrorInfo($e->getMessage());
        }

        unset($execute);

        //Salvando Cache
        if ($select->keycache !== false) {
            $this->cache->createCache($select->keycache, $load);
        }

        return $load;
    }

    public function count($table, $fieldsWhereValue = array()) {

        $select = $this->select($table, array('COUNT(*) AS count'), current($fieldsWhereValue), end($fieldsWhereValue));

        if (property_exists($select, 'cache') === true) {
            return $select->cache;
        }

        if (property_exists($select, 'execute') === false) {
            return 0;
        }

        $execute = $select->execute;

        $fetchtemp = $execute->fetchAll(PDO::FETCH_OBJ);

        $count = end($fetchtemp);

        unset($fetchtemp);

        if (property_exists($count, 'count') === true)
            return $count->count;

        return 0;
    }

    public function insert($table, $fieldsNamesOrNamesToValue = array()) {

        if (is_instanceof('Doctrine\DBAL\Connection', $this->doctrine) === false) {
            $this->getErrorInfo('DB Not Connection');
        }

        $this->clearAll();

        $queryBuilder = $this->doctrine->createQueryBuilder()
                ->insert($table);

        $ind = 0;

        foreach ((array) $fieldsNamesOrNamesToValue as $name => $value) {

            $queryBuilder->setValue($name, '?')
                    ->setParameter($ind++, $value);
        }

        if ($queryBuilder->execute() > 0) {

            return $this->doctrine->lastInsertId("id_" . $table);
        }

        return NULL;
    }

    public function update($table, $fieldsValue = array(), $sqlParamWhere = NULL, $valueListParamWhere = array()) {

        if (is_instanceof('Doctrine\DBAL\Connection', $this->doctrine) === false) {
            $this->getErrorInfo('DB Not Connection');
        }

        if (is_array($sqlParamWhere) === true || is_string($sqlParamWhere) === false) {
            $this->getErrorInfo('DB UPDATE WHERE ERRO');
        }

        $this->clearAll();

        $queryBuilder = $this->doctrine->createQueryBuilder()
                ->update($table);

        foreach ((array) $fieldsValue as $name => $value) {

            $queryBuilder->set($name, ':' . $name)->setParameter(':' . $name, $value);
        }

        if (is_string($sqlParamWhere) === true && $sqlParamWhere !== NULL) {

            $queryBuilder->where($sqlParamWhere);

            if (is_array($valueListParamWhere) && count($valueListParamWhere) > 0) {
                foreach ((array) $valueListParamWhere as $name => $value) {
                    $queryBuilder->setParameter(':' . $name, $value);
                }
            }
        }

        if ($queryBuilder->execute() > 0) {

            return true;
        }

        return false;
    }

    public function delete($table, $sqlParamWhere = NULL, $valueListParamWhere = array()) {

        if (is_instanceof('Doctrine\DBAL\Connection', $this->doctrine) === false) {
            $this->getErrorInfo('DB Not Connection');
        }

        if (is_array($sqlParamWhere) === true || is_string($sqlParamWhere) === false) {
            $this->getErrorInfo('DB DELETE WHERE ERRO');
        }

        $this->clearAll();

        $queryBuilder = $this->doctrine->createQueryBuilder()
                ->delete($table);

        if (is_string($sqlParamWhere) === true && $sqlParamWhere !== NULL) {

            $queryBuilder->where($sqlParamWhere);

            if (is_array($valueListParamWhere) && count($valueListParamWhere) > 0) {

                $queryBuilder->setParameters($valueListParamWhere);
            }
        }

        if ($queryBuilder->execute() > 0) {

            return true;
        }

        return false;
    }

    public function select($table, $fieldsNames = array(), $sqlParamWhere = NULL, $valueListParamWhere = array(), $limit = NULL, $order = NULL) {

        $queryBuilder = $this->buildSQL($table, $fieldsNames, $sqlParamWhere, $valueListParamWhere, $limit, $order);

        return $this->cacheExist($table, $queryBuilder);

        //return $this->doctrine->executeQuery($this->sql, $queryBuilder->getParameters(), $queryBuilder->getType(),  new \Doctrine\DBAL\Cache\QueryCacheProfile(3600, md5($this->sql . implode(', ', $queryBuilder->getParameters())), $this->cacheFile));
    }

    public function buildSQL($table, $fieldsNames = array(), $sqlParamWhere = NULL, $valueListParamWhere = array(), $limit = NULL, $order = NULL) {

        if (is_instanceof('Doctrine\DBAL\Connection', $this->doctrine) === false) {
            $this->getErrorInfo('DBAL Not Connection');
        }

        $this->clearAll();

        $queryBuilder = $this->doctrine->createQueryBuilder()
                ->select(count($fieldsNames) == 0 ? $table[0] . '.*' : $fieldsNames)
                ->from($table, $table[0]);

        //$queryBuilder->useResultCache(true, $this->timecache);

        if (is_string($sqlParamWhere) === true && $sqlParamWhere !== NULL) {

            $queryBuilder->where($sqlParamWhere);

            if (is_array($valueListParamWhere) && count($valueListParamWhere) > 0) {

                $queryBuilder->setParameters($valueListParamWhere);
            }
        }

        if ($order !== NULL) {

            $queryBuilder->orderBy($order, ' ');
        }

        if (is_array($limit) === true) {

            $queryBuilder->setFirstResult($limit[0])
                    ->setMaxResults($limit[1]);
        } else if ($limit > 0) {

            $queryBuilder->setMaxResults($limit);
        }

        $this->sql = $queryBuilder->getSQL();

        //Registre Log
        if ($this->log === true) {
            @error_log(date('Y-m-d-H:i:s') . "-" . USER_IP . "-" . $this->sql . "\r\n", 3, COD_DIR_LOG . "/selectsql.log");
        }

        return $queryBuilder;
    }

    public function setLog($log) {
        $this->log = $log;
        return $this;
    }

    public function beginTransaction() {
        $this->doctrine->beginTransaction();
        return $this;
    }

    public function getConnection() {
        return $this->doctrine;
    }

    public function rollBack() {
        $this->doctrine->rollBack();
        return $this;
    }

    public function commit() {
        $this->doctrine->commit();
        return $this;
    }

    public function setAutoCommit($autoCommit = false) {
        $this->doctrine->setAutoCommit($autoCommit);
        return $this;
    }

    public function getSql() {
        return $this->sql;
    }

    public function isErro() {
        return $this->erro;
    }

    private function clearAll() {
        $this->sql = NULL;
        $this->erro = false;
        return $this;
    }

    public function setCacheoff($cache) {
        $this->cacheoff = $cache;
    }

    private function getErrorInfo($e, $show = false) {

        $info = null;
        $errorInfo = null;
        $message = method_exists($e, 'getMessage') ? $e->getMessage() : $e;


        if ($_SERVER['SERVER_ADDR'] != '127.0.0.1')
            @error_log("\n\t" . date('d-m-Y H:i') . " - ERRO: [ " . $message . " ] - SQL: [ " . $this->sql . " ]", 3, COD_DIR_LOG . "/sql.log");

        $mensagem = "<pre data-erro=\"db\" style=\"margin: auto;text-align: center;border: 5px red solid;\">
                         <strong>ERRO:</strong> [ " . $message . " ]<br>
                         <strong>SQL:</strong> [ " . $this->sql . " ]
                        </pre>";

        echo ($_SERVER['SERVER_ADDR'] == '127.0.0.1') ? $mensagem : "<!-- " . $mensagem . " -- >";

        preg_match('~SQLSTATE[[]([[:alnum:]]{1,})[]]:?\s[[]?([[:digit:]]{1,})?[]]?\s?(.+)~', $message, $errorInfo);
        $info['state'] = isset($errorInfo[1]) ? $errorInfo[1] : null;
        $info['code'] = isset($errorInfo[2]) ? $errorInfo[2] : null;
        $info['message'] = isset($errorInfo[3]) ? $errorInfo[3] : null;

        return $info;
    }

    /*
     * @return Db
     */

    public static function get_db() {

        static $instance = null;

        return $instance ?: $instance = new static;
    }

}
