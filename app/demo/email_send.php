<?php

define('CONFIG_LOAD', TRUE);

require(dirname(__FILE__) . '/bootstrap.php');

$_config = \Codando\App::getConfig('emailsend');

$transport = \Swift_SmtpTransport::newInstance($_config['smtp'], $_config['port'], 'tls')
        ->setUsername($_config['email'])
        ->setPassword($_config['senha']);

$emailArray = explode(',', 'andrey@neexbrasil.com');

$emailFields = array(
    'Nome' => 'Nome',
    'Telefone' => 'Telefone',
    'Email' => 'Email',
    'Mensagem' => 'mensagem',
    'Gerenciar' => 'Gerencia',
);

$html_email = tpl()->display('contato_send', array('emailFields' => $emailFields), false);

$mailer = \Swift_Mailer::newInstance($transport);

$message = \Swift_Message::newInstance('TESTE')
        ->setFrom(array($_config['return'] => 'Email'))
        ->setTo($emailArray)
        ->addPart($html_email, 'text/html');


if ($mailer->send($message)) {

    echo 'Enviado';
} else {

    echo 'Não Enviado';
}